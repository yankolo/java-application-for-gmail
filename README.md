# Java Application for Gmail

### 420-517-DW Software Development Project - Java III

### Yanik Kolomatski

### Instructions

## To write a new email:
 Click the top left button (with the pencil). 
 
 An in-program popup will show.
 
 In there, it is possible to add attachments (using the attachment button). To delete added attachments, click on the attachment in the bottom
 
## To create a new folder: 
 Click the second top left button (folder with a plus). Then, follow the instructions.
 
## To change the settings: 
 Click on the top right button (the gear). Click finish after modifying the settings.
 
## To delete/rename folders: 
 Right click on them and choose the wanted option. Then, follow the instructions.
 
## To view an email: 
 Double click on it. 
 
 An in-program popup will show.
 
 In there, you will be able to reply, reply all, or forward emails.
 To view attachments, click on them (on the bottom).
 
## To reply/reply all/forward/delete an email: 
    Right click on the email in the table, 
    OR
    View the email and use the top buttons that are on top of the email. 



