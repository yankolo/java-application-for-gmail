package me.yanik.business;

import jodd.mail.*;
import me.yanik.data.*;
import javax.mail.Flags;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.List;
import javax.activation.DataSource;
import java.util.Arrays;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The MailController is used to send and receive emails. When sending emails, EmailBean objects must be used.
 * When receiving emails, EmailBean objects will be returned.
 *
 * When receiving emails, only the emails that are not "seen" will be returned from the server. If there are no
 * emails waiting to be received, an empty array will be returned.
 *
 * Beware, that when receiving emails, an array of emails will always be returned, as there might several emails
 * waiting to be received. When sending emails, it only possible to send one email at a time.
 *
 * When trying to send an email, the MailController will first validate the EmailBean object passed to it, if the
 * email is invalid, an InvalidMailException will be thrown. To know in which cases the InvalidMailException will
 * be thrown consult the documentation of sendEmail() method.
 *
 * Examples of possible uses of the EmailController class:
 *
 *      Send an email:
 *
 *          MailController controller = new MailController("smtp.example.com", "imap.example.com",
 *                                                      "email@example.com", "example_password");
 *          EmailBean emailBean = new EmailBean();
 *          // Initialize email with values
 *
 *          controller.sendEmail();
 *
 *      Receive emails:
 *
 *          MailController controller = new MailController("smtp.example.com", "imap.example.com",
 *                                                      "email@example.com", "example_password");
 *          // Get all emails that are not "seen"
 *          EmailBean[] emailBeans = controller.receiveEmails();
 *
 *          // Get the oldest email that is not "seen"
 *          EmailBean oldestEmail = emailBeans[0];
 *
 * @author Yanik Kolomatski
 */
public class MailController {
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);

    private final String smtpServerName;
    private final String imapServerName;
    private int smtpServerPort = 25;
    private int imapServerPort = 143;
    private final String userEmail;
    private final String userPassword;

    private ResourceBundle resourceBundle;

    public MailController(String smtpServerName, int smtpServerPort, String imapServerName, int  imapServerPort, String userEmail, String userPassword) {
        this.resourceBundle = ResourceBundle.getBundle("ClientBundle");

        this.smtpServerName = smtpServerName;
        this.imapServerName = imapServerName;
        this.smtpServerPort = smtpServerPort;
        this.imapServerPort = imapServerPort;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }

    /**
     * Send an email to the specified recipients in the EmailBean object
     * The EmailBean passed to the method must be valid.
     * I.E. it must conform to the following rules:
     *
     *      An EmailBean object is required to have the following fields set:
     *          "from"
     *          "to"
     *          (All other fields are optional)
     *
     *      The "from" field should correspond to the userEmail string set in the constructor of the MailController
     *      The "to" field should contain Address objects that have a valid email address (RFC2822)
     *      The "cc" field should contain Address objects that have a valid email address (RFC2822)
     *      The "bcc" field should contain Address objects that have a valid email address (RFC2822)
     *      The "priority" field should be either 1, 2, 3, 4, 5 or -1 (if not set)
     *
     * @param emailBean The emailBean to send
     * @throws InvalidMailException Exception is thrown when the EmailBean object passed is invalid:
     *          "from" field is not set
     *          "to" field is not set
     *          "from" field does not correspond to the userEmail string set in the constructor of the MailController
     *          "to" field does not contain Address objects that have a valid email address (RFC2822)
     *          "cc" field does not contain Address objects that have a valid email address (RFC2822)
     *          "bcc" field does not contain Address objects that have a valid email address (RFC2822)
     *          "priority" field is not one of the following numbers 1, 2, 3, 4, 5 or -1 (if not set)
     */
    public void sendEmail(EmailBean emailBean) throws InvalidMailException {
        LOG.info("Starting the send email process");

        checkEmailBean(emailBean);
        Email email = emailBeanToJoddEmail(emailBean);

        SmtpServer smtpServer = createSmtpServer();
        sendJoddEmail(smtpServer, email);

        emailBean.setDateSent(LocalDateTime.now());
    }

    /**
     * Receive emails from the IMAP server specified in the MailController constructor
     * Returns an array of EmailBeans as there can be multiple emails at the server
     *
     * Only the emails that are not "seen" will be returned from the server. If there are no
     * emails waiting to be received, an empty array will be returned.
     *
     * @return Array of received emails
     */
    public EmailBean[] receiveEmails() {
        ImapServer imapServer = createImapServer();
        ReceivedEmail[] joddEmails = receiveUnseenEmails(imapServer);
        EmailBean[] emailBeans = manyJoddEmailsToBeans(joddEmails);

        return emailBeans;
    }

    /**
     * Create an IMAP server instance which will be used to open a connection session to the server
     */
    private ImapServer createImapServer() {
        LOG.info("Creating IMAP server");

        ImapServer imapServer = MailServer.create()
                .host(imapServerName)
                .port(imapServerPort)
                .ssl(true)
                .auth(userEmail, userPassword)
                .buildImapMailServer();

        return imapServer;
    }

    /**
     * Receive all emails from the IMAP server that were not seen
     */
    private ReceivedEmail[] receiveUnseenEmails(ImapServer imapServer) {
        ReceivedEmail[] emails;

        // Opening a connection session to the IMAP server and receiving emails
        try (ReceiveMailSession session = imapServer.createSession()) {
            LOG.info("Opening IMAP session");
            session.open();

            LOG.info("Receiving emails");
            emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
        } catch (Exception e){
            // Opening an IMAP session and receiving emails should not cause an exception
            // Therefore, logging and and rethrowing exception
            LOG.error("Unexpected exception occurred after opening IMAP session or receiving emails: " +
                    e.getMessage() + "\n" + e.getStackTrace());
            throw e;
        }

        return emails;
    }

    private EmailBean[] manyJoddEmailsToBeans(ReceivedEmail[] emails) {
        LOG.info("Transforming received Jodd Emails into EmailBean objects");

        EmailBean[] emailBeans = Arrays.stream(emails)
                .map(receivedEmail -> joddReceivedEmailToBean(receivedEmail))
                .toArray(EmailBean[]::new);

        return emailBeans;
    }

    /**
     * Create an SMTP server instance which will be used to open a connection session to the server
     */
    private SmtpServer createSmtpServer() {
        LOG.info("Creating SMTP Server");

        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(smtpServerName)
                .port(smtpServerPort)
                .auth(userEmail, userPassword)
                .buildSmtpMailServer();

        return smtpServer;
    }

    /**
     * Open a connection session to the SMTP server and send a jodd email
     */
    private void sendJoddEmail(SmtpServer smtpServer, Email email) {
        try (SendMailSession session = smtpServer.createSession()) {
            LOG.info("Opening SMTP session");
            session.open();

            LOG.info("Sending email: " + email.toString());
            session.sendMail(email);
        } catch (Exception e) {
            // Opening an SMTP session and sending an email should not cause an exception
            // because the emails were already validated prior the calling of sendJoddEmail()
            // Therefore, logging and and rethrowing exception
            LOG.error("Unexpected exception occurred after opening SMTP session or sending email: " +
                    e.getMessage() + "\n" + e.getStackTrace());
            throw e;
        }
    }

    /**
     * Convert the ReceivedEmail object from the jodd-mail library to EmailBean objects.
     * This method is used after receiving emails from the IMAP server in order to return EmailBean objects.
     *
     * @param joddReceivedEmail A ReceivedEmail object from the jodd-mail library to be converted
     * @return An EmailBean object created from the ReceivedEmail object which is passed in the parameters
     */
    private EmailBean joddReceivedEmailToBean(jodd.mail.ReceivedEmail joddReceivedEmail) {
        // In the following few blocks of code, jodd's EmailAddress objects are converted
        // in to me.yanik.data.Address objects
        LOG.info("Tranforming email: " + joddReceivedEmail.toString());

        LOG.info("Creating 'from' address");
        Address fromAddress = new Address();
        fromAddress.setEmail(joddReceivedEmail.from().getEmail());
        fromAddress.setPersonalName(joddReceivedEmail.from().getPersonalName());

        LOG.info("Creating 'to' addresses");
        Address[] toAddresses = Arrays.stream(joddReceivedEmail.to())
                .map(joddAdress -> {
                    Address toAddress = new Address();
                    toAddress.setEmail(joddAdress.getEmail());
                    toAddress.setPersonalName(joddAdress.getPersonalName());
                    return toAddress; })
                .toArray(Address[]::new);

        LOG.info("Creating 'cc' addresses");
        Address[] ccAddresses = null;
        if (joddReceivedEmail.cc().length > 0) {
            ccAddresses = Arrays.stream(joddReceivedEmail.cc())
                    .map(joddAdress -> {
                        Address ccAddress = new Address();
                        ccAddress.setEmail(joddAdress.getEmail());
                        ccAddress.setPersonalName(joddAdress.getPersonalName());
                        return ccAddress; })
                    .toArray(Address[]::new);
        }

        LOG.info("Getting subject");
        String subject = joddReceivedEmail.subject();

        LOG.info("Getting the priority");
        int priority = joddReceivedEmail.priority();

        LOG.info("Getting text and html messages");
        // In the jodd-email library, an email can have multiple messages, therefore the following code
        // gets all messages, then finds all messages that are plain text and those that are html.
        // This is done in order to separate the different kind of messages
        List<EmailMessage> messages = joddReceivedEmail.messages();
        List<EmailMessage> plainTextMessages = messages.stream()
                .filter(joddMessage -> joddMessage.getMimeType().equals("TEXT/PLAIN"))
                .collect(Collectors.toList());
        List<EmailMessage> htmlTextMessages = messages.stream()
                .filter(joddMessage -> joddMessage.getMimeType().equals("TEXT/HTML"))
                .collect(Collectors.toList());

        // Concatenate all plain text messages into one string & all html messages into one string
        // This is done so that we will be able to store plain text and html messages as a simple strings
        // in the EmailBean class
        String plainMessage = null;
        String htmlMessage = null;
        if (plainTextMessages.isEmpty() == false) {
            plainMessage = plainTextMessages.stream()
                    .map(joddMessage -> joddMessage.getContent())
                    .collect(Collectors.joining());
        }
        if (htmlTextMessages.isEmpty() == false) {
            htmlMessage = htmlTextMessages.stream()
                    .map(joddMessage -> joddMessage.getContent())
                    .collect(Collectors.joining());
        }

        LOG.info("Creating attachments");
        // Creating array of Attachment objects (which are simplified)
        // from the the jodd attachments
        List<EmailAttachment<? extends DataSource>> allAttachments = joddReceivedEmail.attachments();
        Attachment[] newAttachments = null;
        if (allAttachments.isEmpty() == false) {
            newAttachments = allAttachments.stream()
                    .map(joddAttachment -> {
                            Attachment attachment = new Attachment(joddAttachment.getName(),
                                joddAttachment.toByteArray(), joddAttachment.isEmbedded());
                            if (joddAttachment.isEmbedded())
                                attachment.setContentId(joddAttachment.getContentId().replaceAll("[<>]", ""));

                            return attachment;
                    })
                    .toArray(Attachment[]::new);
        }

        LOG.info("Getting date receiving");
        // Getting the date received from the email object and converting it to LocalDateTime
        LocalDateTime dateReceived = LocalDateTime.ofInstant(joddReceivedEmail.receivedDate().toInstant(), ZoneId.systemDefault());

        // Creating the EmailBean and setting its values
        EmailBean bean = new EmailBean();
        bean.setFrom(fromAddress);
        bean.setTo(toAddresses);
        bean.setCc(ccAddresses);
        bean.setSubject(subject);
        bean.setTextMessage(plainMessage);
        bean.setHtmlMessage(htmlMessage);
        bean.setAttachments(newAttachments);
        bean.setDateReceived(dateReceived);
        bean.setPriority(priority);

        LOG.info("EmailBean created: " + bean.toString());

        return bean;
    }

    /**
     * Convert an EmailBean object to an Email object from the jodd-mail library
     * This method is used prior to sending emails, because the jodd-mail library
     * can send only Email object from its library and not EmailBean objects.
     *
     * @param bean The EmailBean object to convert into jodd.mail.Email object
     * @return A jodd.mail.Email object created from the EmailBean object
     */
    private jodd.mail.Email emailBeanToJoddEmail(EmailBean bean) {
        LOG.info("Transforming EmailBean object to jodd.mail.Email object...");

        // In the following few blocks of code, me.yanik.data.Address objects are converted
        // in to jodd's EmailAddress objects
        LOG.info("Creating the 'from' EmailAddress object");
        EmailAddress fromAddress = new EmailAddress(bean.getFrom().getPersonalName(), bean.getFrom().getEmail());

        LOG.info("Creating the 'to' EmailAddress objects");
        EmailAddress[] toAddresses = Arrays.stream(bean.getTo())
                .map(address -> new EmailAddress(address.getPersonalName(), address.getEmail()))
                .toArray(EmailAddress[]::new);

        LOG.info("Creating the 'cc' EmailAddress objects");
        EmailAddress[] ccAddresses = null;
        if (bean.getCc() != null) {
            ccAddresses = Arrays.stream(bean.getCc())
                    .map (address -> new EmailAddress(address.getPersonalName(), address.getEmail()))
                    .toArray(EmailAddress[]::new);
        }

        LOG.info("Creating the 'bcc' EmailAddress objects");
        EmailAddress[] bccAddresses = null;
        if (bean.getBcc() != null) {
            bccAddresses = Arrays.stream(bean.getBcc())
                    .map (address -> new EmailAddress(address.getPersonalName(), address.getEmail()))
                    .toArray(EmailAddress[]::new);
        }

        LOG.info("Getting the subject");
        String subject = bean.getSubject();

        LOG.info("Getting the priority");
        int priority = bean.getPriority();

        // Creating List of normal attachments and embedded attachments
        // so it will be easier to insert the attachments to the Email class
        // EmailAttachmentBuilder objects are made instead of EmailAttachment objects because
        // the methods that attach attachments to the email can only modify attachment values (like isInline)
        // when given a EmailAttachmentBuilder object
        LOG.info("Creating Attachment objects");
        List<EmailAttachmentBuilder> normalAttachments = null;
        List<EmailAttachmentBuilder> embeddedAttachments = null;
        if (bean.getAttachments() != null) {
            normalAttachments = Arrays.stream(bean.getAttachments())
                    .filter(attachment -> attachment.isEmbedded() == false)
                    .map(attachment -> EmailAttachment.with()
                            .name(attachment.getName())
                            .content(attachment.getContent()))
                    .collect(Collectors.toList());

            embeddedAttachments = Arrays.stream(bean.getAttachments())
                    .filter(attachment -> attachment.isEmbedded() == true)
                    .map(attachment -> EmailAttachment.with()
                            .name(attachment.getName())
                            .content(attachment.getContent())
                            .contentId(attachment.getContentId()))
                    .collect(Collectors.toList());
        }

        LOG.info("Creating the Jodd Email object");

        // Creating the jodd.mail.Email and object and setting its values
        Email joddEmail = Email.create()
                .from(fromAddress)
                .to(toAddresses)
                .cc(ccAddresses)
                .bcc(bccAddresses)
                .subject(subject)
                .priority(priority);

        // jodd-mail library creates an EmailMessage that contains a null string
        // if we call the textMessage() method with a null string.
        // Same goes for attachments, as it creates an EmailAttachment that contains
        // an empty byte[].
        // This is unwanted behavior, therefore the following code checks whether
        // the values are null prior to calling their respective methods
        if (bean.getTextMessage() != null)
            joddEmail.textMessage(bean.getTextMessage());
        if (bean.getHtmlMessage() != null)
            joddEmail.htmlMessage(bean.getHtmlMessage());
        if (bean.getAttachments() != null) {
            normalAttachments.stream().forEach(joddAttachment -> joddEmail.attachment(joddAttachment));
            embeddedAttachments.stream().forEach(joddAttachment -> joddEmail.embeddedAttachment(joddAttachment));
        }

        LOG.info("Jodd Email object created");

        return joddEmail;
    }

    /**
     * Validates the EmailBean object. If the EmailBean object is invalid an InvalidMailException is thrown
     *
     * @param bean An EmailBean object to validate
     * @throws InvalidMailException Exception is thrown when the EmailBean object passed is invalid:
     *           "from" field is not set
     *           "to" field is not set
     *           "from" field does not correspond to the userEmail string set in the constructor of the MailController
     *           "to" field does not contain Address objects that have a valid email address (RFC2822)
     *           "cc" field does not contain Address objects that have a valid email address (RFC2822)
     *           "bcc" field does not contain Address objects that have a valid email address (RFC2822)
     *           "priority" field is not one of the following numbers 1, 2, 3, 4, 5 or -1 (if not set)
     */
    private void checkEmailBean(EmailBean bean) throws InvalidMailException {
        String errorMessage;

        LOG.info("Verifying if email is valid");

        // Verifying if 'from' address exists (Required for emails)
        if (bean.getFrom() == null) {
            errorMessage = resourceBundle.getString("email_error_no_sender");
            LOG.error(errorMessage);
            throw new InvalidMailException(errorMessage);
        }

        // Verifying if 'to' address exists (Required for emails)
        if (bean.getTo() == null) {
            errorMessage = resourceBundle.getString("email_error_no_recipient");
            LOG.error(errorMessage);
            throw new InvalidMailException(errorMessage);
        }

        // Verifying if the 'from' email matches with the email address contained
        // in the MailController instance (userEmail)
        if (bean.getFrom().getEmail() != userEmail) {
            errorMessage = String.format(resourceBundle.getString("email_error_from_not_matching"),
                    bean.getFrom().getEmail(), userEmail);
            LOG.error(errorMessage);
            throw new InvalidMailException(errorMessage);
        }

        // Verifying if 'to' addresses are valid
        List<String> invalidToAddresses = Arrays.stream(bean.getTo())
                .map(address -> address.getEmail())
                .filter(address -> !checkEmail(address))
                .collect(Collectors.toList());
        if (invalidToAddresses.isEmpty() == false) {
            errorMessage = String.format(resourceBundle.getString("email_error_to"),
                    String.join(", ", invalidToAddresses));
            LOG.error(errorMessage);
            throw new InvalidMailException(errorMessage);
        }

        // Verifying if 'cc' addresses are valid
        if (bean.getCc() != null) {
            List<String> invalidCcAddresses = Arrays.stream(bean.getCc())
                    .map(address -> address.getEmail())
                    .filter(address -> !checkEmail(address))
                    .collect(Collectors.toList());
            if (invalidCcAddresses.isEmpty() == false) {
                errorMessage = String.format(resourceBundle.getString("email_error_cc"),
                        String.join(", ", invalidCcAddresses));
                LOG.error(errorMessage);
                throw new InvalidMailException(errorMessage);
            }
        }

        // Verifying if 'bcc' addresses are valid
        if (bean.getBcc() != null) {
            List<String> invalidBccAddresses = Arrays.stream(bean.getBcc())
                    .map(address -> address.getEmail())
                    .filter(address -> !checkEmail(address))
                    .collect(Collectors.toList());
            if (invalidBccAddresses.isEmpty() == false) {
                errorMessage = String.format(resourceBundle.getString("email_error_bcc"),
                        String.join(", ", invalidBccAddresses));
                LOG.error(errorMessage);
                throw new InvalidMailException(errorMessage);
            }
        }

        // Verifying if "priority" is valid
        IntStream validPriorities = IntStream.of(-1, 1, 2, 3, 4, 5);
        if (validPriorities.noneMatch(validPriority -> validPriority == bean.getPriority())) {
            errorMessage = String.format(resourceBundle.getString("email_error_priority"), bean.getPriority());
            LOG.error(errorMessage);
            throw new InvalidMailException(errorMessage);
        }
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
}
