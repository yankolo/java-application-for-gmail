package me.yanik.business;

/**
 * The InvalidMailException is thrown when trying to send an invalid email
 * Here are a few examples (might be more, depends on implementation) when the exception might be thrown:
 *      'from' field is missing
 *      'to' field is missing
 *      Address objects in the 'to' field contain email addresses that violate the RFC 2822 format for emails
 *      Address objects in the 'cc' field contain email addresses that violate the RFC 2822 format for emails
 *      etc.
 *
 * @author Yanik Kolomatski
 */
public class InvalidMailException extends Exception {
    public InvalidMailException() { }

    public InvalidMailException(String message) {
        super(message);
    }
}
