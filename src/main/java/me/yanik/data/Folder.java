package me.yanik.data;

import javafx.beans.property.*;

import java.util.Arrays;
import java.util.Objects;

/**
 * The Folder class represents folders in the Email Application. Logically in the email client,
 * it "contains" EmailBeans objects, but in reality it doesn't contain them. Instead, the EmailBean
 * objects have a reference to the folder object.
 */
public class Folder {
    private int id = -1;
    private StringProperty name = new SimpleStringProperty();
    private BooleanProperty deletable = new SimpleBooleanProperty(true);
    private BooleanProperty renamable = new SimpleBooleanProperty(true);
    private ObjectProperty<EmailBean[]> emailBeans = new SimpleObjectProperty<>();

    /**
     * Gets the id of the folder
     * @return The id of the folder (-1 if not in the database)
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the folder
     * @param id The id of the folder
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the name of the folder
     * @return The name of the folder
     */
    public String getName() {
        return name.get();
    }

    /**
     * Sets the name of the folder
     * @param name The name of the folder
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * Returns whether this folder is deletable
     * @return True if the folder is deletable
     */
    public boolean isDeletable() {
        return deletable.get();
    }

    /**
     * Sets whether this folder is deletable
     * @param deletable True if the folder is deletable
     */
    public void setDeletable(boolean deletable) {
        this.deletable.set(deletable);
    }

    /**
     * Returns whether this folder is renamable
     * @return True if the folder is renamable
     */
    public boolean isRenamable() {
        return renamable.get();
    }

    /**
     * Sets whether this folder is renamable
     * @param renamable True if the folder is renamable
     */
    public void setRenamable(boolean renamable) {
        this.renamable.set(renamable);
    }

    /**
     * Gets the EmailBean objects that the folder contains
     * @return The EmailBean objects that the folder contains
     */
    public EmailBean[] getEmailBeans() {
        return emailBeans.get();
    }

    /**
     * Sets the EmailBean objects that the folder should contain
     * @param emailBeans The EmailBean objects that the folder should contain
     */
    public void setEmailBeans(EmailBean[] emailBeans) {
        this.emailBeans.set(emailBeans);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public BooleanProperty deletableProperty() {
        return deletable;
    }

    public BooleanProperty renamableProperty() {
        return renamable;
    }

    public ObjectProperty<EmailBean[]> emailBeansProperty() {
        return emailBeans;
    }

    /**
     * Verify if an object is equal to this object
     *
     * The following fields are considered:
     *      "name"
     *      "emailBeans"
     *
     * @param o The object to compare
     * @return True if equals, false if not equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return deletable == folder.deletable &&
                renamable == folder.renamable &&
                Objects.equals(name.get(), folder.name.get()) &&
                Arrays.equals(emailBeans.get(), folder.emailBeans.get());
    }

    /**
     * Create a hashcode from the object
     *
     * The following fields are considered:
     *      "name"
     *      "emailBeans"
     *
     * @return A hashcode
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(name.get(), deletable.get(), renamable.get());
        result = 31 * result + Arrays.hashCode(emailBeans.get());
        return result;
    }

    @Override
    public String toString() {
        return "Folder{" +
                "id=" + id +
                ", name='" + name.get() + '\'' +
                ", deletable=" + deletable.get() +
                ", renamable=" + renamable.get() +
                ", emailBeans=" + Arrays.toString(emailBeans.get()) +
                '}';
    }
}
