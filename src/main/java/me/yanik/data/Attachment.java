package me.yanik.data;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Arrays;
import java.util.Objects;

/**
 * The Attachment class represents an email attachment.
 * It is a simplified version of the EmailAttachment class of the jodd-mail library,
 * as it contains only the name of the attachment and its contents as byte[]
 *
 * An Attachment is only used to transport data as it has no functionality.
 */
public class Attachment {
    private int id = -1;
    private StringProperty name = new SimpleStringProperty();
    private ObjectProperty<byte[]> content = new SimpleObjectProperty<>();
    private boolean isEmbedded;
    private String contentId = "";

    public Attachment() { }

    public Attachment(String name, byte[] content, boolean isEmbedded) {
        this.name.set(name);
        this.content.set(content);
        this.isEmbedded = isEmbedded;
    }

    /**
     * Gets the id of the attachment
     * @return The id of the attachment (-1 if not in the database)
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the attachment
     * @param id The id of the attachment
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the file name of the attachment
     * @return The file name of the attachment
     */
    public String getName() {
        return name.get();
    }

    /**
     * Set the file name of the attachment
     * @param name The file name of the attachment
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * Get the content of the attachment
     * @return The content of the attachment
     */
    public byte[] getContent() {
        return content.get();
    }

    /**
     * Set the content of the attachment
     * @param content The content of the attachment
     */
    public void setContent(byte[] content) {
        this.content.set(content);
    }

    /**
     * The name property
     * @return The name property
     */
    public StringProperty nameProperty() {
        return name;
    }

    /**
     * The content wrapped in an object property
     * @return the content wrapped in an object property
     */
    public ObjectProperty<byte[]> contentProperty() {
        return content;
    }

    /**
     * Gets a boolean that tells whether the attachment is embedded
     * @return A boolean that tells whether the attachment is embedded
     */
    public boolean isEmbedded() {
        return isEmbedded;
    }

    /**
     * Marks the attachment as an embedded attachment
     * @param embedded A boolean specifying whether the attachment is embedded
     */
    public void setEmbedded(boolean embedded) {
        isEmbedded = embedded;
    }

    /**
     * Gets the content ID of the attachment
     * @return The content ID of the attachment
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * Sets the content ID of the attachment
     * @param contentId The content ID of the attachment
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    /**
     * Verify if an object is equal to this object
     *
     * The following fields are considered:
     *      "name"
     *      "content"
     *
     * @param o The object to compare
     * @return True if equals, false if not equals
     */


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attachment that = (Attachment) o;
        return Objects.equals(name.get(), that.name.get()) &&
                Arrays.equals(content.get(), that.content.get());
    }

    /**
     * Create a hashcode from the object
     *
     * The following fields are considered:
     *      "name"
     *      "content"
     *
     * @return A hashcode
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(name.get());
        result = 31 * result + Arrays.hashCode(content.get());
        return result;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "id=" + id +
                ", name='" + name.get() + '\'' +
                ", isEmbedded=" + isEmbedded +
                '}';
    }
}
