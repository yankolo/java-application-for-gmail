package me.yanik.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Objects;

/**
 * An Address is used to store email addresses. Due to the fact that emails can have corresponding personal names,
 * the Address stores a personal name, and a an email address.
 *
 * Address objects are mainly used in EmailBean object to represent the email address of
 * the sender, recipients, cc recipients, and bcc recipients.
 *
 * An Address is only used to transport data as it has no functionality.
 */
public class Address {
    private int id = -1;
    private StringProperty personalName = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();

    public Address() { }

    public Address(String personalName, String email) {
        this.personalName.set(personalName);
        this.email.set(email);
    }

    /**
     * Gets the id of the address
     * @return The id of the address (-1 if not in the database)
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the address
     * @param id The id of the address
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the personal name of the email
     * @return The personal name of the email
     */
    public String getPersonalName() {
        return personalName.get();
    }

    /**
     * Set the personal name of the email
     * @param personalName The personal name of the email
     */
    public void setPersonalName(String personalName) {
        this.personalName.set(personalName);
    }

    /**
     * Get the email address
     * @return The email address
     */
    public String getEmail() {
        return email.get();
    }

    /**
     * Set the email address
     * @param email The email address
     */
    public void setEmail(String email) {
        this.email.set(email);
    }

    /**
     * Verify if an object is equal to this object
     *
     * The following fields are considered:
     *      "personalName"
     *      "email"
     *
     * @param o The object to compare
     * @return True if equals, false if not equals
     */

    /**
     * The personalName property
     * @return the personalName property
     */
    public StringProperty personalNameProperty() {
        return personalName;
    }

    /**
     * The email property
     * @return the email property
     */
    public StringProperty emailProperty() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(personalName.get(), address.personalName.get()) &&
                Objects.equals(email.get(), address.email.get());
    }

    /**
     * Create a hashcode from the object
     *
     * The following fields are considered:
     *      "personalName"
     *      "email"
     *
     * @return A hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(personalName, email);
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                "personalName='" + personalName.get() + '\'' +
                ", email='" + email.get() + '\'' +
                '}';
    }
}
