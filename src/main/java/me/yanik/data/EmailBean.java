package me.yanik.data;

import javafx.beans.property.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

/**
 * An EmailBean is used to represent emails. It can represent emails that are about to be sent and
 * emails that are received.
 *
 * An EmailBean is only used to transport data as it has no functionality.
 *
 */
public class EmailBean {
    private int id = -1;
    private ObjectProperty<Address> from = new SimpleObjectProperty<>();
    private ObjectProperty<Address[]> to = new SimpleObjectProperty<>();
    private ObjectProperty<Address[]> cc = new SimpleObjectProperty<>();
    private ObjectProperty<Address[]> bcc = new SimpleObjectProperty<>();
    private StringProperty subject = new SimpleStringProperty();

    // By default messages are empty, this is to prevent errors (for example, textMessage or htmlMessage disappears)
    private StringProperty textMessage = new SimpleStringProperty("");
    private StringProperty htmlMessage = new SimpleStringProperty("");

    private ObjectProperty<Attachment[]> attachments = new SimpleObjectProperty<>();

    // By default, priority is -1 (i.e. not available)
    private IntegerProperty priority = new SimpleIntegerProperty(-1);

    // dateTime field not included in equals() or hashcode()
    // As Email might be the same but received at different times
    private ObjectProperty<LocalDateTime> dateReceived = new SimpleObjectProperty<>();

    // dateSent not included in equals() or hashcode() because
    // because received emails do not contain dateSent
    private ObjectProperty<LocalDateTime> dateSent = new SimpleObjectProperty<>();

    public EmailBean() { }

    public EmailBean(Address from, Address[] to, Address[] cc,
                     Address[] bcc, String subject, String textMessage,
                     String htmlMessage, Attachment[] attachments,
                     int priority) {
        this.from.set(from);
        this.to.set(to);
        this.cc.set(cc);
        this.bcc.set(bcc);
        this.subject.set(subject);
        this.textMessage.set(textMessage);
        this.htmlMessage.set(htmlMessage);
        this.attachments.set(attachments);
        this.priority.set(priority);
    }

    /**
     * Gets the id of the email
     * @return The id of the email (-1 if not in the database)
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the email
     * @param id The id of the email
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the email address of the sender
     * @return The email address of the sender
     */
    public Address getFrom() {
        return from.get();
    }

    /**
     * Set the email address  of the sender
     * @param from The email address  of the sender
     */
    public void setFrom(Address from) {
        this.from.set(from);
    }

    /**
     * Get the email addresses of the recipients
     * @return The email addresses of the recipients
     */
    public Address[] getTo() {
        return to.get();
    }

    /**
     * Set the email addresses of the recipients
     * @param to The email addresses of the recipients
     */
    public void setTo(Address[] to) {
        this.to.set(to);
    }

    /**
     * Get the email addresses of the CC (carbon copy) recipients
     * @return The email addresses of the CC (carbon copy) recipients
     */
    public Address[] getCc() {
        return cc.get();
    }

    /**
     * Set the email addresses of the CC (carbon copy) recipients
     * @param cc The email addresses of the CC (carbon copy) recipients
     */
    public void setCc(Address[] cc) {
        this.cc.set(cc);
    }

    /**
     * Get the email addresses of the BCC (blind carbon copy) recipients
     * Only possible to emails that are about to be sent, as blind carbon copy addresses
     * are not displayed in received emails
     * @return The email addresses of the BCC (blind carbon copy) recipients
     */
    public Address[] getBcc() {
        return bcc.get();
    }

    /**
     * Set the email addresses of the BCC recipients
     * @param bcc The email addresses of the BCC recipients
     */
    public void setBcc(Address[] bcc) {
        this.bcc.set(bcc);
    }

    /**
     * Get the subject of the email
     * @return The subject of the email
     */
    public String getSubject() {
        return subject.get();
    }

    /**
     * Set the subject of the email
     * @param subject The subject of the email
     */
    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    /**
     * Get the plain text message of the email
     * @return The plain text message of the email
     */
    public String getTextMessage() {
        return textMessage.get();
    }

    /**
     * Set the plain text message of the email
     * @param textMessage The plain text message of the email
     */
    public void setTextMessage(String textMessage) {
        this.textMessage.set(textMessage);
    }

    /**
     * Get the html message of the email
     * @return The html message of the email
     */
    public String getHtmlMessage() {
        return htmlMessage.get();
    }

    /**
     * Set the html message of the email
     * @param htmlMessage The html message of the email
     */
    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage.set(htmlMessage);
    }

    /**
     * Get all the normal (not embedded) attachments attached to the email
     * @return All the normal (not embedded) attachments attached to the email
     */
    public Attachment[] getAttachments() {
        return attachments.get();
    }

    /**
     * Set all the normal (not embedded) attachments that will be attached to the email
     * @param attachments All the normal (not embedded) attachments that will be attached to the email
     */
    public void setAttachments(Attachment[] attachments) {
        this.attachments.set(attachments);
    }

    /**
     * Get the priority (1-5), if not available the priority is -1
     * @return The priority (1-5) or if not available -1
     */
    public int getPriority() {
        return priority.get();
    }

    /**
     * Set the priority (1-5)
     * @param priority The priority to set (1-5)
     */
    public void setPriority(int priority) {
        this.priority.set(priority);
    }

    /**
     * Get the date and time of the received email
     * @return The date and time of the received email
     */
    public LocalDateTime getDateReceived() {
        return dateReceived.get();
    }

    /**
     * Set the date and time of the received email
     * @param dateReceived The date and time of the received email
     */
    public void setDateReceived(LocalDateTime dateReceived) {
        this.dateReceived.set(dateReceived);
    }

    /**
     * Get the date and time the email was sent
     * @return The date and time the email was sent
     */
    public LocalDateTime getDateSent() {
        return dateSent.get();
    }

    /**
     * Set the date and time the email was sent
     * @param dateSent The date and time the email was sent
     */
    public void setDateSent(LocalDateTime dateSent) {
        this.dateSent.set(dateSent);
    }

    public ObjectProperty<Address> fromProperty() {
        return from;
    }

    public ObjectProperty<Address[]> toProperty() {
        return to;
    }

    public ObjectProperty<Address[]> ccProperty() {
        return cc;
    }

    public ObjectProperty<Address[]> bccProperty() {
        return bcc;
    }

    public StringProperty subjectProperty() {
        return subject;
    }

    public StringProperty textMessageProperty() {
        return textMessage;
    }

    public StringProperty htmlMessageProperty() {
        return htmlMessage;
    }

    public ObjectProperty<Attachment[]> attachmentsProperty() {
        return attachments;
    }

    public IntegerProperty priorityProperty() {
        return priority;
    }

    public ObjectProperty<LocalDateTime> dateReceivedProperty() {
        return dateReceived;
    }

    public ObjectProperty<LocalDateTime> dateSentProperty() {
        return dateSent;
    }

    /**
     * Verify if an object is equal to this object
     *
     * The following fields are considered:
     *      "from"
     *      "to"
     *      "cc"
     *      "bcc"
     *      "subject"
     *      "textMessage"
     *      "htmlMessage"
     *      "attachments"
     *      "priority"
     *
     * @param o The object to compare
     * @return True if equals, false if not equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailBean emailBean = (EmailBean) o;
        return priority.get() == emailBean.priority.get() &&
                Objects.equals(from.get(), emailBean.from.get()) &&
                Arrays.equals(to.get(), emailBean.to.get()) &&
                Arrays.equals(cc.get(), emailBean.cc.get()) &&
                Arrays.equals(bcc.get(), emailBean.bcc.get()) &&
                Objects.equals(subject.get(), emailBean.subject.get()) &&
                Objects.equals(textMessage.get(), emailBean.textMessage.get()) &&
                Objects.equals(htmlMessage.get(), emailBean.htmlMessage.get()) &&
                Arrays.equals(attachments.get(), emailBean.attachments.get());
    }

    /**
     * Create a hashcode from the object
     *
     * The following fields are considered:
     *      "from"
     *      "to"
     *      "cc"
     *      "bcc"
     *      "subject"
     *      "textMessage"
     *      "htmlMessage"
     *      "attachments"
     *      "embeddedAttachments"
     *      "priority"
     *
     * @return A hashcode
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(from.get(), subject.get(), textMessage.get(), htmlMessage.get(), priority.get());
        result = 31 * result + Arrays.hashCode(to.get());
        result = 31 * result + Arrays.hashCode(cc.get());
        result = 31 * result + Arrays.hashCode(bcc.get());
        result = 31 * result + Arrays.hashCode(attachments.get());
        return result;
    }

    @Override
    public String toString() {
        return "EmailBean{" +
                "id=" + id +
                "from=" + from.get() +
                ", to=" + Arrays.toString(to.get()) +
                ", cc=" + Arrays.toString(cc.get()) +
                ", bcc=" + Arrays.toString(bcc.get()) +
                ", subject='" + subject.get() + '\'' +
                ", textMessage='" + textMessage.get() + '\'' +
                ", htmlMessage='" + htmlMessage.get() + '\'' +
                ", attachments=" + Arrays.toString(attachments.get()) +
                ", priority=" + priority.get() +
                ", dateReceived=" + dateReceived.get() +
                ", dateSent=" + dateSent.get() +
                '}';
    }
}
