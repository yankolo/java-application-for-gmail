package me.yanik.persistence;

import me.yanik.data.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The AttachmentDAOImpl fully implements the AttachmentDAO interface
 */
public class AttachmentDAOImpl implements AttachmentDAO{
    private final static Logger LOG = LoggerFactory.getLogger(AttachmentDAOImpl.class);

    private ConnectionManager connectionManager;

    public AttachmentDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * This method creates an attachment in the database
     *
     * @param attachment  The attachment to create
     * @param emailBeanId The ID of the email that the attachment is associated with
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    @Override
    public int createAttachment(Attachment attachment, int emailBeanId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Creating attachment in database: " + attachment);

            connection = connectionManager.openConnection();

            String sql = "INSERT INTO attachment(name, data, is_embedded, email_bean, content_id) VALUES " +
                    "(?, ?, ?, ?, ?)";

            PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pStatement.setString(1, attachment.getName());
            pStatement.setBytes(2, attachment.getContent());
            pStatement.setBoolean(3, attachment.isEmbedded());
            pStatement.setInt(4, emailBeanId);
            pStatement.setString(5, attachment.getContentId());

            int rowsAffected = pStatement.executeUpdate();

            LOG.info("Attachment created in database: " + attachment);
            LOG.info("Rows Affected: " + rowsAffected);

            ResultSet generatedKeys = pStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int generatedId = generatedKeys.getInt(1);
                attachment.setId(generatedId);
                LOG.info("ID of Attachment object set: " + attachment.getId());
            }

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when creating attachment in database: " + attachment;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * This method deletes an attachment from the database
     *
     * @param attachment The Attachment to delete
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if attachment id doesn't exist in the database or
     *                     failed to open connection
     */
    @Override
    public int deleteAttachment(Attachment attachment) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Deleting attachment from database: " + attachment);

            connection = connectionManager.openConnection();

            if (isAttachmentExists(attachment.getId()) == false) {
                String errorMessage = "Attachment with id " + attachment.getId() + " doesn't exist";
                LOG.error(errorMessage);
                throw new DaoException(errorMessage);
            }

            String sql = "DELETE FROM attachment WHERE attachment_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, attachment.getId());

            int rowsAffected = pStatement.executeUpdate();
            attachment.setId(-1);

            LOG.info("Attachment deleted from database: " + attachment);
            LOG.info("Rows Affected: " + rowsAffected);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when deleting attachment from database: " +
                    attachment;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Returns an Attachment object with the specified ID from the database
     *
     * @param id The ID of the Attachment
     * @return The Attachment with the specified ID
     * @throws DaoException Exception is thrown if failed to open connection
     */
    @Override
    public Attachment findAttachmentById(int id) throws DaoException {
        LOG.info("Starting to find attachment with ID " + id);

        Attachment attachment = retrieveAttachment(id);

        LOG.info("Finished finding attachment with ID " + id);

        return attachment;
    }

    /**
     * This method verifies if the attachment exists in the database
     *
     * @param attachmentId The ID of the attachment
     * @return True if attachment exists
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private boolean isAttachmentExists(int attachmentId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Verifying if attachment with id " + attachmentId + " exists in the database");

            connection = connectionManager.openConnection();

            String sql = "SELECT * FROM attachment " +
                    "WHERE attachment_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, attachmentId);

            ResultSet resultSet = pStatement.executeQuery();

            boolean attachmentExists = false;
            // Verifying if the first row exists (if true, attachment exists)
            if (resultSet.first() == true) {
                attachmentExists = true;
            }

            LOG.info("Attachment with id " + attachmentId + " exists: " + attachmentExists);

            return attachmentExists;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when verifying if attachment with id " +
                    attachmentId + " exists in the database";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to retrieve an attachment using an ID from the database
     *
     * @param attachmentId The ID of the attachment
     * @return The retrieved attachment (null if not found)
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private Attachment retrieveAttachment(int attachmentId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Retrieving attachment by ID: " + attachmentId);

            connection = connectionManager.openConnection();

            String sql = "SELECT name, data, is_embedded, content_id FROM attachment " +
                    "WHERE attachment_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, attachmentId);

            ResultSet resultSet = pStatement.executeQuery();

            Attachment attachment = null;
            if (resultSet.next()) {
                String name = resultSet.getString(1);
                byte[] data = resultSet.getBytes(2);
                boolean isEmbedded = resultSet.getBoolean(3);
                String contentId = resultSet.getString(4);

                attachment = new Attachment();
                attachment.setId(attachmentId);
                attachment.setName(name);
                attachment.setContent(data);
                attachment.setEmbedded(isEmbedded);
                attachment.setContentId(contentId);
            }

            LOG.info("Retrieved attachment by ID: " + attachment);

            return attachment;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding attachment by ID: " + attachmentId;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }
}
