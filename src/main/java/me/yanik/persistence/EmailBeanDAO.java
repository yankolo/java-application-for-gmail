package me.yanik.persistence;

import me.yanik.data.EmailBean;
import me.yanik.data.Folder;

import java.util.List;

/**
 * The EmailBeanDAO interface defines all the methods that
 * interact with the email_bean entity in the database.
 */
public interface EmailBeanDAO {
    /**
     * This method creates an EmailBean object in the database
     *
     * @param emailBean The EmailBean object to create in the database
     * @param folderId The ID of the folder
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    public int createEmailBean(EmailBean emailBean, int folderId) throws DaoException;

    /**
     * This method deletes an EmailBean object from the database
     *
     * @param emailBean The EmailBean object to delete from the database
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if the EmailBean ID doesn't
     *                      exist in the database
     */
    public int deleteEmailBean(EmailBean emailBean) throws DaoException;

    /**
     * Returns an EmailBean with the specified ID from the database
     *
     * @param id The ID of the EmailBean
     * @return The EmailBean that has the specified ID
     * @throws DaoException Exception is thrown if email bean is not found or unable to open connection
     */
    public EmailBean findEmailBeanById(int id) throws DaoException;

    /**
     * This method verifies if an email bean with the specified ID
     * exists in the database
     *
     * @param emailBeanId The ID of the email bean
     * @return True if email bean exists
     * @throws DaoException Exception is thrown if failed to open connection
     */
    public boolean isEmailBeanExists(int emailBeanId) throws DaoException;

    /**
     * Changes the folder of the email (foreign key to folder)
     *
     * @param emailBeanId The Id of the email bean
     * @param folderId The new folder id
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if email bean doesn't exist or failed to open connection
     */
    public int changeEmailBeanFolder(int emailBeanId, int folderId) throws DaoException;
}
