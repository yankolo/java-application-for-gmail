package me.yanik.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * The ConnectionManager class provides an easy way to use JDBC connections
 *
 * The reasons why it was decided to use the ConnectionManager to open and close connections
 * instead of doing it in try-with-resources blocks are the following:
 * 1) It allows to log when there is an error with opening/closing connections
 * 2) It centralizes the opening/closing of connections (that also means that there is
 * only one place that has access to the credentials)
 */
public class ConnectionManager {
    private final static Logger LOG = LoggerFactory.getLogger(ConnectionManager.class);

    private final String URL;
    private final String USER;
    private final String PASSWORD;

    private ResourceBundle resourceBundle;

    public ConnectionManager(String url, String user, String password) {
        this.resourceBundle = ResourceBundle.getBundle("ClientBundle");

        this.URL = url;
        this.USER = user;
        this.PASSWORD = password;
    }

    /**
     * This method returns a new JDBC connection
     * @return A new JDBC connection
     * @throws SQLException Exception is thrown if the method couldn't open
     * a new JDBC connection
     */
    public Connection openConnection() throws DaoException {
        try {
            LOG.info("Opening database connection");
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            LOG.info("Database connection successfully opened");

            return connection;
        } catch (SQLException e) {
            String errorMessage = resourceBundle.getString("open_database_error");
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }
    }

    /**
     * This method closes a JDBC connection. If the connection couldn't be closed,
     * a RunTimeException is thrown
     * @param connection The connection to close
     */
    public void closeConnection(Connection connection) {
        try {
            LOG.info("Closing database connection");
            connection.close();
            LOG.info("Database connection closed");
        } catch (SQLException e) {
            String errorMessage = resourceBundle.getString("close_database_error");
            LOG.error(errorMessage + e);
            throw new RuntimeException(errorMessage, e);
        }
    }
}
