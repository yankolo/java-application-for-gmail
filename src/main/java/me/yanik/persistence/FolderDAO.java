package me.yanik.persistence;

import me.yanik.data.EmailBean;
import me.yanik.data.Folder;

import java.util.List;

/**
 * The FolderDAO interface defines all the methods that
 * interact with folder entities in the database.
 */
public interface FolderDAO {
    /**
     * This method creates a folder in the database with all the
     * EmailBean objects that it contains. The ID of the folder object will be
     * set to the generated ID in the database.
     *
     * @param folder The folder to create
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder name already taken or
     * failed to open connection
     */
    public int createFolder(Folder folder) throws DaoException;

    /**
     * This method changes the name of the folder. The name of the Folder
     * object will be also changed.
     *
     * @param folder        The folder to update
     * @param newFolderName The new name for the folder
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist, new name is taken, or
     * unable to open connection
     */
    public int changeFolderName(Folder folder, String newFolderName) throws DaoException;

    /**
     * This method updates the EmailBean array of the folder (refreshes it)
     *
     * @param folder The folder to update
     * @throws DaoException Exception is thrown if folder doesn't exist or failed to open connection
     */
    public void reloadEmailsInFolder(Folder folder) throws DaoException;

    /**
     * This method deletes a folder and all the EmailBean objects that
     * it contains from the database
     *
     * @param folder The folder to delete
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist or if failed to open connection
     */
    public int deleteFolder(Folder folder) throws DaoException;

    /**
     * Returns a folder with the specified ID. The EmailBean array of the folder
     * will be set
     *
     * @param id The ID of the folder
     * @return The folder with the specified ID
     * @throws DaoException Exception is thrown if folder doesn't exist or unable to open connection
     */
    public Folder findFolderById(int id) throws DaoException;

    /**
     * Returns a list of all folders. The EmailBean arrays of the folders are all set
     * List will be empty if none are found.
     *
     * @return The list of all folders
     * @throws  DaoException Exception is thrown if failed to open connection.
     */
    public List<Folder> findAllFolders() throws DaoException;

    /**
     * Returns a folder with the specified name. The EmailBean array of the folder
     * will be set
     *
     * @param name The name to search
     * @return The folder with the specified name (null if not found)
     * @throws DaoException Exception is thrown if unable to open connection
     */
    public Folder findFolderByName(String name) throws DaoException;

    /**
     * Verifies if a folder with the specified ID
     * exists in the database
     *
     * @param folderId The ID of the folder
     * @return True if folder exists
     */
    public boolean isFolderExists(int folderId) throws DaoException;

    /**
     * Verifies if a folder name exists
     *
     * @param folderName The folder name to search for
     * @return True if the folder name is taken
     * @throws DaoException Exception is thrown if failed to open connection
     */
    public boolean isFolderNameExists(String folderName) throws DaoException;
}
