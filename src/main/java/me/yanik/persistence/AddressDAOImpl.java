package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The AddressDAOImpl fully implements the AddressDAO interface
 */
public class AddressDAOImpl implements AddressDAO {
    private final static Logger LOG = LoggerFactory.getLogger(AddressDAOImpl.class);

    private ConnectionManager connectionManager;

    public AddressDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * This method crates an address in the database if it doesn't exist and
     * associates (links) to an email_bean entity
     * in the email_address table (bridging table between address and emailBean)
     *
     * @param address     The address to associate
     * @param emailBeanId The email_bean to associate the address to
     * @param addressType The type of the address (from, to, cc, or bcc)
     * @return Returns the number of rows affected
     */
    @Override
    public int createAddress(Address address, int emailBeanId, String addressType) throws DaoException{
            int totalRrowsAffected = 0;

            LOG.info("Starting creation of address in database: " + address +
                    " associated with email_bean id: " + emailBeanId +
                    " with address type: " + addressType);

            if (isAddressExists(address.getEmail()) == true) {
                int addressId = findIdOfAddress(address);
                LOG.info("Setting ID " + addressId + " to Address object");
                address.setId(addressId);
            } else {
                totalRrowsAffected += createOnlyAddressEntity(address);
            }

            if (isEmailAddressExists(address.getId(), emailBeanId, addressType)) {
                String errorMessage = "Email address (addressId: " + address.getId() +
                    ", emailBeanId: " + emailBeanId + ", type: " + addressType + ") already exists in database";
                LOG.error(errorMessage);
                throw new DaoException(errorMessage);
            }

            totalRrowsAffected += insertEmailAddressLink(emailBeanId, address.getId(), addressType);

            LOG.info("Finished creation of address in database: " + address +
                " associated with email_bean id: " + emailBeanId +
                " with address type: " + addressType);
            LOG.info("Total rows affected: " + totalRrowsAffected);

            return totalRrowsAffected;
    }

    /**
     * This method deletes a link between the specified an Address and an
     * EmailBean from the email_address table (bridging table between address and email_bean).
     * It also deletes the address from the address table if there are no more references to it;
     *
     * @param address     The address associated with the EmailBean
     * @param emailBeanId The EmailBean associated with the address
     * @param addressType The type of the address (from, to, cc, or bcc)
     * @return Returns the number of rows affected
     */
    @Override
    public int deleteAddress(Address address, int emailBeanId, String addressType) throws DaoException {
        int totalRowsAffected = 0;

        LOG.info("Starting deletion of address from database (address: " + address + " , emailBeanId: "
                + emailBeanId + ", addressType: " + addressType + ")");

        if(isEmailAddressExists(address.getId(), emailBeanId, addressType) == false) {
            String errorMessage = "Email address (addressId: " + address.getId() +
                    ", emailBeanId: " + emailBeanId + ", type: " + addressType + ") does not exist in database";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        totalRowsAffected += deleteEmailAddress(address.getId(), emailBeanId, addressType);

        if (isAddressSafeToDelete(address)) {
            totalRowsAffected += deleteOnlyAddressEntity(address);
        }

        LOG.info("Finished deleting address from database (address: " + address + " , emailBeanId: "
                + emailBeanId + ", addressType: " + addressType + ")");
        LOG.info("Total rows affected: " + totalRowsAffected);

        return totalRowsAffected;
    }

    /**
     * This method finds addresses with the specified address type that
     * are associated with the specified EmailBean.
     * The list will be null if none are found.
     *
     * @param emailBeanId The ID of the EmailBean associated with the addresses
     * @param addressType The type of the address (from, to, cc, or bcc)
     * @return A list of addresses associated with the specified email bean with the specified
     * address type
     * @throws DaoException Exception is thrown if failed to open session
     */
    @Override
    public List<Address> findEmailAddresses(int emailBeanId, String addressType) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Retrieving all '" + addressType + "' addresses with email_bean id: " + emailBeanId);

            connection = connectionManager.openConnection();

            String sql = "SELECT address FROM email_address WHERE email_bean = ? AND type = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, emailBeanId);
            pStatement.setString(2, addressType);

            ResultSet resultSet = pStatement.executeQuery();

            List<Address> addresses = null;
            while(resultSet.next()) {
                if (addresses == null)
                    addresses = new ArrayList<>();

                int addressId = resultSet.getInt(1);

                addresses.add(findAddressById(addressId));
            }

            LOG.info("Addresses (email_bean id: " + emailBeanId + ", " +
                    "type: " + addressType + ") retrieved:" + addresses);

            return addresses;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding " + addressType + " addresses " +
                    "with email_bean id: " + emailBeanId;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Returns an Address object that represents with the
     * specified ID
     *
     * @param id The ID of the Address
     * @return The Address with the specified ID
     * @throws DaoException Exception is thrown if failed to open session
     */
    @Override
    public Address findAddressById(int id) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Retrieving address by id: " + id);

            connection = connectionManager.openConnection();

            String sql = "SELECT email, name FROM address WHERE address_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, id);

            ResultSet resultSet = pStatement.executeQuery();
            Address address = null;
            if (resultSet.next()) {
                String email = resultSet.getString(1);
                String name = resultSet.getString(2);

                address = new Address();
                address.setId(id);
                address.setEmail(email);
                address.setPersonalName(name);
            }

            LOG.info("Retrieved address by id (" + id + "): " + address);

            return address;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding address by id: " + id;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * This helper method verifies if an address with the specified email
     * already exists in the database. Possible because the email column in
     * the address table is unique.
     *
     * @param email The email of the address
     * @return True if the address exists
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private boolean isAddressExists(String email) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Verifying if address with email " + email + " exists in the database");

            connection = connectionManager.openConnection();

            String sql = "SELECT * FROM address " +
                    "WHERE email = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, email);

            ResultSet resultSet = pStatement.executeQuery();

            boolean addressExists = false;
            // Verifying if the first row exists (if true address email exists)
            if (resultSet.first() == true) {
                addressExists = true;
            }

            LOG.info("Address with email " + email + " exists: " + addressExists);

            return addressExists;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error while verifying if address with email " +
                    email + " exists in the database";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * This helper method verifies if an an email address (an entry in the the bridging table
     * that has an email_bean id and a type) exists in the database
     *
     * @param addressId The ID of the address
     * @param emailBeanId The ID of the EmailBean
     * @param type The type of the address (from, to, cc, or bcc)
     * @return True if the email address exists
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private boolean isEmailAddressExists(int addressId, int emailBeanId, String type) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Verifying if email address (addressId: " + addressId +
                    ", emailBeanId: " + emailBeanId + ", type: " + type + ") exists in the database");

            connection = connectionManager.openConnection();

            String sql = "SELECT * FROM email_address " +
                    "WHERE address = ? AND email_bean = ? AND type = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, addressId);
            pStatement.setInt(2, emailBeanId);
            pStatement.setString(3, type);

            ResultSet resultSet = pStatement.executeQuery();

            boolean emailAddressExists = false;
            // Verifying if the first row exists (if true, address id exists)
            if (resultSet.first() == true) {
                emailAddressExists = true;
            }

            LOG.info("Email address (addressId: " + addressId + " , emailBeanId: "
                    + emailBeanId + ", type: " + type + ") exists: " + emailAddressExists);

            return emailAddressExists;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error while verifying if email address " +
                    "(addressId: " + addressId + " , emailBeanId: " + emailBeanId + ", type: " + type + ")" +
                    " exists in the database";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to create only an address entity in the database
     *
     * @param address The address to create
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int createOnlyAddressEntity(Address address) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Creating only address entity: " + address);

            connection = connectionManager.openConnection();

            String sql = "INSERT INTO address(email,name) VALUES (?, ?)";
            PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pStatement.setString(1, address.getEmail());
            pStatement.setString(2, address.getPersonalName());

            int rowsAffected = pStatement.executeUpdate();

            LOG.info("Address entity created in database: " + address);
            LOG.info("Rows Affected: " + rowsAffected);

            ResultSet generatedKeys = pStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int generatedId = generatedKeys.getInt(1);
                address.setId(generatedId);
                LOG.info("ID of Address object set: " + generatedId);
            }

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when creating only address entity: " + address +
                    " in the database";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to find ID of address
     * It is expected that the address exists is in the
     * database
     * It is possible to find the ID of an address using an email
     * because email field is unique in address table.
     *
     * @param address The address to find the
     * @return The ID of the address
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int findIdOfAddress(Address address) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Finding the ID of address: " + address);

            connection = connectionManager.openConnection();

            String sql = "SELECT address_id FROM address WHERE email = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, address.getEmail());

            ResultSet resultSet = pStatement.executeQuery();

            int addressId = -1;
            if (resultSet.next()) {
                addressId = resultSet.getInt(1);
                LOG.info("Found ID (" + addressId + ") of address: " + address);
            }

            return addressId;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding ID of address: " + address;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to delete an email address (entry in the the bridging table
     * that has an email_bean id, address, and a type)
     *
     * @param addressId The ID of the address
     * @param emailBeanId The ID of the email bean
     * @param type The type of the address (from, to, cc, or bcc)
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int deleteEmailAddress(int addressId, int emailBeanId, String type) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Deleting email address from database " +
                    "(addressId: " + addressId + " , emailBeanId: " + emailBeanId + ", type: " + type + ")");

            connection = connectionManager.openConnection();

            String sql = "DELETE FROM email_address WHERE address = ? AND email_bean = ? AND type = ?";

            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, addressId);
            pStatement.setInt(2, emailBeanId);
            pStatement.setString(3, type);

            int rowsAffected = pStatement.executeUpdate();
            LOG.info("Deleted email address from database " +
                    "(addressId: " + addressId + " , emailBeanId: " + emailBeanId + ", type: " + type + ")");
            LOG.info("Rows affected: " + rowsAffected);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when deleting email address: " +
                    "(addressId: " + addressId + " , emailBeanId: " + emailBeanId + ", type: " + type + ")";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to delete only the address entity from the database
     *
     * @param address The address to delete
     * @return Number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int deleteOnlyAddressEntity(Address address) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Deleting address from database: " + address);

            connection = connectionManager.openConnection();

            String sql = "DELETE FROM address WHERE address_id = ?";

            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, address.getId());

            int rowsAffected = pStatement.executeUpdate();

            address.setId(-1);
            LOG.info("Deleted address from database: " + address);
            LOG.info("Rows affected: " + rowsAffected);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when deleting address: " + address;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to verify if the address is safe to delete
     * (i.e. there are no more references to it in the email_address bridging table)
     *
     * @param address The address to check
     * @return True if the address is safe to delete
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private boolean isAddressSafeToDelete(Address address) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Verifying if address is safe to delete: " + address);

            connection = connectionManager.openConnection();

            String sql = "SELECT * FROM address a " +
                    "JOIN email_address ea ON a.address_id = ea.address " +
                    "WHERE address_id = ?";

            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, address.getId());

            ResultSet resultSet = pStatement.executeQuery();

            boolean safeToDelete = false;
            if (resultSet.next() == false) {
                    safeToDelete = true;
            }

            LOG.info("Address safe to delete: " + safeToDelete);

            return safeToDelete;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when verifying if address is safe to delete: " + address;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * Helper method to insert an email_address (data in email_address bridging table)
     * @param emailBeanId The ID of the email bean
     * @param addressId The ID of the address
     * @param addressType The type of the address (from, to, cc, or bcc)
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int insertEmailAddressLink(int emailBeanId, int addressId, String addressType) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Inserting email_address (emailBeanId: " + emailBeanId + " , addressId: "
                    + addressId + ", addressType: " + addressType + ")");

            connection = connectionManager.openConnection();

            String sql = "INSERT INTO email_address (email_bean, address, type) values(?, ?, ?)";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, emailBeanId);
            pStatement.setInt(2, addressId);
            pStatement.setString(3, addressType);

            int rowsAffected = pStatement.executeUpdate();

            LOG.info("Inserted email_address (emailBeanId: " + emailBeanId + " , addressId: "
                    + addressId + ", addressType: " + addressType + ")");
            LOG.info("Rows affected: " + rowsAffected);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when inserting email address  " +
                    "(emailBeanId: " + emailBeanId + " , addressId: "
                    + addressId + ", addressType: " + addressType + ")";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }
}
