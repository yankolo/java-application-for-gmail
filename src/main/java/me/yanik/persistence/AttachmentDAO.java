package me.yanik.persistence;

import me.yanik.data.Attachment;

import java.util.List;

/**
 * The AttachmentDAO interface defines all the methods that
 * interact with the attachment entity in the database.
 */
public interface AttachmentDAO {
    /**
     * This method creates an attachment in the database
     *
     * @param attachment  The attachment to create
     * @param emailBeanId The ID of the email that the attachment is associated with
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    public int createAttachment(Attachment attachment, int emailBeanId) throws DaoException;

    /**
     * This method deletes an attachment from the database
     *
     * @param attachment The Attachment to delete
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if attachment id doesn't exist in the database or
     *                     failed to open connection
     */
    public int deleteAttachment(Attachment attachment) throws DaoException;

    /**
     * Returns an Attachment object with the specified ID from the database
     *
     * @param id The ID of the Attachment
     * @return The Attachment with the specified ID
     * @throws DaoException Exception is thrown if failed to open connection
     */
    public Attachment findAttachmentById(int id) throws DaoException;
}
