package me.yanik.persistence;

import jodd.mail.Email;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The FolderDAOImpl fully implements the FolderDAO interface
 */
public class FolderDAOImpl implements FolderDAO{
    private final static Logger LOG = LoggerFactory.getLogger(FolderDAOImpl.class);

    private ConnectionManager connectionManager;
    private EmailBeanDAO emailBeanDAO;

    private ResourceBundle resourceBundle;

    public FolderDAOImpl(ConnectionManager connectionManager, EmailBeanDAO emailBeanDAO) {
        this.resourceBundle = ResourceBundle.getBundle("ClientBundle");

        this.connectionManager = connectionManager;
        this.emailBeanDAO = emailBeanDAO;
    }

    /**
     * This method creates a folder in the database with all the
     * EmailBean objects that it contains. The ID of the folder object will be
     * set to the generated ID in the database.
     *
     * @param folder The folder to create
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder name already taken or
     * failed to open connection
     */
    @Override
    public int createFolder(Folder folder) throws DaoException {
         int totalRowsAffected = 0;

         LOG.info("Starting to create folder");

         if (isFolderNameExists(folder.getName())) {
             String errorMessage = String.format(resourceBundle.getString("folder_already_exists"), folder.getName());
             LOG.error(errorMessage);
             throw new DaoException(errorMessage);
         }

         totalRowsAffected =+ createOnlyFolder(folder);
         totalRowsAffected += createEmailBeans(folder.getEmailBeans(), folder.getId());

         LOG.info("Finished creating folder");
         LOG.info("Total rows affected: " + totalRowsAffected);

         return totalRowsAffected;
    }

    /**
     * This method changes the name of the folder. The name of the Folder
     * object will be also changed.
     *
     * @param folder        The folder to update
     * @param newFolderName The new name for the folder
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist, new name is taken, or
     * unable to open connection
     */
    @Override
    public int changeFolderName(Folder folder, String newFolderName) throws DaoException {
        LOG.info("Start of folder name change");

        if (isFolderExists(folder.getId()) == false) {
            String errorMessage = "Folder with id " + folder.getId() + " doesn't exist";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        if (isFolderNameExists(newFolderName) == true) {
            String errorMessage = String.format(resourceBundle.getString("folder_name_already_taken"), newFolderName);
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        if (isFolderRenamable(folder.getId()) == false) {
            String errorMessage = "Folder with id " + folder.getId() + " is not renamable";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        int rowsAffected = renameFolder(folder, newFolderName);

        LOG.info("End of folder name change");

        return rowsAffected;
    }

    /**
     * This method updates the EmailBean array of the folder
     *
     * @param folder The folder to update
     * @throws DaoException Exception is thrown if folder doesn't exist or failed to open connection
     */
    @Override
    public void reloadEmailsInFolder(Folder folder) throws DaoException {
        if (isFolderExists(folder.getId()) == false) {
            String errorMessage = "Folder with id " + folder.getId() + " doesn't exist";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        reloadEmailsWithoutValidation(folder);
    }

    /**
     * This method deletes a folder and all the EmailBean objects that
     * it contains from the database
     *
     * @param folder The folder to delete
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist or if failed to open connection
     */
    @Override
    public int deleteFolder(Folder folder) throws DaoException {
        int totalRowsAffected = 0;

        LOG.info("Starting to delete folder from database");

        if (isFolderExists(folder.getId()) == false) {
            String errorMessage = "Folder with id " + folder.getId() + " doesn't exist";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        if (isFolderDeletable(folder.getId()) == false) {
            String errorMessage = "Folder with id " + folder.getId() + " is not deletable";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        totalRowsAffected += deleteEmailBeans(folder.getEmailBeans());
        totalRowsAffected += deleteOnlyFolder(folder);

        LOG.info("Ended to delete folder from database");

        return totalRowsAffected;
    }

    /**
     * Returns a folder with the specified ID. The EmailBean array of the folder
     * will be set
     *
     * @param id The ID of the folder
     * @return The folder with the specified ID
     * @throws DaoException Exception is thrown if folder doesn't exist or unable to open connection
     */
    @Override
    public Folder findFolderById(int id) throws DaoException {
        LOG.info("Starting to retrieve folder by ID " + id);

        Folder folder = retrieveOnlyFolder(id);

        if (folder != null) {
            List<EmailBean> emailBeans = findEmailBeansInFolder(id);
            EmailBean[] emailBeansArray = emailBeans.toArray(new EmailBean[emailBeans.size()]);

            folder.setEmailBeans(emailBeansArray);
        }

        LOG.info("Finished to retrieve folder by ID " + id + ": " + folder);

        return folder;
    }

    /**
     * Returns a list of all folders. The EmailBean arrays of the folders are all set
     * List will be empty if none are found.
     *
     * @return The list of all folders
     * @throws DaoException Exception is thrown if failed to open connection.
     */
    @Override
    public List<Folder> findAllFolders() throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Starting the retrieval of all the folders in the database");

            connection = connectionManager.openConnection();

            String sql = "SELECT folder_id FROM folder ORDER BY folder_id ASC";
            PreparedStatement pStatement = connection.prepareStatement(sql);

            ResultSet resultSet = pStatement.executeQuery();

            List<Folder> folders = new ArrayList<Folder>();
            while(resultSet.next()) {
                int folderId = resultSet.getInt(1);

                folders.add(findFolderById(folderId));
            }

            LOG.info("Finished retrieval of all the folders. Retrieved " + folders.size());

            return folders;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding all folders";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Returns a folder with the specified name. The EmailBean array of the folder
     * will be set
     *
     * @param name The name to search
     * @return The folder with the specified name (null if not found)
     * @throws DaoException Exception is thrown if unable to open connection
     */
    @Override
    public Folder findFolderByName(String name) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Starting the retrieval of folder by name: " + name);

            connection = connectionManager.openConnection();

            String sql = "SELECT folder_id FROM folder WHERE name = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, name);

            ResultSet resultSet = pStatement.executeQuery();

            Folder folder = null;
            if(resultSet.next()) {
                int folderId = resultSet.getInt(1);
                folder = findFolderById(folderId);
            }

            LOG.info("Finished the retrieval of folder by name: " + folder);

            return folder;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding all folders";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Verifies if a folder with the specified ID
     * exists in the database
     *
     * @param folderId The ID of the folder
     * @return True if folder exists
     */
    @Override
    public boolean isFolderExists(int folderId) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Verifying if folder (id: " + folderId + ") exists in the database");

            connection = connectionManager.openConnection();

            String sql = "SELECT * FROM folder " +
                    "WHERE folder_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, folderId);

            ResultSet resultSet = pStatement.executeQuery();

            boolean folderExists = false;
            // Verifying if the first row exists (if true, folder exists)
            if (resultSet.first() == true) {
                folderExists = true;
            }

            LOG.info("Folder with id " + folderId + " exists: " + folderExists);

            return folderExists;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error while verifying if folder id (" +
                    folderId + ") exists in the database";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Verifies if a folder name exists
     *
     * @param folderName The folder name to search for
     * @return True if the folder name is taken
     * @throws DaoException Exception is thrown if failed to open connection
     */
    @Override
    public boolean isFolderNameExists(String folderName) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Verifying if folder name (" + folderName + ") exists");

            connection = connectionManager.openConnection();

            String sql = "SELECT * FROM folder " +
                    "WHERE name = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, folderName);

            ResultSet resultSet = pStatement.executeQuery();

            boolean folderNameExists = false;
            // Verifying if the first row exists (if true, folder name is taken)
            if (resultSet.first() == true) {
                folderNameExists = true;
            }

            LOG.info("folder name (" + folderName + ") exists: " + folderNameExists);

            return folderNameExists;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when verifying if folder name (" + folderName +
                    ") exists";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to retrieve only the folder entity from database
     * (without email beans)
     *
     * @param folderId The ID of the folder
     * @return The folder retrieved (null if not found)
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private Folder retrieveOnlyFolder(int folderId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Retrieving only folder by id: " + folderId);

            connection = connectionManager.openConnection();

            String sql = "SELECT name, is_deletable, is_renamable FROM folder WHERE folder_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, folderId);

            ResultSet resultSet = pStatement.executeQuery();
            Folder folder = null;
            if (resultSet.next()) {
                String name = resultSet.getString(1);
                boolean isDeletable = resultSet.getBoolean(2);
                boolean isRenamable = resultSet.getBoolean(3);

                folder = new Folder();
                folder.setId(folderId);
                folder.setName(name);
                folder.setDeletable(isDeletable);
                folder.setRenamable(isRenamable);
            }

            LOG.info("Retrieved only folder by id: " + folder);

            return folder;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding only folder by id: " + folderId;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to delete only a folder entity from the database
     * (doesn't delete email beans)
     *
     * @param folder The Folder to delete
     * @return The number of rows affected
     * @throws DaoException And exception is thrown if failed to open connection
     */
    private int deleteOnlyFolder(Folder folder) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Deleting only folder from database: " + folder);

            connection = connectionManager.openConnection();

            String sql = "DELETE FROM folder WHERE folder_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, folder.getId());
            int rowsAffected = pStatement.executeUpdate();

            folder.setId(-1);
            LOG.info("Deleted folder from database: " + folder);
            LOG.info("Rows affected: " + rowsAffected);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when deleting only folder: " + folder;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to create only a folder entity in the database
     *
     * @param folder The folder to create
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int createOnlyFolder(Folder folder) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Creating folder in database: " + folder);

            connection = connectionManager.openConnection();

            String name = folder.getName();
            boolean isDeletable = folder.isDeletable();
            boolean isRenamable = folder.isRenamable();

            String sql = "INSERT INTO folder(name, is_deletable, is_renamable)" +
                    " VALUES (?, ?, ?)";
            PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pStatement.setString(1, name);
            pStatement.setBoolean(2, isDeletable);
            pStatement.setBoolean(3, isRenamable);

            int rowsAffected = pStatement.executeUpdate();
            LOG.info("Folder created in database: " + folder);
            LOG.info("Rows Affected: " + rowsAffected);

            ResultSet generatedKeys = pStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int generatedId = generatedKeys.getInt(1);
                folder.setId(generatedId);
                LOG.info("ID of Folder object set: " + generatedId);
            }

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when creating folder: " + folder;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to create email beans in the database
     *
     * @param emailBeans The email beans to create
     * @param folderId The folder id of the email
     * @return The total rows affected
     * @throws DaoException Exception is thrown if failed to open connection or error creating beans
     */
    private int createEmailBeans(EmailBean[] emailBeans, int folderId) throws DaoException {
        int totalRowsAffected  = 0;
        for (EmailBean emailBean: emailBeans) {
            totalRowsAffected += emailBeanDAO.createEmailBean(emailBean, folderId);
        }
        return totalRowsAffected;
    }

    /**
     * Helper method to delete many email beans
     *
     * @param emailBeans The email beans to delete
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection or error when deleting
     * email beans
     */
    private int deleteEmailBeans(EmailBean[] emailBeans) throws DaoException {
        int totalRowsAffected = 0;
        for (EmailBean emailBean: emailBeans) {
            totalRowsAffected += emailBeanDAO.deleteEmailBean(emailBean);
        }
        return totalRowsAffected;
    }

    /**
     * Helper method to rename a folder. Renames folder in database and
     * updates the Folder object.
     *
     * @param folder The folder to rename
     * @param newFolderName The new name of the folder
     * @return The number of rows affected
     */
    private int renameFolder(Folder folder, String newFolderName) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Renaming folder (new name " + newFolderName + ") :" + folder);

            connection = connectionManager.openConnection();

            String sql = "UPDATE folder SET name = ? WHERE folder_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, newFolderName);
            pStatement.setInt(2, folder.getId());

            int rowsAffected = pStatement.executeUpdate();
            folder.setName(newFolderName);
            LOG.info("Renamed folder (new name " + newFolderName + "): " + folder);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when renaming folder (new name " +
                    newFolderName + "): " + folder;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Private method that returns a list of email beans that are in the specified folder
     *
     * @param folderId The ID of the folder
     * @return A list of emil beans that are in the specified folder
     * @throws DaoException Exception if unable to open connection
     */
    private List<EmailBean> findEmailBeansInFolder(int folderId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Retrieving all email beans in folder id: " + folderId);

            connection = connectionManager.openConnection();

            String sql = "SELECT email_id FROM email_bean WHERE folder_id = ? ORDER BY received_date DESC";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, folderId);

            ResultSet resultSet = pStatement.executeQuery();

            List<EmailBean> emailBeans = new ArrayList<EmailBean>();
            while(resultSet.next()) {
                int emailBeanId = resultSet.getInt(1);

                emailBeans.add(emailBeanDAO.findEmailBeanById(emailBeanId));
            }

            LOG.info("Number of email beans (folder id: " + folderId + ") retrieved:" + emailBeans.size());

            return emailBeans;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding email beans in folder id: " + folderId;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to refresh the Email Bean array in the folder
     * without validation (without checking if the folder exists)
     *
     * @param folder The folder to reload
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private void reloadEmailsWithoutValidation(Folder folder) throws DaoException{
        LOG.info("Updating email bean array in folder: " + folder.getName());

        List<EmailBean> beansInFolder = findEmailBeansInFolder(folder.getId());
        EmailBean[] beansInFolderArray = beansInFolder.toArray(new EmailBean[beansInFolder.size()]);
        folder.setEmailBeans(beansInFolderArray);

        LOG.info("Updated email bean array in folder: " + folder.getName());
    }

    /**
     * Helper method to verify if a folder is deletable
     *
     * @param folderId The folder id to verify
     * @return True if the folder is deletable
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private boolean isFolderDeletable(int folderId) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Verifying if folder (id: " + folderId + ") is deletable");

            connection = connectionManager.openConnection();

            String sql = "SELECT is_deletable FROM folder " +
                    "WHERE folder_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, folderId);

            ResultSet resultSet = pStatement.executeQuery();

            boolean folderDeletable = false;
            if (resultSet.next()) {
                folderDeletable = resultSet.getBoolean(1);
            }

            LOG.info("Folder with id " + folderId + " is deletable: " + folderDeletable);

            return folderDeletable;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error while verifying if folder id (" +
                    folderId + ") is deletable";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to verify if a folder is renamable
     *
     * @param folderId The folder id to verify
     * @return True if the folder is renamable
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private boolean isFolderRenamable(int folderId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Verifying if folder (id: " + folderId + ") is renamable");

            connection = connectionManager.openConnection();

            String sql = "SELECT is_renamable FROM folder " +
                    "WHERE folder_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, folderId);

            ResultSet resultSet = pStatement.executeQuery();

            boolean folderRenamable = false;
            if (resultSet.next()) {
                folderRenamable = resultSet.getBoolean(1);
            }

            LOG.info("Folder with id " + folderId + " is renamable: " + folderRenamable);

            return folderRenamable;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error while verifying if folder id (" +
                    folderId + ") is renamable";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }
}
