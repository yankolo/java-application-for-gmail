package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The EmailBeanDAOImpl fully implements the EmailBeanDAO interface
 */
public class EmailBeanDAOImpl implements EmailBeanDAO {
    private final static Logger LOG = LoggerFactory.getLogger(EmailBeanDAOImpl.class);

    private ConnectionManager connectionManager;
    private AddressDAO addressDAO;
    private AttachmentDAO attachmentDAO;

    public EmailBeanDAOImpl(ConnectionManager connectionManager, AddressDAO addressDAO, AttachmentDAO attachmentDAO) {
        this.connectionManager = connectionManager;
        this.addressDAO = addressDAO;
        this.attachmentDAO = attachmentDAO;
    }

    /**
     * This method creates an EmailBean object in the database
     *
     * @param emailBean The EmailBean object to create in the database
     * @param folderId The ID of the folder
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    @Override
    public int createEmailBean(EmailBean emailBean, int folderId) throws DaoException {
        int totalRowsAffected = 0;

        LOG.info("Creating email bean in database (folderId: " + folderId + "): " + emailBean);

        totalRowsAffected =+ createOnlyEmailBean(emailBean, folderId);

        totalRowsAffected =+ createFromAddress(emailBean.getFrom(), emailBean.getId());
        totalRowsAffected =+ createAddresses(emailBean.getTo(), emailBean.getId(), "to");
        totalRowsAffected =+ createAddresses(emailBean.getCc(), emailBean.getId(), "cc");
        totalRowsAffected =+ createAddresses(emailBean.getBcc(), emailBean.getId(), "bcc");
        totalRowsAffected =+ createAttachments(emailBean.getAttachments(), emailBean.getId());

        LOG.info("Fully created email bean in database: " + emailBean);
        LOG.info("Total rows affected: " + totalRowsAffected);

        return totalRowsAffected;
    }

    /**
     * This method deletes an EmailBean object from the database
     *
     * @param emailBean The EmailBean object to delete from the database
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if the EmailBean ID doesn't
     *                      exist in the database
     */
    @Override
    public int deleteEmailBean(EmailBean emailBean) throws DaoException {
        int totalRowsAffected = 0;

        LOG.info("Starting deletion of email bean from database: " + emailBean);

        if (isEmailBeanExists(emailBean.getId()) == false) {
            String errorMessage = "Email bean with id " + emailBean.getId() + " doesn't exist";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        totalRowsAffected += deleteAddresses(emailBean);
        totalRowsAffected += deleteAttachments(emailBean);
        totalRowsAffected += deleteOnlyEmailBean(emailBean);

        LOG.info("Finished deletion of email bean from database: " + emailBean);
        LOG.info("Total rows affected: " + totalRowsAffected);

        return totalRowsAffected;
    }

    /**
     * Returns an EmailBean with the specified ID from the database
     *
     * @param id The ID of the EmailBean
     * @return The EmailBean that has the specified ID
     * @throws DaoException Exception is thrown if email bean is not found or unable to open connection
     */
    @Override
    public EmailBean findEmailBeanById(int id) throws DaoException {
        LOG.info("Starting to retrieve email bean by id: " + id);

        EmailBean emailBean = retrieveOnlyEmailBean(id);

        if (emailBean != null) {
            findBeanAttachments(emailBean);
            findBeanAddresses(emailBean);
        }

        LOG.info("Retrieved whole email bean by id: " + emailBean);

        return emailBean;
    }

    /**
     * This method verifies if an email bean with the specified ID
     * exists in the database
     *
     * @param emailBeanId The ID of the email bean
     * @return True if email bean exists
     * @throws DaoException Exception is thrown if failed to open connection
     */
    @Override
    public boolean isEmailBeanExists(int emailBeanId) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Verifying if email bean (id: " + emailBeanId + ") exists in the database");

            connection = connectionManager.openConnection();

            String sql = "SELECT * FROM email_bean " +
                    "WHERE email_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, emailBeanId);

            ResultSet resultSet = pStatement.executeQuery();

            boolean emailBeanExists = false;
            // Verifying if the first row exists (if true email bean exists)
            if (resultSet.first() == true) {
                emailBeanExists = true;
            }

            LOG.info("Email bean with id " + emailBeanId + " exists: " + emailBeanExists);

            return emailBeanExists;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error while verifying if email bean id (" +
                    emailBeanId + ") exists in the database";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Changes the folder of the email (foreign key to folder)
     *
     * @param emailBeanId The ID of the email bean
     * @param folderId The new folder id
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if email bean doesn't exist or failed to open connection
     */
    @Override
    public int changeEmailBeanFolder(int emailBeanId, int folderId) throws DaoException {
        LOG.info("Starting to change folder id of email bean");

        if (isEmailBeanExists(emailBeanId) == false) {
            String errorMessage = "Email bean with id " + emailBeanId + " doesn't exist";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        int rowsAffected = changeFolderReferenceId(emailBeanId, folderId);

        LOG.info("Finished changing the folder id of email bean");

        return rowsAffected;
    }

    /**
     * Helper method to change the folder foreign key in the email bean entity
     *
     * @param emailBeanId The Id of the email bean to change folder
     * @param folderId The folder id to set
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int changeFolderReferenceId(int emailBeanId, int folderId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Changing folder (folder id: " + folderId + ") of email bean id: " + emailBeanId);

            connection = connectionManager.openConnection();

            String sql = "UPDATE email_bean SET folder_id = ? WHERE email_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, folderId);
            pStatement.setInt(2, emailBeanId);

            int rowsAffected = pStatement.executeUpdate();
            LOG.info("Changed folder (folder id: " + folderId + ") of email bean id: " + emailBeanId);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when changing folder (folder id: " +folderId +
                    ") of email bean id: " + emailBeanId;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to retrieve only the email bean entity from database
     * (without addresses or attachments)
     *
     * @param emailBeanId The ID of the email bean
     * @return The email bean retrieved (null if not found)
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private EmailBean retrieveOnlyEmailBean(int emailBeanId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Retrieving only email bean by id: " + emailBeanId);

            connection = connectionManager.openConnection();

            String sql = "SELECT subject, text_message, html_message, priority, " +
                    "received_date, sent_date FROM email_bean WHERE email_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, emailBeanId);

            ResultSet resultSet = pStatement.executeQuery();
            EmailBean emailBean = null;
            if (resultSet.next()) {
                String subject = resultSet.getString(1);
                String textMessage = resultSet.getString(2);
                String htmlMessage = resultSet.getString(3);
                int priority = resultSet.getInt(4);
                Timestamp receivedDate = resultSet.getTimestamp(5);
                Timestamp sentDate = resultSet.getTimestamp(6);

                LocalDateTime localReceivedDate = null;
                if (receivedDate != null) {
                    localReceivedDate = receivedDate.toLocalDateTime();
                }

                LocalDateTime localSentDate = null;
                if (sentDate != null) {
                    localSentDate = sentDate.toLocalDateTime();
                }

                emailBean = new EmailBean();
                emailBean.setId(emailBeanId);
                emailBean.setSubject(subject);
                emailBean.setTextMessage(textMessage);
                emailBean.setHtmlMessage(htmlMessage);
                emailBean.setPriority(priority);
                emailBean.setDateReceived(localReceivedDate);
                emailBean.setDateSent(localSentDate);
            }

            LOG.info("Retrieved only email bean by id: " + emailBean);

            return emailBean;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding only email bean by id: " + emailBeanId;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to create "from" email address in the database
     *
     * @param address The addresses to create
     * @param emailBeanId The id of the emailBean
     * @return The total rows affected
     * @throws DaoException Exception is thrown if error to create address or failed to open connection
     */
    private int createFromAddress(Address address, int emailBeanId) throws DaoException{
        return addressDAO.createAddress(address, emailBeanId, "from");
    }

    /**
     * Helper method to create only an email_bean entity in the database
     *
     * @param emailBean The emailBean to create
     * @param folderId The folder id of the emailBean
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int createOnlyEmailBean(EmailBean emailBean, int folderId) throws DaoException {
        Connection connection = null;

        try {
            LOG.info("Creating EmailBean (only entity) in database: " + emailBean);

            connection = connectionManager.openConnection();

            String subject = emailBean.getSubject();
            String textMessage = emailBean.getTextMessage();
            String htmlMessage = emailBean.getHtmlMessage();
            int priority = emailBean.getPriority();

            Timestamp receivedDate = null;
            if (emailBean.getDateReceived() != null) {
                receivedDate = Timestamp.valueOf(emailBean.getDateReceived());
            }

            Timestamp sentDate = null;
            if (emailBean.getDateSent() != null) {
                sentDate = Timestamp.valueOf(emailBean.getDateSent());
            }


            String sql = "INSERT INTO email_bean(subject, text_message, html_message, priority, " +
                    "received_date, sent_date, folder_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pStatement.setString(1, subject);
            pStatement.setString(2, textMessage);
            pStatement.setString(3, htmlMessage);
            pStatement.setInt(4, priority);
            pStatement.setTimestamp(5, receivedDate);
            pStatement.setTimestamp(6, sentDate);
            pStatement.setInt(7, folderId);

            int rowsAffected = pStatement.executeUpdate();
            LOG.info("EmailBean (only entity) created in database: " + emailBean);
            LOG.info("Rows Affected: " + rowsAffected);

            ResultSet generatedKeys = pStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int generatedId = generatedKeys.getInt(1);
                emailBean.setId(generatedId);
                LOG.info("ID of EmailBean object set: " + generatedId);
            }

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when creating email bean (only entity) in database: " + emailBean;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * Helper method to create addresses in the database
     *
     * @param addresses The addresses to create
     * @param emailBeanId The id of the emailBean
     * @param addressType The type of addresses
     * @return The total rows affected
     * @throws DaoException Exception is thrown if email address already exists or failed to open connection
     */
    private int createAddresses(Address[] addresses, int emailBeanId, String addressType) throws DaoException {
        int totalRowsAffected  = 0;
        if (addresses != null) {
            for (Address address: addresses) {
                totalRowsAffected += addressDAO.createAddress(address, emailBeanId, addressType);
            }
        }

        return totalRowsAffected;
    }

    /**
     * Helper method to create attachments in the database
     *
     * @param attachments The attachments to create
     * @param emailBeanId The ID of the emailBean
     * @return The total rows affected
     * @throws DaoException Exception is thrown if error creating attachments or failed to open connection
     */
    private int createAttachments(Attachment[] attachments, int emailBeanId) throws DaoException {
        int totalRowsAffected  = 0;
        if (attachments != null) {
            for (Attachment attachment: attachments) {
                totalRowsAffected += attachmentDAO.createAttachment(attachment, emailBeanId);
            }
        }
        return totalRowsAffected;
    }

    /**
     * Helper method to delete all the email bean addresses
     *
     * @param emailBean The email bean that contains the email addresses to delete
     * @throws DaoException Exception is thrown if error deleting addresses or failed to open connection
     * @return The number of rows affected
     */
    private int deleteAddresses(EmailBean emailBean) throws DaoException {
        int totalRowsAffected = 0;

        totalRowsAffected += addressDAO.deleteAddress(emailBean.getFrom(), emailBean.getId(), "from");

        for (Address address: emailBean.getTo()) {
            totalRowsAffected += addressDAO.deleteAddress(address, emailBean.getId(), "to");
        }

        if (emailBean.getCc() != null) {
            for (Address address: emailBean.getCc()) {
                totalRowsAffected += addressDAO.deleteAddress(address, emailBean.getId(), "cc");
            }
        }

        if (emailBean.getBcc() != null) {
            for (Address address: emailBean.getBcc()) {
                totalRowsAffected += addressDAO.deleteAddress(address, emailBean.getId(), "bcc");
            }
        }

        return totalRowsAffected;
    }

    /**
     * Helper method to delete all the attachments in the email bean
     *
     * @param emailBean The email bean that contains the attachments to delete
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if attachment not found or unable to open connection
     */
    private int deleteAttachments(EmailBean emailBean) throws DaoException {
        int totalRowsAffected = 0;
        if (emailBean.getAttachments() != null) {
            for (Attachment attachment : emailBean.getAttachments()) {
                totalRowsAffected += attachmentDAO.deleteAttachment(attachment);
            }
        }
        return totalRowsAffected;
    }

    /**
     * Helper method to delete only the email bean entity from the database
     * (Doesn't delete addresses or attachments)
     *
     * @param emailBean The email bean to delete
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if failed to open connection
     */
    private int deleteOnlyEmailBean(EmailBean emailBean) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Deleting only email bean from database: " + emailBean);

            connection = connectionManager.openConnection();

            String sql = "DELETE FROM email_bean WHERE email_id = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, emailBean.getId());
            int rowsAffected = pStatement.executeUpdate();

            emailBean.setId(-1);
            LOG.info("Deleted only email bean from database: " + emailBean);
            LOG.info("Rows affected: " + rowsAffected);

            return rowsAffected;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when deleting only email bean: " + emailBean;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method that returns a list of Attachment objects that are in the specified EmailBean.
     *
     * @param emailBeanId The ID of the email_bean that contains the attachment
     * @return A list of the Attachment objects that are in the EmailBean
     * @throws DaoException Exception is thrown if error to retrieve attachments or failed to open connection
     */
    private List<Attachment> findAttachmentsInEmailBean(int emailBeanId) throws DaoException{
        Connection connection = null;

        try {
            LOG.info("Retrieving all attachments associated with email_bean id: " + emailBeanId);

            connection = connectionManager.openConnection();

            String sql = "SELECT attachment_id FROM attachment WHERE email_bean = ?";
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, emailBeanId);

            ResultSet resultSet = pStatement.executeQuery();

            List<Attachment> attachments = null;
            while (resultSet.next()) {
                if (attachments == null)
                    attachments = new ArrayList<>();

                int attachmentId = resultSet.getInt(1);

                attachments.add(attachmentDAO.findAttachmentById(attachmentId));
            }

            LOG.info("Attachments (with email_bean id: " + emailBeanId + ") retrieved: "
                    + attachments);

            return attachments;
        } catch (SQLException e) {
            String errorMessage = "Unexpected SQL error when finding attachments" +
                    " associated with email_bean id: " + emailBeanId;
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Helper method to find and set the attachments of the email bean.
     * Expecting email bean with existing id
     *
     * @param emailBean The email bean to fill
     * @throws DaoException Exception is thrown if failed to open connection or error retrieving
     * attachments of email
     */
    private void findBeanAttachments(EmailBean emailBean) throws DaoException {
        List<Attachment> attachments = findAttachmentsInEmailBean(emailBean.getId());

        if (attachments != null) {
            Attachment[] attachmentsArray = attachments.toArray(new Attachment[attachments.size()]);
            emailBean.setAttachments(attachmentsArray);
        }
    }

    /**
     * Helper method to find and set the addresses of the email bean.
     * Expecting email bean with existing id
     *
     * @param emailBean The email bean to fill
     * @throws DaoException Exception is thrown if failed to open connection or error retrieving
     * addresses of email
     */
    private void findBeanAddresses(EmailBean emailBean) throws DaoException {
        Address fromAddress = addressDAO.findEmailAddresses(emailBean.getId(), "from").get(0);
        emailBean.setFrom(fromAddress);

        List<Address> toAddresses = addressDAO.findEmailAddresses(emailBean.getId(), "to");
        Address[] toAddressesArray = toAddresses.toArray(new Address[toAddresses.size()]);
        emailBean.setTo(toAddressesArray);

        List<Address> ccAddresses = addressDAO.findEmailAddresses(emailBean.getId(), "cc");
        if (ccAddresses != null) {
            Address[] ccAddressesArray = ccAddresses.toArray(new Address[ccAddresses.size()]);
            emailBean.setCc(ccAddressesArray);
        }

        List<Address> bccAddresses = addressDAO.findEmailAddresses(emailBean.getId(), "bcc");
        if (bccAddresses != null) {
            Address[] bccAddressesArray = bccAddresses.toArray(new Address[bccAddresses.size()]);
            emailBean.setBcc(bccAddressesArray);
        }
    }
}
