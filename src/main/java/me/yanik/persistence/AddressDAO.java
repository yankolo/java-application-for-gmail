package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.EmailBean;

import java.util.List;

/**
 *  The AddressDAO interface defines all the methods that
 *  interact with the address entity in the database.
 */
public interface AddressDAO {
    /**
     * This method crates an address in the database if it doesn't exist and
     * associates (links) to an email_bean entity
     * in the email_address table (bridging table between address and emailBean)
     *
     * @param address     The address to associate
     * @param emailBeanId The email_bean to associate the address to
     * @param addressType The type of the address (from, to, cc, or bcc)
     * @return Returns the number of rows affected
     */
    public int createAddress(Address address, int emailBeanId, String addressType) throws DaoException;

    /**
     * This method deletes a link between the specified an Address and an
     * EmailBean from the email_address table (bridging table between address and email_bean).
     * It also deletes the address from the address table if there are no more references to it;
     *
     * @param address     The address associated with the EmailBean
     * @param emailBeanId The EmailBean associated with the address
     * @param addressType The type of the address (from, to, cc, or bcc)
     * @return Returns the number of rows affected
     */
    public int deleteAddress(Address address, int emailBeanId, String addressType) throws DaoException;

    /**
     * This method finds addresses with the specified address type that
     * are associated with the specified EmailBean.
     * The list will be null if none are found.
     *
     * @param emailBeanId The ID of the EmailBean associated with the addresses
     * @param addressType The type of the address (from, to, cc, or bcc)
     * @return A list of addresses associated with the specified email bean with the specified
     * address type
     * @throws DaoException Exception is thrown if failed to open session
     */
    public List<Address> findEmailAddresses(int emailBeanId, String addressType) throws DaoException;

    /**
     * Returns an Address object that represents with the
     * specified ID
     *
     * @param id The ID of the Address
     * @return The Address with the specified ID
     * @throws DaoException Exception is thrown if failed to open session
     */
    public Address findAddressById(int id) throws DaoException;


}
