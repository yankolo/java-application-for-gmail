package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;

import java.util.List;

/**
 * The ClientDAO interface defines all the operations available database operations for the Email Client.
 *
 * @author Yanik
 */
public interface ClientDAO {
    /**
     * Creates an email bean in the database. Associates the email bean to the
     * specified folder. The EmailBean array of the folder will get updated.
     *
     * @param emailBean The email bean to create
     * @param folderName The name of the folder to associate the email bean with.
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if folder name not found or failed to open connection
     */
    public void createEmailBean(EmailBean emailBean, String folderName) throws DaoException;

    /**
     * This method creates a folder in the database with all the
     * EmailBean objects that it contains. The ID of the folder object will be
     * set to the generated ID in the database.
     *
     * @param folder The folder to create
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder name already taken or
     * failed to open connection
     */
    public int createFolder(Folder folder) throws DaoException;

    /**
     * Returns a list of all folders. The EmailBean arrays of the folders are all set
     * List will be empty if none are found.
     *
     * @return The list of all folders
     * @throws  DaoException Exception is thrown if failed to open connection.
     */
    public List<Folder> findAllFolders() throws DaoException;

    /**
     * Returns a folder with the specified name. The EmailBean array of the folder
     * will be set
     *
     * @param name The name to search
     * @return The folder with the specified name
     * @throws DaoException Exception is thrown if folder doesn't exist or unable to open connection
     */
    public Folder findFolderByName(String name) throws DaoException;

    /**
     * This method updates the EmailBean array of the folder (refreshes it)
     *
     * @param folder The folder to update
     * @throws DaoException Exception is thrown if folder doesn't exist or failed to open connection
     */
    public void reloadEmailsInFolder(Folder folder) throws DaoException;

    /**
     * Moves an email from a folder to another folder.
     *
     * @param emailBeanId The Id of the email bean to move
     * @param folderName The name of the folder to move the email to
     * @throws DaoException Exception is thrown if the specified email or
     * folders do not exist in the database.
     * @return Returns the number of rows affected
     */
    public int changeEmailBeanFolder(int emailBeanId, String folderName) throws DaoException;

    /**
     * This method changes the name of the folder. The name of the Folder
     * object will be also changed.
     *
     * @param folder        The folder to update
     * @param newFolderName The new name for the folder
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist, new name is taken, or
     * unable to open connection
     */
    public int changeFolderName(Folder folder, String newFolderName) throws DaoException;

    /**
     * This method deletes an EmailBean object from the database
     *
     * @param emailBean The EmailBean object to delete from the database
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if the EmailBean ID doesn't
     *                      exist in the database
     */
    public int deleteEmailBean(EmailBean emailBean) throws DaoException;

    /**
     * This method deletes a folder and all the EmailBean objects that
     * it contains from the database
     *
     * @param folder The folder to delete
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist or if failed to open connection
     */
    public int deleteFolder(Folder folder) throws DaoException;

    /**
     * Returns an EmailBean with the specified ID from the database
     *
     * @param id The ID of the EmailBean
     * @return The EmailBean that has the specified ID
     * @throws DaoException Exception is thrown if email bean is not found or unable to open connection
     */
    public EmailBean findEmailBeanById(int id) throws DaoException;

    /**
     * Returns a folder with the specified ID. The EmailBean array of the folder
     * will be set
     *
     * @param id The ID of the folder
     * @return The folder with the specified ID
     * @throws DaoException Exception is thrown if folder doesn't exist or unable to open connection
     */
    public Folder findFolderById(int id) throws DaoException;

    /**
     * Returns an Attachment object with the specified ID from the database
     *
     * @param id The ID of the Attachment
     * @return The Attachment with the specified ID
     * @throws DaoException Exception is thrown if attachment doesn't exist or
     *                      failed to open connection
     */
    public Attachment findAttachmentById(int id) throws DaoException;

    /**
     * Returns an Address object that represents with the
     * specified ID
     *
     * @param id The ID of the Address
     * @return The Address with the specified ID
     * @throws DaoException Exception is thrown if failed to open session
     */
    public Address findAddressById(int id) throws DaoException;
}
