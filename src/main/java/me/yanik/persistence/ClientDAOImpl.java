package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

/**
 * The ClientDAOImpl fully implements the ClientDAO interface.
 *
 * @author Yanik 
 */
public class ClientDAOImpl implements ClientDAO {
    private final static Logger LOG = LoggerFactory.getLogger(ClientDAOImpl.class);

    private ConnectionManager connectionManager;
    private FolderDAO folderDAO;
    private EmailBeanDAO emailBeanDAO;
    private AttachmentDAO attachmentDAO;
    private AddressDAO addressDAO;

    public ClientDAOImpl(ConnectionManager connectionManager ) {
        this.connectionManager = connectionManager;

        attachmentDAO = new AttachmentDAOImpl(connectionManager);
        addressDAO =  new AddressDAOImpl(connectionManager);
        emailBeanDAO = new EmailBeanDAOImpl(connectionManager, addressDAO, attachmentDAO);
        folderDAO = new FolderDAOImpl(connectionManager, emailBeanDAO);
    }

    /**
     * Creates an email bean in the database. Associates the email bean to the
     * specified folder. The EmailBean array of the folder will get updated.
     *
     * @param emailBean The email bean to create
     * @param folderName The name of the folder to associate the email bean with.
     * @return The number of rows affected
     * @throws DaoException Exception is thrown if folder name not found or failed to open connection
     */
    @Override
    public void createEmailBean(EmailBean emailBean, String folderName) throws DaoException {
        if (folderDAO.isFolderNameExists(folderName) == false) {
            String errorMessage = "Folder with name " + folderName + " doesn't exist";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        Folder folder = folderDAO.findFolderByName(folderName);

        emailBeanDAO.createEmailBean(emailBean, folder.getId());
    }

    /**
     * This method creates a folder in the database with all the
     * EmailBean objects that it contains. The ID of the folder object will be
     * set to the generated ID in the database.
     *
     * @param folder The folder to create
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder name already taken or
     * failed to open connection
     */
    @Override
    public int createFolder(Folder folder) throws DaoException {
        return folderDAO.createFolder(folder);
    }

    /**
     * Returns a list of all folders. The EmailBean arrays of the folders are all set
     * List will be empty if none are found.
     *
     * @return The list of all folders
     * @throws  DaoException Exception is thrown if failed to open connection.
     */
    @Override
    public List<Folder> findAllFolders() throws DaoException {
        return folderDAO.findAllFolders();
    }

    /**
     * Returns a folder with the specified name. The EmailBean array of the folder
     * will be set
     *
     * @param name The name to search
     * @return The folder with the specified name
     * @throws DaoException Exception is thrown if folder doesn't exist or unable to open connection
     */
    @Override
    public Folder findFolderByName(String name) throws DaoException{
        return folderDAO.findFolderByName(name);
    }

    /**
     * This method updates the EmailBean array of the folder (refreshes it)
     *
     * @param folder The folder to update
     * @throws DaoException Exception is thrown if folder doesn't exist or failed to open connection
     */
    @Override
    public void reloadEmailsInFolder(Folder folder) throws DaoException {
        folderDAO.reloadEmailsInFolder(folder);
    }

    /**
     * Moves an email from a folder to another folder.
     *
     * @param emailBeanId The Id of the email bean to move
     * @param folderName The name of the folder to move the email to
     * @throws DaoException Exception is thrown if the specified email or
     * folders do not exist in the database.
     * @return Returns the number of rows affected
     */
    @Override
    public int changeEmailBeanFolder(int emailBeanId, String folderName) throws DaoException {
        if (folderDAO.isFolderNameExists(folderName) == false) {
            String errorMessage = "Folder with name " + folderName + " doesn't exist";
            LOG.error(errorMessage);
            throw new DaoException(errorMessage);
        }

        Folder folder = folderDAO.findFolderByName(folderName);

        return emailBeanDAO.changeEmailBeanFolder(emailBeanId, folder.getId());
    }

    /**
     * This method changes the name of the folder. The name of the Folder
     * object will be also changed.
     *
     * @param folder        The folder to update
     * @param newFolderName The new name for the folder
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist, new name is taken, or
     * unable to open connection
     */
    @Override
    public int changeFolderName(Folder folder, String newFolderName) throws DaoException {
        return folderDAO.changeFolderName(folder, newFolderName);
    }

    /**
     * This method deletes an EmailBean object from the database
     *
     * @param emailBean The EmailBean object to delete from the database
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if the EmailBean ID doesn't
     *                      exist in the database
     */
    @Override
    public int deleteEmailBean(EmailBean emailBean) throws DaoException {
        return emailBeanDAO.deleteEmailBean(emailBean);
    }

    /**
     * This method deletes a folder and all the EmailBean objects that
     * it contains from the database
     *
     * @param folder The folder to delete
     * @return Returns the number of rows affected
     * @throws DaoException Exception is thrown if folder doesn't exist or if failed to open connection
     */
    @Override
    public int deleteFolder(Folder folder) throws DaoException {
        return folderDAO.deleteFolder(folder);
    }

    /**
     * Returns an EmailBean with the specified ID from the database
     *
     * @param id The ID of the EmailBean
     * @return The EmailBean that has the specified ID
     * @throws DaoException Exception is thrown if email bean is not found or unable to open connection
     */
    @Override
    public EmailBean findEmailBeanById(int id) throws DaoException{
        return emailBeanDAO.findEmailBeanById(id);
    }

    /**
     * Returns a folder with the specified ID. The EmailBean array of the folder
     * will be set
     *
     * @param id The ID of the folder
     * @return The folder with the specified ID
     * @throws DaoException Exception is thrown if folder doesn't exist or unable to open connection
     */
    @Override
    public Folder findFolderById(int id) throws DaoException{
        return folderDAO.findFolderById(id);
    }

    /**
     * Returns an Attachment object with the specified ID from the database
     *
     * @param id The ID of the Attachment
     * @return The Attachment with the specified ID
     * @throws DaoException Exception is thrown if attachment doesn't exist or
     *                      failed to open connection
     */
    @Override
    public Attachment findAttachmentById(int id) throws DaoException{
        return attachmentDAO.findAttachmentById(id);
    }

    /**
     * Returns an Address object that represents with the
     * specified ID
     *
     * @param id The ID of the Address
     * @return The Address with the specified ID
     * @throws DaoException Exception is thrown if failed to open session
     */
    @Override
    public Address findAddressById(int id) throws DaoException{
        return addressDAO.findAddressById(id);
    }
}
