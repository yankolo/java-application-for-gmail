package me.yanik.gui;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import jodd.mail.Email;
import me.yanik.business.InvalidMailException;
import me.yanik.business.MailController;
import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.util.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class ComposeEmailController {
    private final static Logger LOG = LoggerFactory.getLogger(ComposeEmailController.class);

    private Image appIcon;

    private HBox statusBar;

    private Action onCloseButtonClick;
    private Consumer<EmailBean> onEmailSent;

    private ListProperty<Attachment> attachments = new SimpleListProperty<>(FXCollections.observableArrayList());

    private StringProperty userPersonalName = new SimpleStringProperty("");
    private StringProperty userEmail = new SimpleStringProperty("");

    private MailController mailController;

    @FXML
    private Label fromLabel;

    @FXML
    private Button sendButton;

    @FXML
    private Button addAttachmentButton;

    @FXML
    private Button closeButton;

    @FXML
    private TextField toTextField;

    @FXML
    private TextField ccTextField;

    @FXML
    private TextField bccTextField;

    @FXML
    private TextField subjectTextField;

    @FXML
    private TextField priorityTextField;

    @FXML
    private HTMLEditor messageHTMLEditor;

    @FXML
    private HBox attachmentsHBox;

    @FXML
    private ResourceBundle resources;

    @FXML
    public void initialize() {
        // Binding the from label with the personal name concatenated with the email
        fromLabel.textProperty().bind(Bindings.concat(userPersonalName, " <", userEmail, ">"));

        // Setting the action of the close button
        closeButton.setOnAction(
                (actionEvent) -> {
                    if (onCloseButtonClick != null) {
                        onCloseButtonClick.execute();
                    }
                }
        );

        // Pre-creating the left click handler for the attachments
        // Better performance than always creating new handlers (if handler is reused)
        EventHandler<MouseEvent> attachmentLeftClickHandler = createAttachmentLeftClickHandler();

        // If the attachments list is changed, removes all attachments, and sets them again
        // Attachments get the left click handler attached to them and a style class applied
        attachments.addListener(
                (observable, oldValue, newValue) -> {
                    LOG.info("Attachment list has changed");

                    attachmentsHBox.getChildren().removeAll(attachmentsHBox.getChildren());

                    List<Label> labels = new ArrayList<>();
                    for (Attachment attachment: attachments.get()) {
                        Label label = new Label();
                        label.setText(attachment.getName());
                        label.addEventHandler(MouseEvent.MOUSE_PRESSED,
                                attachmentLeftClickHandler);
                        label.getStyleClass().add("compose-attachment-label");
                        labels.add(label);
                    }

                    for (Label label: labels) {
                        attachmentsHBox.getChildren().add(label);
                    }
                }
        );

        /**
         * When the add attachment button is clicked, a file chooser opens and lets the user choose a file
         * to attach
         */
        addAttachmentButton.setOnAction(
                (actionEvent) -> {
                    LOG.info("Add Attachment pressed");

                    Window currentWindow = attachmentsHBox.getScene().getWindow();

                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle(resources.getString("addAttachment"));
                    fileChooser.getExtensionFilters().addAll(
                            new FileChooser.ExtensionFilter(resources.getString("allFiles"), "*.*"));
                    File selectedFile = fileChooser.showOpenDialog(currentWindow);
                    if (selectedFile != null) {
                        byte[] content = new byte[] {};
                        try {
                            content = Files.readAllBytes(selectedFile.toPath());
                        } catch (IOException e) {
                            String errorMessage = "Unexpected error when reading all bytes of chosen file";
                            LOG.error(errorMessage);
                            throw new RuntimeException(errorMessage, e);
                        }

                        Attachment attachment = new Attachment();
                        attachment.setContent(content);
                        attachment.setEmbedded(false);
                        attachment.setName(selectedFile.getName());

                        attachments.add(attachment);
                    }
                }
        );

        /**
         * Event handler for send button
         */
        sendButton.setOnAction(
                (actionEvent) -> {
                    LOG.info("Send button pressed");

                    Thread sendEmailThread = new Thread(
                            () -> {
                                ProgressIndicator progressIndicator = new ProgressIndicator();
                                progressIndicator.setPrefSize(15, 15);
                                Label statusLabel = new Label(resources.getString("sending_email"));

                                try {
                                    Platform.runLater( () -> {
                                        statusBar.getChildren().addAll(statusLabel, progressIndicator);
                                    });

                                    EmailBean emailBeanToSend = createEmailBean();

                                    mailController.sendEmail(emailBeanToSend);

                                    Platform.runLater(
                                            () -> {
                                                if (onEmailSent != null)
                                                    onEmailSent.accept(emailBeanToSend);

                                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                                ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                                                alert.setTitle(resources.getString("email_sent"));
                                                alert.setHeaderText(null);
                                                alert.setContentText(resources.getString("the_email_was_sent"));

                                                alert.showAndWait();
                                            }
                                    );

                                    LOG.info("Email was sent.");
                                } catch (InvalidMailException e) {
                                    Platform.runLater(
                                            () -> {
                                                LOG.warn(resources.getString("email_not_sent_invalid_email"));

                                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                                ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                                                alert.setTitle(resources.getString("email_not_sent_alert_title"));
                                                alert.setHeaderText(resources.getString("email_not_sent_alert_header"));
                                                alert.setContentText(resources.getString("email_not_sent_alert_message") +
                                                        e.getMessage());

                                                alert.showAndWait();
                                            }
                                    );
                                } finally {
                                    Platform.runLater(() -> statusBar.getChildren().removeAll(statusLabel, progressIndicator));
                                }
                            }
                    );

                    sendEmailThread.start();
                }
        );

        LOG.info("Compose email controlled initialized");
    }

    /**
     * A helper method to create an event handler when an attachment is left clicked
     */
    private EventHandler<MouseEvent> createAttachmentLeftClickHandler() {
        EventHandler<MouseEvent> attachmentLeftClickHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown()) {
                    LOG.info("Attachment left clicked");

                    Label label = (Label) event.getSource();

                    // Removing the attachment that corresponds to the position of the
                    // label in the HBox. Using the position instead of the object when removing
                    // because attachments can have the same name
                    int labelPosition = ((HBox) label.getParent()).getChildren().indexOf(label);
                    attachments.remove(labelPosition);

                    event.consume();
                }
            }
        };

        return attachmentLeftClickHandler;
    }

    /**
     * Method used to prepare the compose email controller to write a new message
     */
    public void writeNewEmail() {
        clearAll();
        setPriorityNormal();

        LOG.info("Compose controller prepared to write a new message");
    }

    /**
     * Method used to prepare the compose email controller to reply to a message
     * (fills the appropriate fields)
     * @param emailBean The emailbean to reply to
     */
    public void reply(EmailBean emailBean) {
        clearAll();
        setAddresses(toTextField, emailBean.getFrom());
        setPriorityNormal();
        subjectTextField.setText(generateReplySubject(emailBean));
        messageHTMLEditor.setHtmlText(generateReplyMessage(emailBean));
        attachments.addAll(emailBean.getAttachments());

        // To display embedded attachments
        messageHTMLEditor.setHtmlText(replaceCidsInHtml(emailBean, messageHTMLEditor.getHtmlText()));

        LOG.info("Compose controller prepared to reply to a message");
    }

    /**
     * Method used to prepare the compose email controller to "reply all" to a message
     * (fills the appropriate fields)
     * @param emailBean The emailbean to "reply all" to
     */
    public void replyAll(EmailBean emailBean) {
        clearAll();
        setAddresses(toTextField, emailBean.getFrom());
        setPriorityNormal();

        Address[] toAddresses = emailBean.getTo();
        Address[] ccAddresses = (emailBean.getCc() != null) ? emailBean.getCc() : new Address[] {};

        Address[] recipients = Stream.of(toAddresses, ccAddresses)
                .flatMap(emailArray -> Arrays.stream(emailArray))
                .filter(address -> !address.equals(userEmail))
                .toArray(Address[]::new);

        setAddresses(ccTextField, recipients);

        subjectTextField.setText(generateReplySubject(emailBean));
        messageHTMLEditor.setHtmlText(generateReplyMessage(emailBean));
        attachments.addAll(emailBean.getAttachments());

        // To display embedded attachments
        messageHTMLEditor.setHtmlText(replaceCidsInHtml(emailBean, messageHTMLEditor.getHtmlText()));

        LOG.info("Compose controller preapred to reply all to a message");
    }

    /**
     * Method used to prepare the compose email controller to forward a message
     * (fills the appropriate fields)
     * @param emailBean The emailbean to forward
     */
    public void forward(EmailBean emailBean) {
        clearAll();
        setPriorityNormal();
        subjectTextField.setText(emailBean.getSubject());
        messageHTMLEditor.setHtmlText(emailBean.getHtmlMessage());
        attachments.addAll(emailBean.getAttachments());

        // To display embedded attachments
        messageHTMLEditor.setHtmlText(replaceCidsInHtml(emailBean, messageHTMLEditor.getHtmlText()));

        LOG.info("Compose controller preapred to forward a message");
    }

    /**
     * Sets the callback that gets called when the close button is presesed
     * @param onCloseButtonClick The callback that gets called when the close button is presesed
     */
    public void setOnCloseButtonClick(Action onCloseButtonClick) {
        this.onCloseButtonClick = onCloseButtonClick;
    }

    /**
     * Helper method to return an html message that has all the cids
     * replaced to srcs (to be able to display the embedded attachments)
     *
     * Returns a string that has all the cids replaced with a file src
     */
    private String replaceCidsInHtml(EmailBean emailBean, String htmlString) {
        if (emailBean.getAttachments() == null)
            return htmlString;

        String newHtml = htmlString;

        for (Attachment attachment: emailBean.getAttachments()) {
            Path tempFilePath = Paths.get("client-images" + File.separator + attachment.getName());
            newHtml = newHtml.replaceAll("cid:" + attachment.getContentId(), tempFilePath.toUri().toString());
        }

        return newHtml;
    }

    /**
     * Helper method to return an html message that has all the srcs
     * replaced to cids (to be able to send embedded attachments)
     *
     * @param htmlString The HTML string where the srcs will be replaced into cids
     * @return A string that has all the file srcs replaced with cids
     */
    private String replaceSrcInHtml(EmailBean emailBean, String htmlString) {
        if (emailBean.getAttachments() == null)
            return htmlString;

        String newHtml = htmlString;

        for (Attachment attachment: emailBean.getAttachments()) {
            if (attachment.isEmbedded()) {
                Path attachmentPath = Paths.get("client-images" + File.separator + attachment.getName());
                newHtml = newHtml.replaceAll(attachmentPath.toUri().toString(), "cid:" + attachment.getContentId());
            }
        }

        return newHtml;
    }

    /**
     * Helper method to set the priority to normal (3)
     */
    private void setPriorityNormal() {
        priorityTextField.setText("3");
    }

    /**
     * Helper method to set a textfield with a list of addresses (separated by commas)
     */
    private void setAddresses(TextField textField, Address ... addresses) {
        String addressesString = "";
        for (int i = 0; i < addresses.length; i++) {
            addressesString += addresses[i].getEmail();
            if (i != addresses.length - 1)
                addressesString += ", ";
        }

        textField.setText(addressesString);
    }

    /**
     * Helper method to generate a "Reply" subject (Re: appended before the subject)
     * @param emailBean The emailbean to reply
     * @return A new subject to represent a reply
     */
    private String generateReplySubject(EmailBean emailBean) {
        String originalSubject = (emailBean.getSubject() != null) ? emailBean.getSubject() : "";
        String replySubject = "Re: " + originalSubject;

        LOG.info("Reply subject generated");

        return replySubject;
    }

    /**
     * Helper method to generate a "Reply" message body. Separates the message that is being replied to
     * @param emailBean The email bean to reply
     * @return A new message body that has the message that is being replied to separated
     */
    private String generateReplyMessage(EmailBean emailBean) {
        DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        String dateTime = localDateTimeFormatter.format(emailBean.getDateReceived());

        String replyMessage = "<br><br><br><br>";
        replyMessage += String.format(resources.getString("replyWrote"), dateTime, formatAddress(emailBean.getFrom()));
        replyMessage += "<blockquote>";
        replyMessage += emailBean.getHtmlMessage();
        replyMessage += "</blockquote>";

        LOG.info("Reply message body generated");

        return replyMessage;
    }

    /**
     * Helper method to format addresses
     */
    private String formatAddress(Address address) {
        return address.getPersonalName() + "<" + address.getPersonalName() + ">";
    }

    /**
     * Helper method to clear all the fields
     */
    private void clearAll() {
        toTextField.setText("");
        ccTextField.setText("");
        bccTextField.setText("");
        subjectTextField.setText("");
        messageHTMLEditor.setHtmlText("");

        attachments.removeAll(attachments.get());

        LOG.info("All fields have reset");
    }

    /**
     * Helper method to remove all tags from an html message
     * Uses regex to remove all <> characters and everything between them
     * @return A string without html tags
     */
    private String getPlainText() {
        return messageHTMLEditor.getHtmlText().replaceAll("\\<[^>]*>","");
    }

    /**
     * Helper method to create an address array from a text field
     * @param textField A TextField that contains a list of emails separated with commmas
     * @return An Address array
     */
    private Address[] createAddressesFromField(TextField textField) {
        if (textField.getText().equals(""))
            return null;

        String labelText = textField.getText();

        // Removing any leading and trailing commas, spaces and semi colons, so that when
        // splitting, we don't have empty elements in the array
        labelText = labelText.replaceAll("^[,; ]+", "");
        labelText = labelText.replaceAll("[,; ]+$", "");

        // Splitting all emails
        String[] emailStringArray = labelText.split("[,; ]+");

        Address[] addresses = Arrays.stream(emailStringArray)
                .map(emailString -> {
                    Address address = new Address();
                    address.setEmail(emailString);
                    return address;
                })
                .toArray(Address[]::new);

        return addresses;
    }

    /**
     * Helper method to create an email bean from the all the fields
     * in the controller
     * @return An email bean from all the fields from the controller
     */
    private EmailBean createEmailBean() {
        EmailBean emailBean = new EmailBean();

        Address fromAddress = new Address(userPersonalName.get(), userEmail.get());

        emailBean.setFrom(fromAddress);
        emailBean.setTo(createAddressesFromField(toTextField));
        emailBean.setCc(createAddressesFromField(ccTextField));
        emailBean.setBcc(createAddressesFromField(bccTextField));
        emailBean.setSubject(subjectTextField.getText());
        emailBean.setHtmlMessage(messageHTMLEditor.getHtmlText());
        emailBean.setTextMessage(getPlainText());
        emailBean.setPriority(Integer.parseInt(priorityTextField.getText()));
        emailBean.setDateSent(LocalDateTime.now());
        emailBean.setDateReceived(LocalDateTime.now());
        emailBean.setAttachments(attachments.get().toArray(new Attachment[attachments.get().size()]));

        // To properly send embedded attachments
        String properHtmlMessage = replaceSrcInHtml(emailBean, emailBean.getHtmlMessage());
        emailBean.setHtmlMessage(properHtmlMessage);

        return emailBean;
    }

    /**
     * Sets the personal name of the user
     * @param userPersonalName the personal name of the user
     */
    public void setUserPersonalName(String userPersonalName) {
        this.userPersonalName.setValue(userPersonalName);
    }

    /**
     * Sets the email address of the user
     * @param userEmail the email address of the user
     */
    public void setUserEmail(String userEmail) {
        this.userEmail.setValue(userEmail);
    }

    /**
     * Sets the MailController that will be used to send emails
     * @param mailController
     */
    public void setMailController(MailController mailController) {
        this.mailController = mailController;
    }

    /**
     * Sets the callback that will be called when an email is sent successfully.
     * @param onEmailSent The callback that will be called when an email is sent successfully
     */
    public void setOnEmailSent(Consumer<EmailBean> onEmailSent) {
        this.onEmailSent = onEmailSent;
    }

    /**
     * Sets the reference to the status bar
     * @param statusBar Sets the reference to the status bar
     */
    public void setStatusBar(HBox statusBar) {
        this.statusBar = statusBar;
    }

    /**
     * Sets the image for the app icon (so that it will be easy to set the icon of alerts)
     * @param appIcon The image for the app icon
     */
    public void setAppIcon(Image appIcon) {
        this.appIcon = appIcon;
    }
}
