package me.yanik.gui;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.css.PseudoClass;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import me.yanik.persistence.ClientDAO;
import me.yanik.persistence.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.*;
import java.util.function.Consumer;

import static me.yanik.gui.MainWindowController.EMAIL_BEAN_ID;

public class FolderListController {
    private final static Logger LOG = LoggerFactory.getLogger(FolderListController.class);

    private Image appIcon;

    private Consumer<Folder> onFolderSelected;
    private Consumer<Folder> onSelectedFolderUpdated;
    private ListProperty<Folder> folders = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ClientDAO clientDAO;

    @FXML
    private ListView<Folder> listView;

    @FXML
    private ResourceBundle resources;

    @FXML
    public void initialize() {
        /*
         * Creating all the drag event handlers before hand
         * They will be attached to each non-empty list cell
         * Better performance than always creating new handlers for each cell (as they are reused)
         */
        EventHandler<DragEvent> emailDragOverHandler = createEmailDragOverHandler();
        EventHandler<DragEvent> emailDragDroppedHandler = createEmailDragDroppedHandler();
        EventHandler<DragEvent> emailDragEnteredHandler = createEmailDragEnteredHandler();
        EventHandler<DragEvent> emailDragExitedHandler = createEmailDragExitedHandler();

        listView.itemsProperty().bindBidirectional(folders);

        /*
         * Sets the factory for each cell, sets all the above drag handlers
         * and sets the context menu
         */
        listView.setCellFactory(
                (listView) -> {
                    // The cell overrides the updateItem() method so that
                    // it properly displays the folder
                    ListCell<Folder> cell = createFolderListCell();

                    cell.setOnDragOver(emailDragOverHandler);
                    cell.setOnDragDropped(emailDragDroppedHandler);
                    cell.setOnDragEntered(emailDragEnteredHandler);
                    cell.setOnDragExited(emailDragExitedHandler);

                    // Creating a context menu that will be shown when a cell is right clicked
                    ContextMenu contextMenu = new ContextMenu();

                    MenuItem renameMenuItem = createRenameMenuItem(cell);
                    MenuItem deleteMenuItem = createDeleteMenuItem(cell);

                    contextMenu.getItems().addAll(renameMenuItem, deleteMenuItem);

                    // Only attaches the context menu to non-empty cells
                    cell.emptyProperty().addListener(
                            (observable, oldValue, isEmptyCell) -> {
                                if (isEmptyCell) {
                                    cell.setContextMenu(null);
                                } else {
                                    deleteMenuItem.setDisable(!cell.getItem().isDeletable());
                                    renameMenuItem.setDisable(!cell.getItem().isRenamable());
                                    cell.setContextMenu(contextMenu);
                                }
                            });

                    return cell;
                }
        );

        listView.getSelectionModel().selectedItemProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if (onFolderSelected != null)
                        onFolderSelected.accept(newValue);
                })
        );
    }

    /**
     * Creates an anonymous class that overrides updateItem of a ListCell.
     * This is done so that the ListCell will be able to display a Folder
     * in a ListView properly
     *
     * @return A ListCell that has an overridden updateItem method
     */
    private ListCell<Folder> createFolderListCell() {
        ListCell<Folder> folderListCell = new ListCell<Folder>() {
            @Override
            protected void updateItem(Folder item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    if (item.getName().equals("inbox"))
                        setText(resources.getString("inbox_folder"));
                    else if (item.getName().equals("sent"))
                        setText(resources.getString("sent_folder"));
                    else
                        setText(item.getName());
                }
            }
        };

        return folderListCell;
    }

    /**
     * Helper method to create a drag over event handler
     */
    private EventHandler<DragEvent> createEmailDragOverHandler() {
        EventHandler<DragEvent> emailDragOverHandler = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                ListCell<Folder> cell = (ListCell) event.getSource();

                // Accept data only if it is not dragged from the listview
                // and if it has a "text/email_bean_id" data
                if (event.getGestureSource() != listView &&
                        event.getDragboard().hasContent(EMAIL_BEAN_ID) &&
                        !cell.isEmpty() &&
                        cell.getItem() != listView.getSelectionModel().getSelectedItem()) {
                    event.acceptTransferModes(TransferMode.MOVE);
                }

                event.consume();
            }
        };

        return emailDragOverHandler;
    }

    /**
     * Helper method to create a drag dropped event handler
     */
    private EventHandler<DragEvent> createEmailDragDroppedHandler() {
        EventHandler<DragEvent> emailDragDroppedHandler = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasContent(EMAIL_BEAN_ID)) {
                    try {
                        LOG.info("Drag Dropped");

                        ListCell<Folder> cell = (ListCell) event.getSource();

                        int emailBeanId = Integer.parseInt((String) db.getContent(EMAIL_BEAN_ID));

                        Folder sourceFolder = listView.getSelectionModel().getSelectedItem();
                        Folder targetFolder = cell.getItem();

                        // Finding the email with this id in the selected folder
                        EmailBean emailBean = Arrays.stream(sourceFolder.getEmailBeans())
                                .filter(email -> email.getId() == emailBeanId)
                                .findFirst()
                                .get();

                        clientDAO.changeEmailBeanFolder(emailBeanId, cell.getItem().getName());

                        removeEmailFromFolder(sourceFolder, emailBean);
                        addEmailToFolder(targetFolder, emailBean);

                        success = true;

                        LOG.info("Email folder was changed");
                    } catch (DaoException e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                        alert.setTitle(resources.getString("email.folder.was.not.changed"));
                        alert.setHeaderText(resources.getString("email_folder_not_changed_header"));
                        alert.setContentText(resources.getString("email_folder_not_changed_message") + e.getMessage());

                        alert.showAndWait();

                        LOG.warn("Error when changing email folder: " + e.getMessage());
                    }
                }

                event.setDropCompleted(success);

                event.consume();
            }
        };

        return emailDragDroppedHandler;
    }

    /**
     * Helper method to create a drag entered event handler
     * Sets a style pseudo class used to style cells that are dragged over
     */
    private EventHandler<DragEvent> createEmailDragEnteredHandler() {
        EventHandler<DragEvent> emailDragEnteredHandler = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                ListCell<Folder> cell = (ListCell) event.getSource();

                if (event.getGestureSource() != listView &&
                        event.getDragboard().hasContent(EMAIL_BEAN_ID) &&
                        !cell.isEmpty()) {
                    cell.pseudoClassStateChanged(PseudoClass.getPseudoClass("drag-entered"), true);
                }

                event.consume();
            }
        };

        return emailDragEnteredHandler;
    }

    /**
     * Helper method to create a drag exited event handler
     * Removes the pseudo style class used to style cells that are drag over
     */
    private EventHandler<DragEvent> createEmailDragExitedHandler() {
        EventHandler<DragEvent> emailDragExitedHandler = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                ListCell<Folder> cell = (ListCell) event.getSource();

                // Remove background set by onDragEntered
                cell.pseudoClassStateChanged(PseudoClass.getPseudoClass("drag-entered"), false);

                event.consume();
            }
        };

        return emailDragExitedHandler;
    }

    /**
     * Helper method to create a rename folder context menu item
     *
     * @param cell The cell that will be used to determine if the
     *             menu item is enabled or not
     * @return the menu item
     */
    private MenuItem createRenameMenuItem(ListCell<Folder> cell) {
        MenuItem renameMenuItem = new MenuItem();
        renameMenuItem.textProperty().setValue(resources.getString("renameFolder"));
        renameMenuItem.setOnAction(
                (actionEvent) -> {
                    TextInputDialog dialog = new TextInputDialog();
                    ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                    dialog.setTitle(resources.getString("renameFolder"));
                    dialog.setHeaderText(resources.getString("renamingAFolder"));
                    dialog.setContentText(resources.getString("enterNewNameForFolder"));

                    Button buttonOk = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
                    buttonOk.setDisable(true);

                    dialog.getEditor().textProperty().addListener(
                            (observable, oldValue, newValue) -> {
                                if (newValue.isEmpty())
                                    buttonOk.setDisable(true);
                                else
                                    buttonOk.setDisable(false);
                            }
                    );

                    Optional<String> result = dialog.showAndWait();

                    if (result.isPresent()){
                        try {
                            Folder folder = cell.getItem();
                            String newFolderName = result.get();

                            clientDAO.changeFolderName(folder, newFolderName);

                            folder.setName(newFolderName);
                            listView.refresh();

                            LOG.info("Folder was renamed: " + folder);
                        } catch (DaoException e) {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                            alert.setTitle(resources.getString("folder.was.not.renamed.title"));
                            alert.setHeaderText(resources.getString("folder_invalid_name_header"));
                            alert.setContentText(MessageFormat.format(resources.getString("folder_invalid_name_message"), e.getMessage()));

                            alert.showAndWait();

                            LOG.warn("Error when renaming folder: " + e.getMessage());
                        }
                    }
                });

        return renameMenuItem;
    }

    /**
     * Helper method to create a delete folder context menu item
     *
     * @param cell The cell that will be used to determine if the
     *             menu item is enabled or not
     * @return the menu item
     */
    private MenuItem createDeleteMenuItem(ListCell<Folder> cell) {
        MenuItem deleteMenuItem = new MenuItem();
        deleteMenuItem.textProperty().setValue(resources.getString("deleteFolder"));
        deleteMenuItem.setOnAction(
                (actionEvent) -> {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                    alert.setTitle(resources.getString("deleteFolder"));
                    alert.setHeaderText(resources.getString("deletingAFolder"));
                    alert.setContentText(resources.getString("carefulDeletingEmail"));

                    ButtonType yesButton = new ButtonType(resources.getString("yes"));
                    ButtonType noButton = new ButtonType(resources.getString("no"));

                    alert.getButtonTypes().setAll(yesButton, noButton);

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == yesButton) {
                        try {
                            Folder folderToRemove = cell.getItem();
                            clientDAO.deleteFolder(folderToRemove);
                            folders.get().remove(cell.getItem());

                            LOG.info("Folder was deleted");
                        } catch (DaoException e) {
                            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                            ((Stage) errorAlert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                            errorAlert.setTitle(resources.getString("folder_not_deleted_title"));
                            errorAlert.setHeaderText(resources.getString("folder_not_deleted_header"));
                            errorAlert.setContentText(MessageFormat.format(resources.getString("folder_not_deleted_message"), e.getMessage()));

                            errorAlert.showAndWait();
                            LOG.warn("Folder was not deleted: " + e.getMessage());
                        }
                    }
                }
        );

        return deleteMenuItem;
    }

    /**
     * Helper method to remove a specific email bean from the folder
     */
    private void removeEmailFromFolder(Folder folder, EmailBean emailBean) {
        EmailBean[] emails = folder.getEmailBeans();
        List<EmailBean> emailsList = new ArrayList<>(Arrays.asList(emails));

        emailsList.remove(emailBean);

        EmailBean[] newEmailsArray = new EmailBean[emailsList.size()];
        folder.setEmailBeans(emailsList.toArray(newEmailsArray));

        LOG.info("Emailbean removed from folder");
    }

    /**
     * Helper method to add a specific email bean to a folder
     */
    private void addEmailToFolder(Folder folder, EmailBean emailBean) {
        EmailBean[] emails = folder.getEmailBeans();
        List<EmailBean> emailsList = new ArrayList<>(Arrays.asList(emails));

        emailsList.add(emailBean);

        Collections.sort(emailsList,
                (email1, email2) -> {
                    return email2.getDateReceived().compareTo(email1.getDateReceived());
                });

        EmailBean[] newEmailsArray = new EmailBean[emailsList.size()];
        folder.setEmailBeans(emailsList.toArray(newEmailsArray));

        if (folder.equals(listView.getSelectionModel().getSelectedItem())) {
            onSelectedFolderUpdated.accept(folder);
        }

        LOG.info("Emailbean added to folder");
    }

    /**
     * Method that is used when one needs to delete an email beans
     * @param emailBean The email bean to delete
     */
    public void deleteEmail(EmailBean emailBean) {
        try {
            Folder emailFolder = null;

            for (Folder folder : folders) {
                boolean isEmailInFolder = Arrays.stream(folder.getEmailBeans())
                        .anyMatch(emailInFolder -> emailInFolder.equals(emailBean));

                if (isEmailInFolder)
                    emailFolder = folder;
            }

            clientDAO.deleteEmailBean(emailBean);
            removeEmailFromFolder(emailFolder, emailBean);

            if (emailFolder.equals(listView.getSelectionModel().getSelectedItem())) {
                onSelectedFolderUpdated.accept(emailFolder);
            }

            LOG.info("Emailbean deleted");
        } catch (DaoException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
            alert.setTitle(resources.getString("email.was.not.deleted.title"));
            alert.setHeaderText(resources.getString("email.was.not.deleted.header"));
            alert.setContentText(MessageFormat.format(resources.getString("email.was.not.deleted.message"), e.getMessage()));

            alert.showAndWait();

            LOG.warn("Error when deleting email: " + e.getMessage());
        }
    }

    public void addEmailToOutbox(EmailBean emailBean) {
        try {
            clientDAO.createEmailBean(emailBean, "sent");

            Folder outbox = folders.stream()
                    .filter(folder -> folder.getName().equals("sent"))
                    .findFirst()
                    .get();

            addEmailToFolder(outbox, emailBean);

            LOG.info("Email added to outbox");
        } catch (DaoException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
            alert.setTitle(resources.getString("email.was.not.added.to.outbox"));
            alert.setHeaderText(resources.getString("email.was.not.added.to.outbox.header"));
            alert.setContentText(resources.getString("email.was.not.added.to.outbox.message") + e.getMessage());

            alert.showAndWait();
            LOG.warn("Email was not added to outbox: " + e.getMessage());
        }
    }

    public void addEmailToInbox(EmailBean emailBean) {
        Folder inbox = folders.stream()
                .filter(folder -> folder.getName().equals("inbox"))
                .findFirst()
                .get();

        addEmailToFolder(inbox, emailBean);
    }

    /**
     * The method that is used when adding a new folder. Opens a dialog
     * that asks the user the new name for the folder
     */
    public void addNewFolder() {
        LOG.info("Asking user to input name for new folder");

        TextInputDialog dialog = new TextInputDialog();
        ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
        dialog.setTitle(resources.getString("createNewFolder"));
        dialog.setHeaderText(resources.getString("creatingAFolder"));
        dialog.setContentText(resources.getString("enterNameForFolder"));

        Button buttonOk = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        buttonOk.setDisable(true);

        dialog.getEditor().textProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue.isEmpty())
                        buttonOk.setDisable(true);
                    else
                        buttonOk.setDisable(false);
                }
        );

        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()) {
            try {
                String folderName = result.get();

                Folder folder = new Folder();
                folder.setName(folderName);
                folder.setEmailBeans(new EmailBean[]{});

                clientDAO.createFolder(folder);

                folders.add(folder);

                LOG.info("Folder added: " + folder);
            } catch (DaoException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
                alert.setTitle(resources.getString("folder.was.not.added.title"));
                alert.setHeaderText(resources.getString("folder.was.not.added.header"));
                alert.setContentText(resources.getString("folder.was.not.added.message") + e.getMessage());

                alert.showAndWait();

                LOG.warn("Error when adding folder: " + e.getMessage());
            }
        }
    }

    /**
     * Sets the client DAO
     * @param clientDAO the client DAO
     */
    public void setClientDAO(ClientDAO clientDAO) {
        this.clientDAO = clientDAO;
    }

    /**
     * Loads all the emails from the DAO
     * @throws DaoException Exception is thrown for several reasons (see ClientDAO.findAllFolders())
     */
    public void loadData() throws DaoException {
        folders.setAll(clientDAO.findAllFolders());
        listView.getSelectionModel().selectFirst();
    }

    /**
     * Sets the callback that is called when a folder is selected
     * @param onFolderSelected the callback that is called when a folder is selected
     */
    public void setOnFolderSelected(Consumer<Folder> onFolderSelected) {
        this.onFolderSelected = onFolderSelected;
    }

    /**
     * Sets the callback that is called when the email list inside the folder that is selected
     * is updated
     * @param onSelectedFolderUpdated the callback that is called when the email list inside the folder that is selected
     *      is updated
     */
    public void setOnSelectedFolderUpdated(Consumer<Folder> onSelectedFolderUpdated) {
        this.onSelectedFolderUpdated = onSelectedFolderUpdated;
    }

    /**
     * Sets the image for the app icon (so that it will be easy to set the icon of alerts)
     * @param appIcon The image for the app icon
     */
    public void setAppIcon(Image appIcon) {
        this.appIcon = appIcon;
    }
}
