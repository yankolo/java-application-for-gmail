package me.yanik.gui;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.util.Callback;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;
import java.util.function.Consumer;

import static me.yanik.gui.MainWindowController.EMAIL_BEAN_ID;

public class EmailListController {
    private final static Logger LOG = LoggerFactory.getLogger(EmailListController.class);

    private Image appIcon;

    private Consumer<EmailBean> onEmailDoubleClicked;
    private Consumer<EmailBean> onReplyClicked;
    private Consumer<EmailBean> onReplyAllClicked;
    private Consumer<EmailBean> onForwardClicked;
    private Consumer<EmailBean> onDeleteClicked;

    @FXML
    private TableView<EmailBean> mailTable;

    @FXML
    private TableColumn<EmailBean, String> subjectColumn;

    @FXML
    private TableColumn<EmailBean, String> fromColumn;

    @FXML
    private TableColumn<EmailBean, LocalDateTime> dateColumn;

    @FXML
    private ResourceBundle resources;

    private ObjectProperty<Folder> currentFolder = new SimpleObjectProperty<>();
    private ListProperty<EmailBean> emails = new SimpleListProperty<>(FXCollections.observableArrayList());

    @FXML
    public void initialize() {
        mailTable.itemsProperty().bindBidirectional(emails);

        /*
         * Sets the factory of the rows (each row will contain a context menu with 4 items)
         */
        mailTable.setRowFactory(
                new Callback<TableView<EmailBean>, TableRow<EmailBean>>() {
                    @Override
                    public TableRow<EmailBean> call(TableView<EmailBean> tableView) {
                        TableRow<EmailBean> row = new TableRow<>();

                        ContextMenu rowMenu = new ContextMenu();
                        MenuItem replyItem = new MenuItem(resources.getString("reply"));
                        MenuItem replyAllItem = new MenuItem(resources.getString("replyAll"));
                        MenuItem forwardItem = new MenuItem(resources.getString("forward"));
                        MenuItem deleteItem = new MenuItem(resources.getString("delete"));

                        replyItem.setOnAction(
                                (actionEvent) -> {
                                    if (onReplyClicked != null)
                                        onReplyClicked.accept(row.getItem());
                                }
                        );

                        replyAllItem.setOnAction(
                                (actionEvent) -> {
                                    if (onReplyAllClicked != null)
                                        onReplyAllClicked.accept(row.getItem());
                                }
                        );

                        forwardItem.setOnAction(
                                (actionEvent) -> {
                                    if (onForwardClicked != null)
                                        onForwardClicked.accept(row.getItem());
                                }
                        );

                        deleteItem.setOnAction(
                                (actionEvent) -> {
                                    if (onDeleteClicked != null)
                                        onDeleteClicked.accept(row.getItem());
                                }
                        );

                        rowMenu.getItems().addAll(replyItem, replyAllItem, forwardItem, deleteItem);

                        row.emptyProperty().addListener(
                                (observable, oldValue, isEmptyCell) -> {
                                    if (!isEmptyCell) {
                                        row.setContextMenu(rowMenu);
                                    }
                                });

                        return row;
                    }
                });

        // Note, cellDataFeatures.getValue() returns the EmailBean object
        // of a particular TableView row
        subjectColumn.setCellValueFactory(
                (cellDataFeatures) -> cellDataFeatures.getValue().subjectProperty());
        fromColumn.setCellValueFactory(
                (cellDataFeatures) -> cellDataFeatures.getValue().getFrom().emailProperty());
        dateColumn.setCellValueFactory(
                (cellDataFeatures) -> cellDataFeatures.getValue().dateReceivedProperty());

        // Setting how the date and time should be displayed in the cell
        DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        dateColumn.setCellFactory(
                (column) -> new TableCell<EmailBean, LocalDateTime>() {

                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(localDateTimeFormatter.format(item));
                        }
                    }
                }
        );

        // Setup event on double click, call onEmailDoubleClicked (if it is initialized)
        mailTable.onMouseClickedProperty().setValue(
                (mouseEvent) -> {
                    if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2) {
                        EmailBean selectedEmail = mailTable.getSelectionModel().getSelectedItem();

                        if (onEmailDoubleClicked != null)
                            onEmailDoubleClicked.accept(selectedEmail);
                    }
                }
        );

        mailTable.setOnDragDetected(createEmailDragDetectedHandler());
        mailTable.setOnDragDone(createEmailDragDoneHandler());

        LOG.info("Email List initialized");
    }

    /*
     * Helper method to create an event handler when a drag is detected
     */
    private EventHandler<MouseEvent> createEmailDragDetectedHandler() {
        EventHandler<MouseEvent> emailDragDetectedHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (mailTable.getSelectionModel().getSelectedItem() != null) {
                    Dragboard db = mailTable.startDragAndDrop(TransferMode.ANY);

                    // We will put the EmailBean ID in the clipboard so that we can use it to change its folder
                    int emailBeanId = mailTable.getSelectionModel().getSelectedItem().getId();
                    // We need the ID to be a string to put it into the clipboard content
                    String stringId = String.valueOf(emailBeanId);

                    ClipboardContent content = new ClipboardContent();

                    // Using a custom DataFormat so that in setOnDragOver() we could
                    // easily identify whether the target is compatible with the target
                    // (we don't want to accept any strings, as it might lead to an error)
                    content.put(EMAIL_BEAN_ID, stringId);

                    db.setContent(content);

                    event.consume();
                }
            }
        };

        return emailDragDetectedHandler;
    }

    /**
     * Helper method to create an event handler when a drag is done
     */
    private EventHandler<DragEvent> createEmailDragDoneHandler() {
        EventHandler<DragEvent> emailDragDoneHandler = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                if (event.getTransferMode() == TransferMode.MOVE) {
                    LOG.info("Drag Done");
                    emails.remove(mailTable.getSelectionModel().getSelectedItem());
                }


                event.consume();
            }
        };

        return emailDragDoneHandler;
    }

    /**
     * Method to change the emails displayed in the email list
     * @param folder The folder to display
     */
    public void displayEmailsInFolder(Folder folder) {
        LOG.info("Email List folder changed");
        currentFolder.set(folder);
        emails.setValue(FXCollections.observableArrayList(folder.getEmailBeans()));
    }

    /**
     * Sets the callback that is executed when an email is double clicked
     * @param onEmailDoubleClicked the callback that is executed when an email is double clicked
     */
    public void setOnEmailDoubleClicked(Consumer<EmailBean> onEmailDoubleClicked) {
        this.onEmailDoubleClicked = onEmailDoubleClicked;
    }

    /**
     * Sets the callback that is executed when the reply menu item is pressed
     * @param onReplyClicked the callback that is executed when the reply menu item is pressed
     */
    public void setOnReplyClicked(Consumer<EmailBean> onReplyClicked) {
        this.onReplyClicked = onReplyClicked;
    }

    /**
     * Sets the callback that is executed when the "reply all" menu item is pressed
     * @param onReplyAllClicked the callback that is executed when the "reply all" menu is pressed
     */
    public void setOnReplyAllClicked(Consumer<EmailBean> onReplyAllClicked) {
        this.onReplyAllClicked = onReplyAllClicked;
    }

    /**
     * Sets the callback that is executed when the forward menu item is pressed
     * @param onForwardClicked the callback that is executed when the forward menu item is pressed
     */
    public void setOnForwardClicked(Consumer<EmailBean> onForwardClicked) {
        this.onForwardClicked = onForwardClicked;
    }

    /**
     * Sets the callback that is executed when the delete menu item is pressed
     * @param onDeleteClicked the callback that is executed when the delete menu item is pressed
     */
    public void setOnDeleteClicked(Consumer<EmailBean> onDeleteClicked) {
        this.onDeleteClicked = onDeleteClicked;
    }

    /**
     * Sets the image for the app icon (so that it will be easy to set the icon of alerts)
     * @param appIcon The image for the app icon
     */
    public void setAppIcon(Image appIcon) {
        this.appIcon = appIcon;
    }
}
