package me.yanik.gui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import jodd.mail.MailServer;
import jodd.mail.ReceiveMailSession;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import me.yanik.persistence.ConnectionManager;
import me.yanik.persistence.DaoException;
import me.yanik.util.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ConfigurationController {
    private final static Logger LOG = LoggerFactory.getLogger(ConfigurationController.class);

    private Image appIcon;

    private Action onFinish;

    @FXML
    private VBox rootVBox;

    @FXML
    private TextField personalName;

    @FXML
    private TextField userEmail;

    @FXML
    private PasswordField userPassword;

    @FXML
    private TextField imapURL;

    @FXML
    private TextField imapPort;

    @FXML
    private TextField smtpURL;

    @FXML
    private TextField smtpPort;

    @FXML
    private TextField dbURL;

    @FXML
    private TextField dbUser;

    @FXML
    private PasswordField dbPassword;

    @FXML
    private Button finishButton;

    @FXML
    private ResourceBundle resources;

    @FXML
    private Label statusLabel;

    @FXML
    private Pane progressPlaceholder;

    private List<TextField> allTextFields;

    @FXML
    private ToggleGroup toggleGroup;

    private Locale locale = new Locale("en", "CA");

    @FXML
    public void initialize() {
        toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle oldToggle, Toggle newToggle) {
                if (toggleGroup.getSelectedToggle() != null) {
                    RadioButton button = (RadioButton) toggleGroup.getSelectedToggle();
                    // Didn't implement
                }
            }
        });

        // Creating List that contains all text fields for easier
        // manipulation of all text fields (i.e. if we need to manipulate all of them at once)
        allTextFields = new ArrayList<>();

        allTextFields.add(personalName);
        allTextFields.add(userEmail);
        allTextFields.add(userPassword);
        allTextFields.add(imapURL);
        allTextFields.add(imapPort);
        allTextFields.add(smtpURL);
        allTextFields.add(smtpPort);
        allTextFields.add(dbURL);
        allTextFields.add(dbUser);
        allTextFields.add(dbPassword);

        finishButton.setOnAction(
                (actionEvent) -> {
                    Thread validationThread = new Thread(
                            () -> {
                                ProgressIndicator progressIndicator = new ProgressIndicator();

                                Platform.runLater(() -> {
                                    statusLabel.setText(resources.getString("verifying_input"));
                                    progressIndicator.setPrefSize(15, 15);
                                    progressIndicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
                                    progressPlaceholder.getChildren().add(progressIndicator);
                                });

                                if (areAllFieldsFilled()) {
                                    if (validateFields()) {
                                        createPropertiesFile();
                                        Platform.runLater(() -> onFinish.execute());
                                    }
                                } else {
                                    Platform.runLater(() -> displayErrorEmptyFields());
                                }

                                Platform.runLater(() -> {
                                    statusLabel.setText("");
                                    progressPlaceholder.getChildren().remove(progressIndicator);
                                });
                            }
                    );

                    validationThread.start();
                }
        );

        tryToPopulateFields();

        LOG.info("Configuration Controller initialized");
    }

    /**
     * Helper method to validate all the fields in the configuration window
     *
     * Expected to be run on a separate thread.
     */
    private boolean validateFields() {
        if (!isValidImapPort())
            Platform.runLater(() -> showError(resources.getString("the.imap.port.is.not.an.integer")));
        else if (!isValidSmtpPort())
            Platform.runLater(() -> showError(resources.getString("the.smtp.port.is.not.an.integer")));
        else if (!isValidSmtp())
            Platform.runLater(() -> showError(resources.getString("failed.to.log.into.the.smtp.server")));
        else if (!isValidImap())
            Platform.runLater(() -> showError(resources.getString("failed.to.log.into.the.imap.server")));
        else if (!isValidDbInfo())
            Platform.runLater(() -> showError(resources.getString("failed.to.log.into.the.database")));
        else
            return true;

        return false;
    }

    /**
     * Helper method to verify if IMAP port is an integer
     */
    private boolean isValidImapPort() {
        try {
            Integer.parseInt(imapPort.getText());
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    /**
     * Helper method to verify if SMTP port is an integer
     */
    private boolean isValidSmtpPort() {
        try {
            Integer.parseInt(smtpPort.getText());
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    /**
     * Helper method to validate IMAP information
     */
    private boolean isValidImap() {
        String url = imapURL.getText();
        int port = Integer.parseInt(imapPort.getText());
        String user = userEmail.getText();
        String password = userPassword.getText();

        ReceiveMailSession receiveMailSession = null;
        try {
            receiveMailSession = MailServer.create()
                    .ssl(true)
                    .host(url)
                    .port(port)
                    .auth(user, password)
                    .buildImapMailServer()
                    .createSession();

            receiveMailSession.open();

            // Do nothing if session opened

            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (receiveMailSession != null)
                receiveMailSession.close();
        }
    }

    /**
     * Helper method to validate SMTP information
     */
    private boolean isValidSmtp() {
        String url = smtpURL.getText();
        int port = Integer.parseInt(smtpPort.getText());
        String user = userEmail.getText();
        String password = userPassword.getText();

        SendMailSession sendMailSession = null;
        try {
            sendMailSession = MailServer.create()
                    .ssl(true)
                    .host(url)
                    .port(port)
                    .auth(user, password)
                    .buildSmtpMailServer()
                    .createSession();

            sendMailSession.open();

            // Do nothing if session opened

            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (sendMailSession != null)
                sendMailSession.close();
        }
    }

    /**
     * Helper method to validate the database information
     */
    private boolean isValidDbInfo() {
        try {
            String url = dbURL.getText();
            String user = dbUser.getText();
            String password = dbPassword.getText();

            ConnectionManager connectionManager = new ConnectionManager(url, user, password);
            connectionManager.openConnection();
        } catch (DaoException e) {
            return false;
        }

        return true;
    }

    /**
     * Helper method to show an error
     */
    private void showError(String errorMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
        alert.setTitle(resources.getString("config_error_title"));
        alert.setHeaderText(resources.getString("config_error_header"));
        alert.setContentText(resources.getString("config_error_message") + errorMessage);

        alert.showAndWait();
    }

    /**
     * Helper method to try to populate the configuration window (succeeds if the properties file exists)
     */
    private void tryToPopulateFields() {
        Properties properties = new Properties();

        Path configFile = Paths.get("config.properties");

        // File must exist
        if (Files.exists(configFile)) {
            try (InputStream propFileStream = Files.newInputStream(configFile);) {
                properties.load(propFileStream);
            } catch (IOException e) {
                String errorMessage = "Unexpected error when loading properties";
                LOG.error(errorMessage + ": " + e.getMessage());
                throw new RuntimeException(e);
            }

            personalName.setText(properties.getProperty("personalName"));
            userEmail.setText(properties.getProperty("userEmail"));
            userPassword.setText(properties.getProperty("userPassword"));
            imapURL.setText(properties.getProperty("imapURL"));
            imapPort.setText(properties.getProperty("imapPort"));
            smtpURL.setText(properties.getProperty("smtpURL"));
            smtpPort.setText(properties.getProperty("smtpPort"));
            dbURL.setText(properties.getProperty("dbURL"));
            dbUser.setText(properties.getProperty("dbUser"));
            dbPassword.setText(properties.getProperty("dbPassword"));

            LOG.info("Configuration window populated");
        }
    }

    /**
     * Helper method create a properties file
     */
    private void createPropertiesFile() {
        Properties properties = new Properties();

        properties.setProperty("personalName", personalName.getText());
        properties.setProperty("userEmail", userEmail.getText());
        properties.setProperty("userPassword", userPassword.getText());
        properties.setProperty("imapURL", imapURL.getText());
        properties.setProperty("imapPort", imapPort.getText());
        properties.setProperty("smtpURL", smtpURL.getText());
        properties.setProperty("smtpPort", smtpPort.getText());
        properties.setProperty("dbURL", dbURL.getText());
        properties.setProperty("dbUser", dbUser.getText());
        properties.setProperty("dbPassword", dbPassword.getText());

        Path txtFile = Paths.get("config.properties");

        try (OutputStream propFileStream = Files.newOutputStream(txtFile)) {
            properties.store(propFileStream, resources.getString("emailClientSettings"));
        } catch (IOException e) {
            String errorMessage = "Unecpected error when creating properties file";
            LOG.error(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }

        LOG.info("Properties file created");
    }

    /**
     * Helper method to display that there are empty fields remaining (error)
     */
    private void displayErrorEmptyFields() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
        alert.setTitle(resources.getString("emptyFields"));
        alert.setHeaderText(resources.getString("thereAreEmptyFields"));
        alert.setContentText(resources.getString("makeSureNoEmptyFields"));

        alert.showAndWait();
    }

    /**
     * Helper method to verify if there are empty fields in the window
     */
    private boolean areAllFieldsFilled() {
        return allTextFields.stream()
                .noneMatch((textField) -> textField.getText().isEmpty());
    }

    /**
     * Sets the callback that gets called when the configuration window finishes
     * @param onFinish the callback that gets called when the configuration window finishes
     */
    public void setOnFinish(Action onFinish) {
        this.onFinish = onFinish;
    }

    /**
     * Gets the stage of the configuration window
     * @return the stage of the configuration window
     */
    public Stage getStage() {
        return (Stage) rootVBox.getScene().getWindow();
    }

    /**
     * Sets the image for the app icon (so that it will be easy to set the icon of alerts)
     * @param appIcon The image for the app icon
     */
    public void setAppIcon(Image appIcon) {
        this.appIcon = appIcon;
    }
}
