package me.yanik.gui;

import javafx.beans.property.*;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebView;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.util.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class DisplayEmailController {
    private final static Logger LOG = LoggerFactory.getLogger(DisplayEmailController.class);

    private Image appIcon;

    private Consumer<EmailBean> onReplyButtonClick;
    private Consumer<EmailBean> onReplyAllButtonClick;
    private Consumer<EmailBean> onForwardButtonClick;
    private Consumer<EmailBean> onDeleteButtonClick;
    private Action onCloseButtonClick;

    // Used to display embedded attachments
    private String htmlWithoutCids;

    @FXML
    private VBox rootVBox;

    @FXML
    private Button replyButton;

    @FXML
    private Button replyAllButton;

    @FXML
    private Button forwardButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button closeButton;

    @FXML
    private Label fromLabel;

    @FXML
    private Label toLabel;

    @FXML
    private Label ccLabel;

    @FXML
    private Label subjectLabel;

    @FXML
    private Label dateLabel;

    @FXML
    private Label priorityLabel;

    @FXML
    private Button toggleContent;

    @FXML
    private AnchorPane messageContainer;

    @FXML
    private HBox attachmentsHBox;

    @FXML
    private ResourceBundle resources;

    private ObjectProperty<EmailBean> emailToDisplay = new SimpleObjectProperty<>();
    private BooleanProperty displayHtml = new SimpleBooleanProperty();

    private WebView htmlMessageArea;
    private TextArea plainTextArea;

    @FXML
    public void initialize() {
        htmlMessageArea = new WebView();
        plainTextArea = new TextArea();

        // Listeners that executes when the emailToDisplay changes
        // Properly places the email bean information into the appropriate labels
        // and text areas
        emailToDisplay.addListener(
                (observable, oldValue, newValue) -> {
                    LOG.info("Email to display changed");

                    String fromEmail = newValue.getFrom().getEmail();
                    String toEmails = Arrays.stream(newValue.getTo())
                            .map(address -> address.getEmail())
                            .collect(Collectors.joining("; "));
                    String ccEmails = newValue.getCc() == null ? null :
                            Arrays.stream(newValue.getCc())
                                .map(address -> address.getEmail())
                                .collect(Collectors.joining("; "));
                    String subject = newValue.getSubject();
                    String plainTextMessage = newValue.getTextMessage();
                    String htmlMessage = newValue.getHtmlMessage();
                    String priority = String.valueOf(newValue.getPriority());

                    // Defining DateTimeFormatter to define the displaying of the date
                    DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
                    String date = localDateTimeFormatter.format(newValue.getDateReceived());

                    fromLabel.setText(fromEmail);
                    toLabel.setText(toEmails);
                    ccLabel.setText(ccEmails);
                    subjectLabel.setText(subject);
                    plainTextArea.setText(plainTextMessage);
                    htmlMessageArea.getEngine().loadContent(htmlWithoutCids);
                    dateLabel.setText(date);
                    priorityLabel.setText(priority);

                    displayAttachments();
                }
        );


        // Changes the display of the messages html or plain text
        displayHtml.addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue == true) {
                        if (messageContainer.getChildren().contains(plainTextArea)) {
                            messageContainer.getChildren().remove(plainTextArea);
                        }
                        setAllAnchorsToZero(messageContainer, htmlMessageArea);
                        messageContainer.getChildren().add(htmlMessageArea);

                        toggleContent.setText(resources.getString("plainText"));
                    } else if (newValue == false){
                        if (messageContainer.getChildren().contains(htmlMessageArea)) {
                            messageContainer.getChildren().remove(htmlMessageArea);
                        }
                        setAllAnchorsToZero(messageContainer, plainTextArea);
                        messageContainer.getChildren().add(plainTextArea);

                        toggleContent.setText(resources.getString("html"));
                    }
                }
        );


        // The event handler for the button that toggles between html and plain text
        toggleContent.setOnAction(
                (actionEvent) -> {
                    LOG.info("HTML/Plain text toggled");
                    boolean isHtmlDisplayed = displayHtml.get();
                    displayHtml.setValue(!isHtmlDisplayed);
                }
        );

        // Initially html is displayed
        displayHtml.setValue(true);

        replyButton.setOnAction(
                (actionEvent) -> {
                    if (onReplyButtonClick != null)
                        onReplyButtonClick.accept(emailToDisplay.get());
                }
        );

        replyAllButton.setOnAction(
                (actionEvent) -> {
                    if (onReplyAllButtonClick != null)
                        onReplyAllButtonClick.accept(emailToDisplay.get());
                }
        );

        closeButton.setOnAction(
                (actionEvent) -> {
                    if (onCloseButtonClick != null)
                        onCloseButtonClick.execute();
                }
        );

        forwardButton.setOnAction(
                (actionEvent) -> {
                    if (onForwardButtonClick != null)
                        onForwardButtonClick.accept(emailToDisplay.get());
                }
        );

        deleteButton.setOnAction(
                (actionEvent) -> {
                    if (onDeleteButtonClick != null)
                        onDeleteButtonClick.accept(emailToDisplay.get());
                }
        );
    }

    private void setAllAnchorsToZero(AnchorPane anchorPane, Node child) {
        anchorPane.setTopAnchor(child, 0.0);
        anchorPane.setRightAnchor(child, 0.0);
        anchorPane.setBottomAnchor(child, 0.0);
        anchorPane.setLeftAnchor(child, 0.0);
    }

    /**
     * Helper method that displays the name of the attachments below the message body
     */
    private void displayAttachments() {
        if (emailToDisplay.get().getAttachments() == null)
            return;

        attachmentsHBox.getChildren().removeAll(attachmentsHBox.getChildren());

        List<Label> labels = new ArrayList<>();
        for (Attachment attachment: emailToDisplay.get().getAttachments()) {
            Label label = new Label();
            label.setText(attachment.getName());
            label.addEventHandler(MouseEvent.MOUSE_PRESSED,
                    (mouseEvent) -> {
                        openAttachment(attachment);
                    });
            label.getStyleClass().add("view-attachment-label");
            labels.add(label);
        }

        for (Label label: labels) {
            attachmentsHBox.getChildren().add(label);
        }
    }

    /**
     * Opens an attachment with the default application assigned to the file extension
     * @param attachment The attachment to open
     */
    private void openAttachment(Attachment attachment) {
        try {
            // Opening the temporary file using default associated to it
            Desktop desktop = Desktop.getDesktop();
            desktop.open(createTempFile(attachment));
        } catch (IOException e) {
            String errorMessage = "Unexpected error when trying to open an attachment";
            LOG.error(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * Helper method to return an html message that has all the cids
     * replaced to srcs (to be able to display the embedded attachments)
     *
     * Returns a string that has all the cids replaced with a file src
     */
    private String replaceCidsInHtml(EmailBean emailBean) {
        if (emailBean.getAttachments() == null)
            return emailBean.getHtmlMessage();

        String newHtml = emailBean.getHtmlMessage();

        for (Attachment attachment: emailBean.getAttachments()) {
            File tempFile = createTempFile(attachment);
            String filePath = tempFile.toURI().toString();
            newHtml = newHtml.replaceAll("cid:" + attachment.getContentId(), filePath);
        }

        return newHtml;
    }

    /**
     * Helper method to create a temporary file for an attachment
     */
    private File createTempFile(Attachment attachment) {
        InputStream in = new ByteArrayInputStream(attachment.getContent());

        try {
            // Creating directory if doesn't exist
            new File("client-images").mkdirs();

            Path tempFilePath = Paths.get("client-images" + File.separator + attachment.getName());

            try (OutputStream out = Files.newOutputStream(tempFilePath)) {
                // Copying input stream into output stream
                byte[] byteBuffer = new byte[1024];
                int bytesRead;
                while (true) {
                    if ((bytesRead = in.read(byteBuffer)) == -1)
                        break;
                    out.write(byteBuffer, 0, bytesRead);
                }
            }

            return tempFilePath.toFile();
        } catch (IOException e) {
            String errorMessage = "Unexpected error when creating temporary file for attachment";
            LOG.error(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * Sets the email that will be displays (the change listener for this property will get called)
     * @param emailBean the email that will be displays
     */
    public void displayEmail(EmailBean emailBean) {
        htmlWithoutCids = replaceCidsInHtml(emailBean);
        emailToDisplay.set(emailBean);
    }

    /**
     * Sets the call back that gets called when the close button is pressed
     * @param onCloseButtonClick the call back that gets called when the close button is pressed
     */
    public void setOnCloseButtonClick(Action onCloseButtonClick) {
        this.onCloseButtonClick = onCloseButtonClick;
    }

    /**
     * Sets the call back that gets called when the reply button is pressed
     * @param onReplyButtonClick the call back that gets called when the reply button is pressed
     */
    public void setOnReplyButtonClick(Consumer<EmailBean> onReplyButtonClick) {
        this.onReplyButtonClick = onReplyButtonClick;
    }

    /**
     * Sets the call back that gets called when the reply all button is pressed
     * @param onReplyAllButtonClick the call back that gets called when the reply all button is pressed
     */
    public void setOnReplyAllButtonClick(Consumer<EmailBean> onReplyAllButtonClick) {
        this.onReplyAllButtonClick = onReplyAllButtonClick;
    }

    /**
     * Sets the call back that gets called when the forward button is pressed
     * @param onForwardButtonClick the call back that gets called when the forward button is pressed
     */
    public void setOnForwardButtonClick(Consumer<EmailBean> onForwardButtonClick) {
        this.onForwardButtonClick = onForwardButtonClick;
    }

    /**
     * Sets the call back that gets called when the delete button is pressed
     * @param onDeleteButtonClick the call back that gets called when the delete button is pressed
     */
    public void setOnDeleteButtonClick(Consumer<EmailBean> onDeleteButtonClick) {
        this.onDeleteButtonClick = onDeleteButtonClick;
    }

    /**
     * Sets the image for the app icon (so that it will be easy to set the icon of alerts)
     * @param appIcon The image for the app icon
     */
    public void setAppIcon(Image appIcon) {
        this.appIcon = appIcon;
    }
}
