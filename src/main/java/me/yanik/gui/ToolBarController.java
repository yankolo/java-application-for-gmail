package me.yanik.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import me.yanik.util.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class ToolBarController {
    private final static Logger LOG = LoggerFactory.getLogger(ToolBarController.class);

    private Image appIcon;

    private Action onWriteButtonClick;
    private Action onFolderAdd;
    private Action onConfigClick;

    @FXML
    private Button writeButton;

    @FXML
    private Button addFolderButton;

    @FXML
    private Button configurationButton;

    @FXML
    private ResourceBundle resources;

    @FXML
    public void initialize() {
        writeButton.setOnAction(
                (actionEvent) -> {
                    if (onWriteButtonClick != null)
                        onWriteButtonClick.execute();
                }
        );

        addFolderButton.setOnAction(
                (actionEvent) -> {
                        if (onFolderAdd != null)
                            onFolderAdd.execute();
                }
        );

        configurationButton.setOnAction(
                (actionEvent) -> {
                    if (onConfigClick != null)
                        onConfigClick.execute();
                }
        );

        LOG.info("Tool bar has been initialized");
    }

    /**
     * Sets the callback that is called when the write button is pressed
     * @param onWriteButtonClick the callback that is called when the write button is pressed
     */
    public void setOnWriteButtonClick(Action onWriteButtonClick) {
        this.onWriteButtonClick = onWriteButtonClick;
    }

    /**
     * Sets the callback that is called when the folder add button is pressed
     * @param onFolderAdd the callback that is called when the folder add button is pressed
     */
    public void setOnFolderAdd(Action onFolderAdd) {
        this.onFolderAdd = onFolderAdd;
    }

    /**
     * Sets the callback that is called when the settings button is pressed
     * @param onConfigClick the callback that is called when the settings button is pressed
     */
    public void setOnConfigClick(Action onConfigClick) {
        this.onConfigClick = onConfigClick;
    }

    /**
     * Sets the image for the app icon (so that it will be easy to set the icon of alerts)
     * @param appIcon The image for the app icon
     */
    public void setAppIcon(Image appIcon) {
        this.appIcon = appIcon;
    }
}
