package me.yanik.gui;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import me.yanik.persistence.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class GUIManager extends Application {
    private final static Logger LOG = LoggerFactory.getLogger(GUIManager.class);
    private Image appIcon;

    @Override
    public void start(Stage primaryStage) throws IOException, DaoException {
        this.appIcon = new Image(GUIManager.class
                .getResourceAsStream("/gui/icons/app-icon.png"));
        primaryStage.getIcons().addAll(appIcon);
        primaryStage.setTitle(ResourceBundle.getBundle("ClientBundle").getString("appTitle"));

        if (!isPropertyExists()) {
            loadConfigurationWindow(primaryStage, true);
        } else {
            loadMainWindow(primaryStage);
        }
    }

    /**
     * Verifies if the property file exists
     */
    private boolean isPropertyExists() {
        Path configFile = Paths.get("config.properties");

        if (Files.exists(configFile)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Loads the FXML of the configuration window, configures the configuration window, and shows the stage
     */
    private void loadConfigurationWindow(Stage stage, boolean isInitialConfigWindow) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("ClientBundle"));
            loader.setLocation(getClass().getResource("/gui/fxml/ConfigurationWindow.fxml"));
            Parent window = loader.load();
            ConfigurationController configurationController = loader.getController();

            LOG.info("Configuration FXML Loaded");

            if (isInitialConfigWindow) {
                setupInitialConfigurationWindow(configurationController);
            } else {
                setupNormalConfigurationWindow(configurationController);
            }

            Scene scene = new Scene(window);
            stage.setScene(scene);
            stage.show();
            LOG.info("Configuration Stage Showed");
        } catch (IOException e) {
            String errorMessage = "Unexpected Error when loading FXML";
            LOG.error(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * Loads the FXML of the main window, configures it, and shows the stage
     */
    private void loadMainWindow(Stage stage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("ClientBundle"));
            loader.setLocation(getClass().getResource("/gui/fxml/MainWindow.fxml"));
            Parent window = loader.load();
            MainWindowController mainWindowController = loader.getController();

            LOG.info("Main Window FXML Loaded");

            setupMainWindow(mainWindowController);

            mainWindowController.configure();

            Scene scene = new Scene(window);
            stage.setScene(scene);
            stage.show();
            stage.setMaximized(true);
            LOG.info("Main Window Stage Showed");
        } catch (IOException e) {
            String errorMessage = "Unexpected Error when loading FXML";
            LOG.error(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * Setting the appropriate callbacks in the configuration controller (if it is launched before
     * the main window)
     */
    private void setupInitialConfigurationWindow(ConfigurationController configurationController) {
        configurationController.setOnFinish(
                () -> {
                    LOG.info("Initial configuration window finished");
                    Stage primaryStage = configurationController.getStage();
                    loadMainWindow(new Stage());
                    primaryStage.close();
                }
        );

        LOG.info("Callbacks set for initial configuration window");
    }

    /**
     * Setting the appropriate callbacks in the configuration controller (if it is launched after
     * the main window)
     */
    private void setupNormalConfigurationWindow(ConfigurationController configurationController) {
        configurationController.setOnFinish(
                () -> {
                    LOG.info("Normal configuration window finished");
                    Stage configStage = configurationController.getStage();
                    configStage.close();
                }
        );

        LOG.info("Callbacks set for normal configuration window");
    }

    /**
     * Setting the appropriate callbacks in the main window controller
     */
    private void setupMainWindow(MainWindowController mainWindowController) {
        mainWindowController.setAppIcon(appIcon);

        mainWindowController.setOnConfigClick(
                () -> {
                    LOG.info("Configuration window requested for display");
                    Stage stage = new Stage();
                    stage.getIcons().addAll(appIcon);
                    stage.setTitle(ResourceBundle.getBundle("ClientBundle").getString("configTitle"));
                    loadConfigurationWindow(stage, false);
                }
        );

        LOG.info("Callbacks set for main window");
    }

}
