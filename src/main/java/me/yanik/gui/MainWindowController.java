package me.yanik.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import me.yanik.business.MailController;
import me.yanik.data.EmailBean;
import me.yanik.persistence.ClientDAO;
import me.yanik.persistence.ClientDAOImpl;
import me.yanik.persistence.ConnectionManager;
import me.yanik.persistence.DaoException;
import me.yanik.util.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;
import java.util.ResourceBundle;

public class MainWindowController {
    private final static Logger LOG = LoggerFactory.getLogger(MainWindowController.class);

    private Image appIcon;

    // DataFormat that represents an email bean id. Used when
    // dragging an email over a folder. It makes it possible to easily
    // identify whether an email is being dragged.
    public static final DataFormat EMAIL_BEAN_ID = new DataFormat("text/email_bean_id");

    private Action onConfigClick;

    private ClientDAO clientDAO;
    private String userPersonalName;
    private String userEmail;
    private MailController mailController;

    @FXML
    private MenuItem settingsMenuItem;

    @FXML
    private MenuItem exitMenuItem;

    @FXML
    private MenuItem addFolderMenuItem;

    @FXML
    private MenuItem writeEmailMenuItem;

    @FXML
    private MenuItem helpMenuItem;

    @FXML
    private AnchorPane topBlock;

    @FXML
    private AnchorPane leftBlock;

    @FXML
    private SplitPane centerBlock;

    @FXML
    private AnchorPane centerTop;

    private AnchorPane centerBottom;

    @FXML
    private HBox statusBar;

    @FXML
    private ResourceBundle resources;

    private Node toolBar;
    private Node folderList;
    private Node mailList;
    private Node viewEmail;
    private Node composeEmail;

    private ToolBarController toolBarController;
    private FolderListController folderListController;
    private EmailListController emailListController;
    private DisplayEmailController displayEmailController;
    private ComposeEmailController composeEmailController;

    @FXML
    public void initialize() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("ClientBundle"));
            loader.setLocation(getClass().getResource("/gui/fxml/ToolBar.fxml"));
            toolBar = loader.load();
            toolBarController = loader.getController();

            loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("ClientBundle"));
            loader.setLocation(getClass().getResource("/gui/fxml/FolderList.fxml"));
            folderList = loader.load();
            folderListController = loader.getController();

            loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("ClientBundle"));
            loader.setLocation(getClass().getResource("/gui/fxml/EmailList.fxml"));
            mailList = loader.load();
            emailListController = loader.getController();

            loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("ClientBundle"));
            loader.setLocation(getClass().getResource("/gui/fxml/DisplayEmail.fxml"));
            viewEmail = loader.load();
            displayEmailController = loader.getController();

            loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("ClientBundle"));
            loader.setLocation(getClass().getResource("/gui/fxml/ComposeEmail.fxml"));
            composeEmail = loader.load();
            composeEmailController = loader.getController();

            LOG.info("All main window components have loaded");
        } catch (IOException e) {
            String errorMessage = "Unexpected error when loading FXML";
            LOG.error(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * This method configures all the components of the main window (for example, all the callbacks
     * and the component containers)
     */
    public void configure() {
        try {
            readInfoFromProperties();

            folderListController.setClientDAO(clientDAO);

            centerBottom = new AnchorPane();
            centerBottom.setMinHeight(60.0);

            setTopBlock(toolBar);
            setLeftBlock(folderList);
            setCenterTop(mailList);

            setupMenuItems();
            setupToolBarController();
            setupFolderListController();
            setupEmailListController();
            setupDisplayEmailController();
            setupComposeEmailController();

            folderListController.loadData();

            listenForEmails();

            LOG.info("Main window successfully configured");
        } catch (DaoException e) {
            String errorMessage = "Unexpected error when configuring main window";
            LOG.error(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * Helper method to set the top component
     */
    private void setTopBlock(Node node) {
        changeChildComponent(topBlock, node);
        LOG.info("Top component set");
    }

    /**
     * Helper method to set the left component
     */
    private void setLeftBlock(Node node) {
        changeChildComponent(leftBlock, node);
        LOG.info("Left component set");
    }

    /**
     * Helper method to set the center top component
     */
    private void setCenterTop(Node node) {
        changeChildComponent(centerTop, node);
        LOG.info("Center top component set");
    }

    /**
     * Helper method to set the center bottom component
     */
    private void setCenterBottom(Node node) {
        if (centerBlock.getItems().contains(centerBottom) == false) {
            centerBlock.getItems().add(centerBottom);
        }

        changeChildComponent(centerBottom, node);

        LOG.info("Center bottom component set");
    }

    /**
     * Helper method to change the child of an anchor pane (removes child, places new child,
     * sets anchor points)
     */
    private void changeChildComponent(AnchorPane parentComponent, Node newChildComponent) {
        parentComponent.getChildren().clear();

        // Set anchor points so that the child will take the whole area of the AnchorPane
        parentComponent.setTopAnchor(newChildComponent, 0.0);
        parentComponent.setRightAnchor(newChildComponent, 0.0);
        parentComponent.setBottomAnchor(newChildComponent, 0.0);
        parentComponent.setLeftAnchor(newChildComponent, 0.0);

        parentComponent.getChildren().add(newChildComponent);
    }

    /**
     * Sets all the callbacks for the menu items
     */
    private void setupMenuItems() {
        settingsMenuItem.setOnAction(
                (actionEvent) -> {
                    LOG.info("settingsMenuItem clicked");
                    if (onConfigClick != null)
                        onConfigClick.execute();
                }
        );

        exitMenuItem.setOnAction(
                (actionEvent) -> {
                    LOG.info("exitMenuItem clicked");
                    Platform.exit();
                }
        );

        addFolderMenuItem.setOnAction(
                (actionEvent) -> {
                    LOG.info("addFolderMenuItem clicked");
                    folderListController.addNewFolder();
                }
        );

        writeEmailMenuItem.setOnAction(
                (actionEvent) -> {
                    LOG.info("writeEmailMenuItem clicked");
                    composeEmailController.writeNewEmail();
                    setCenterBottom(composeEmail);
                }
        );

        helpMenuItem.setOnAction(
                (actionEvent) -> {
                    LOG.info("helpMenuItem clicked");
                    displayHelp();
                }
        );
    }

    /**
     * Sets all the callbacks for the tool bar controller
     */
    private void setupToolBarController() {
        toolBarController.setAppIcon(appIcon);

        toolBarController.setOnWriteButtonClick(
                () -> {
                    LOG.info("onWriteButtonClick executed");
                    composeEmailController.writeNewEmail();
                    setCenterBottom(composeEmail);
                }
        );

        toolBarController.setOnFolderAdd(
                () -> {
                    LOG.info("onFolderAdd executed");
                    folderListController.addNewFolder();
                }
        );

        toolBarController.setOnConfigClick(onConfigClick);
    }

    /**
     * Sets all the callbacks for the folder List controller
     */
    private void setupFolderListController() {
        folderListController.setAppIcon(appIcon);

        folderListController.setOnFolderSelected(
                (folder) -> {
                    LOG.info("onFolderSelected executed");
                    emailListController.displayEmailsInFolder(folder);
                }
        );

        folderListController.setOnSelectedFolderUpdated(
                (folder) -> {
                    LOG.info("onSelectedFolderUpdated executed");
                    emailListController.displayEmailsInFolder(folder);
                }
        );
    }

    /**
     * Sets all the callbacks for the email list controller
     */
    private void setupEmailListController() {
        emailListController.setAppIcon(appIcon);

        emailListController.setOnEmailDoubleClicked(
                (emailBean) -> {
                    LOG.info("onEmailDoubleClicked executed");
                    displayEmailController.displayEmail(emailBean);
                    setCenterBottom(viewEmail);
                }
        );

        emailListController.setOnReplyClicked(
                (emailBean) -> {
                    LOG.info("onReplyClicked executed");
                    composeEmailController.reply(emailBean);
                    setCenterBottom(composeEmail);
                }
        );

        emailListController.setOnReplyAllClicked(
                (emailBean) -> {
                    LOG.info("onReplyAllClicked executed");
                    composeEmailController.replyAll(emailBean);
                    setCenterBottom(composeEmail);
                }
        );

        emailListController.setOnForwardClicked(
                (emailBean) -> {
                    LOG.info("onForwardClicked executed");
                    composeEmailController.forward(emailBean);
                    setCenterBottom(composeEmail);
                }
        );

        emailListController.setOnDeleteClicked(
                (emailBean) -> {
                    LOG.info("onDeleteClicked executed");
                    centerBlock.getItems().remove(centerBottom);
                    folderListController.deleteEmail(emailBean);
                }
        );
    }

    /**
     * Sets all the callbacks for the display email controller
     */
    private void setupDisplayEmailController() {
        displayEmailController.setAppIcon(appIcon);

        displayEmailController.setOnCloseButtonClick(
                () -> {
                    LOG.info("onCloseButtonClick executed");
                    centerBlock.getItems().remove(centerBottom);
                }
        );

        displayEmailController.setOnReplyButtonClick(
                (emailBean) -> {
                    LOG.info("onReplyButtonClick executed");
                    composeEmailController.reply(emailBean);
                    setCenterBottom(composeEmail);
                }
        );

        displayEmailController.setOnReplyAllButtonClick(
                (emailBean) -> {
                    LOG.info("onReplyAllButtonClick executed");
                    composeEmailController.replyAll(emailBean);
                    setCenterBottom(composeEmail);
                }
        );

        displayEmailController.setOnForwardButtonClick(
                (emailBean) -> {
                    LOG.info("onForwardButtonClick executed");
                    composeEmailController.forward(emailBean);
                    setCenterBottom(composeEmail);
                }
        );

        displayEmailController.setOnDeleteButtonClick(
                (emailBean) -> {
                    LOG.info("onDeleteButtonClick executed");
                    centerBlock.getItems().remove(centerBottom);
                    folderListController.deleteEmail(emailBean);
                }
        );

    }

    /**
     * Sets all the callbacks for the compose email controller
     */
    private void setupComposeEmailController() {
        composeEmailController.setUserPersonalName(userPersonalName);
        composeEmailController.setUserEmail(userEmail);
        composeEmailController.setMailController(mailController);
        composeEmailController.setStatusBar(statusBar);
        composeEmailController.setAppIcon(appIcon);

        composeEmailController.setOnEmailSent(
                (emailBean) -> {
                    folderListController.addEmailToOutbox(emailBean);
                }
        );

        composeEmailController.setOnCloseButtonClick(
                () -> {
                    LOG.info("onCloseButtonClick executed");
                    centerBlock.getItems().remove(centerBottom);
                }
        );
    }

    /**
     * Helper method to display the help dialog
     */
    private void displayHelp() {
        LOG.info("Alert Help displayed");

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(appIcon);
        alert.setTitle(resources.getString("helpTitle"));
        alert.setHeaderText(resources.getString("howToUse"));
        alert.setContentText(resources.getString("help"));

        alert.showAndWait();
    }

    /**
     * Helper method to read all the necessary information from the properties and
     * load them (e.g. DAO and personal name & user email)
     */
    private void readInfoFromProperties() {
        Properties properties = new Properties();
        Path configFile = Paths.get("config.properties");

        if (Files.exists(configFile)) {
            try (InputStream propFileStream = Files.newInputStream(configFile);) {
                properties.load(propFileStream);
            } catch (IOException e) {
                String errorMessage = "Unexpected error when loading properties";
                LOG.error(errorMessage);
                throw new RuntimeException(errorMessage, e);
            }

            String dbURL = properties.getProperty("dbURL");
            String dbUser = properties.getProperty("dbUser");
            String dbPassword = properties.getProperty("dbPassword");

            ConnectionManager connectionManager = new ConnectionManager(dbURL, dbUser, dbPassword);
            clientDAO = new ClientDAOImpl(connectionManager);

            userPersonalName = properties.getProperty("personalName");
            userEmail = properties.getProperty("userEmail");

            String smtpServerName = properties.getProperty("smtpURL");
            String imapServerName = properties.getProperty("imapURL");
            int smtpServerPort = Integer.parseInt(properties.getProperty("smtpPort"));
            int imapServerPort = Integer.parseInt(properties.getProperty("imapPort"));

            String userPassword = properties.getProperty("userPassword");

            mailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, userEmail, userPassword);
        }

        LOG.info("All the info from properties has loaded");
    }

    /**
     * Helper method to start listening for new incoming emails
     */
    private void listenForEmails() {
        Thread emailListener = new Thread(
                () -> {
                    while (true) {
                        EmailBean[] receivedEmails = mailController.receiveEmails();
                        for (EmailBean emailBean: receivedEmails) {
                            Platform.runLater(() -> {
                                try {
                                    clientDAO.createEmailBean(emailBean, "inbox");
                                    folderListController.addEmailToInbox(emailBean);
                                } catch (DaoException e) {
                                    String errorMessage = "Unexpected error when putting receiving email into inbox";
                                    LOG.error(errorMessage + ": " + e.getMessage());
                                }
                            });
                            LOG.info("Email received!");
                        }

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    }
                }
        );

        emailListener.start();
    }

    /**
     * Sets the call back that will be called when the config button will be clicked
     * @param onConfigClick The callback that will be called when the config button will be clicked
     */
    public void setOnConfigClick(Action onConfigClick) {
        this.onConfigClick = onConfigClick;
    }

    /**
     * Sets the image for the app icon (so that it will be easy to set the icon of alerts)
     * @param appIcon The image for the app icon
     */
    public void setAppIcon(Image appIcon) {
        this.appIcon = appIcon;
    }
}
