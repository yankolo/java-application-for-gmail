package me.yanik;

import me.yanik.gui.GUIManager;

public class Application
{
    public static void main( String[] args )
    {
        javafx.application.Application.launch(GUIManager.class, args);
        System.exit(0);
    }
}
