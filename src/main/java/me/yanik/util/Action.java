package me.yanik.util;

/**
 * Represents an operation that accepts no input and returns no
 * result.
 */
@FunctionalInterface
public interface Action {
    /**
     * Performs this action
     */
    void execute();
}
