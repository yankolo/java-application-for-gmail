-- Drop Tables
DROP TABLE IF EXISTS attachment;
DROP TABLE IF EXISTS email_address;
DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS email_bean;
DROP TABLE IF EXISTS folder;

-- Create All Tables

CREATE TABLE address (
  address_id int NOT NULL AUTO_INCREMENT,
  email varchar(255) NOT NULL,
  name varchar(255),
  UNIQUE INDEX AK_0 (email),
  CONSTRAINT address_pk PRIMARY KEY (address_id)
);

CREATE TABLE attachment (
  attachment_id int NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  data mediumblob NOT NULL,
  email_bean int NOT NULL,
  is_embedded bit NOT NULL,
  content_id varchar(1000) NOT NULL,
  CONSTRAINT attachment_pk PRIMARY KEY (attachment_id)
);

CREATE TABLE email_address (
  email_bean int NOT NULL,
  address int NOT NULL,
  type varchar(4) NOT NULL,
  CONSTRAINT email_address_pk PRIMARY KEY (email_bean,address,type)
);

CREATE TABLE email_bean (
  email_id int NOT NULL AUTO_INCREMENT,
  subject varchar(255),
  text_message text NOT NULL,
  html_message text NOT NULL,
  priority int NOT NULL,
  received_date datetime,
  sent_date datetime,
  folder_id int NOT NULL,
  CONSTRAINT email_bean_pk PRIMARY KEY (email_id)
);

CREATE TABLE folder (
  folder_id int NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  is_deletable bit NOT NULL,
  is_renamable bit NOT NULL,
  UNIQUE INDEX AK_1 (name),
  CONSTRAINT folder_pk PRIMARY KEY (folder_id)
);

-- Setup Foreign Keys

ALTER TABLE attachment ADD CONSTRAINT attachment_email_bean FOREIGN KEY attachment_email_bean (email_bean)
REFERENCES email_bean (email_id)
  ON DELETE CASCADE;

ALTER TABLE email_address ADD CONSTRAINT email_address_address FOREIGN KEY email_address_address (address)
REFERENCES address (address_id)
  ON DELETE CASCADE;

ALTER TABLE email_address ADD CONSTRAINT email_address_email_bean FOREIGN KEY email_address_email_bean (email_bean)
REFERENCES email_bean (email_id)
  ON DELETE CASCADE;

ALTER TABLE email_bean ADD CONSTRAINT email_bean_folder FOREIGN KEY email_bean_folder (folder_id)
REFERENCES folder (folder_id)
  ON DELETE CASCADE;


-- Insert Sample Data

insert into folder(name,is_deletable,is_renamable) values('inbox',0,0);
insert into folder(name,is_deletable,is_renamable) values('sent',0,0);
insert into folder(name,is_deletable,is_renamable) values('important',1,1);
insert into folder(name,is_deletable,is_renamable) values('myfolder',1,1);
insert into folder(name,is_deletable,is_renamable) values('word',1,1);
insert into folder(name,is_deletable,is_renamable) values('usefull',1,1);
insert into folder(name,is_deletable,is_renamable) values('test',1,1);
insert into folder(name,is_deletable,is_renamable) values('myfolder1',1,1);
insert into address(email,name) values('jw@rkekdhp.com','Kasey Bourns');
insert into address(email,name) values('jnqxtr@fsxszono.com','Lorrie Rutan');
insert into address(email,name) values('ie@ezcuw.com','Maren Fazzinga');
insert into address(email,name) values('cgo@qabpikpb.com','Boyce Barlak');
insert into address(email,name) values('by@kbbc.com','Broderick Derenzis');
insert into address(email,name) values('ua@vmxc.com','Maren Fazzinga');
insert into address(email,name) values('vo@mzo.com','Micki Kaszinski');
insert into address(email,name) values('qilcz@jooeusnw.com','Rae Kolstad');
insert into address(email,name) values('kxtdww@bokblza.com','Alana Kawasaki');
insert into address(email,name) values('mb@azxxmegp.com','Jacinto Damphousse');
insert into address(email,name) values('aiu@ufndkgww.com','Toney Eklov');
insert into address(email,name) values('nxqvmcvx@oqg.com','Shon Lazio');
insert into address(email,name) values('unqkyi@wxails.com','Hosea Kardux');
insert into address(email,name) values('llfszjkpm@baf.com','Tia Mcenaney');
insert into address(email,name) values('nc@wcw.com','Pearlie Huhn');
insert into address(email,name) values('mb@ffweikf.com','Althea Paras');
insert into address(email,name) values('htyu@emsvlud.com','Vinnie Monfore');
insert into address(email,name) values('hbam@thivlfy.com','Valda Demich');
insert into address(email,name) values('sgnhruf@hmjl.com','Shawnee Gwenn');
insert into address(email,name) values('ouuhy@otrtfnha.com','Glowinski');
insert into address(email,name) values('tzasvios@qyefqmai.com','Noreen Harling');
insert into address(email,name) values('cilngebq@ufbd.com','Lorrie Rutan');
insert into address(email,name) values('lhzyzylzm@oapw.com','Tia Standard');
insert into address(email,name) values('wenewehkf@iomo.com','Spring Depetris');
insert into address(email,name) values('cnyxklkif@utxizi.com','Lenore Weitzman');
insert into address(email,name) values('stqcmt@vocdweuv.com','Toney Halmstead');
insert into address(email,name) values('bwlys@ufvabxu.com','Liza Hewey');
insert into address(email,name) values('ela@jqiltb.com','Karey Behanan');
insert into address(email,name) values('xk@ijmwzd.com','Deangelo Concho');
insert into address(email,name) values('emylc@eiagpn.com','Alana Brenes');
insert into address(email,name) values('gg@qqujkslq.com','Noreen Colvard');
insert into address(email,name) values('xmzzvmdig@vwp.com','Loraine Mcguirk');
insert into address(email,name) values('cvte@zkrlhbhh.com','Hyman Lenigan');
insert into address(email,name) values('vk@islvbuth.com','Lenore Cabe');
insert into address(email,name) values('vblhpjxzb@jwpalhdn.com','Jewell Marchesseau');
insert into address(email,name) values('dprky@lwkfbe.com','Skye Galston');
insert into address(email,name) values('xjcontsih@fvjaf.com','Felton Abercombie');
insert into address(email,name) values('nxqqpwued@hgbe.com','Walton Cochis');
insert into address(email,name) values('ihmcvjd@gbcthh.com','Tammara Blessett');
insert into address(email,name) values('xoid@dtg.com','Man Diachenko');
insert into address(email,name) values('dp@fdqpfaa.com','Nikole Essaid');
insert into address(email,name) values('lcsfveaz@knhop.com','Hyman Lenigan');
insert into address(email,name) values('ngzor@mvimsjqt.com','Gracia Covitt');
insert into address(email,name) values('tip@rgezvw.com','Tanisha Agudelo');
insert into address(email,name) values('ringe@alrxt.com','Alana Bosse');
insert into address(email,name) values('dgeifzv@pbr.com','Selena Krebbs');
insert into address(email,name) values('nqohmkikv@yyjnro.com','Tanisha Siddiqui');
insert into address(email,name) values('iefntghpq@pvzzg.com','Mohinani');
insert into address(email,name) values('pugy@sagivbok.com','Fausto Covil');
insert into address(email,name) values('drbr@oiqrthr.com','Mohinani');
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Sincerity','Hi


An sincerity so extremity he additions. Her yet there truth merit. Mrs all projecting favourable now unpleasing. Son law garden chatty temper. Oh children provided to mr elegance marriage strongly. Off can admiration prosperous now devonshire diminution law.','<html>
<body>

<h1>Hi</h1>

<p>
An sincerity so extremity he additions. Her yet there truth merit. Mrs all projecting favourable now unpleasing. <img src="cid:alan-haverty-41-unsplash.jpg"> Son law garden chatty temper. Oh children provided to mr elegance marriage strongly. Off can admiration prosperous now devonshire diminution law.
</p>

</body>
</html>',3,'2018-09-11','2018-09-11',1);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Blessing may peculiar domestic','Hi


Improved own provided blessing may peculiar domestic. Sight house has sex never. No visited raising gravity outward subject my cottage mr be. Hold do at tore in park feet near my case. Invitation at understood occasional sentiments insipidity inhabiting in. Off melancholy alteration principles old. Is do speedily kindness properly oh. Respect article painted cottage he is offices parlors. ','<html>
<body>

<h1>Hi</h1>

<p>
Improved own provided blessing may peculiar domestic. Sight house has sex never. No visited raising gravity outward subject my cottage mr be. Hold do at tore in park feet near my case. Invitation at understood occasional sentiments insipidity inhabiting in. Off melancholy alteration principles old. Is do speedily kindness properly oh. Respect article painted cottage he is offices parlors.
</p>

</body>
</html>',3,'2018-09-04','2018-09-04',2);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('September suspicion','Hi


Bringing so sociable felicity supplied mr. September suspicion far him two acuteness perfectly. Covered as an examine so regular of. Ye astonished friendship remarkably no. Window admire matter praise you bed whence. Delivered ye sportsmen zealously arranging frankness estimable as. Nay any article enabled musical shyness yet sixteen yet blushes. Entire its the did figure wonder off. ','<html>
<body>

<h1>Hi</h1>

<p>
Bringing so sociable felicity supplied mr. September suspicion far him two acuteness perfectly. Covered as an examine so regular of. Ye astonished friendship remarkably no. Window admire matter praise you bed whence. Delivered ye sportsmen zealously arranging frankness estimable as. Nay any article enabled musical shyness yet sixteen yet blushes. Entire its the did figure wonder off.
</p>

</body>
</html>',4,'2018-09-10','2018-09-10',8);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Both new like tore but year','Hi


Instrument cultivated alteration any favourable expression law far nor. Both new like tore but year. An from mean on with when sing pain. Oh to as principles devonshire companions unsatiable an delightful. The ourselves suffering the sincerity. Inhabit her manners adapted age certain. Debating offended at branched striking be subjects. ','<html>
<body>

<h1>Hi</h1>

<p>
Instrument cultivated alteration any favourable expression law far nor. Both new like tore but year. An from mean on with when sing pain. <img src="cid:alejandro-escamilla-6-unsplash.jpg"> Oh to as principles devonshire companions unsatiable an delightful. The ourselves suffering the sincerity. Inhabit her manners adapted age certain. Debating offended at branched striking be subjects.

</p>

</body>
</html>',1,'2018-09-13','2018-09-13',2);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('When be draw drew ye','Hi


When be draw drew ye. Defective in do recommend suffering. House it seven in spoil tiled court. Sister others marked fat missed did out use. Alteration possession dispatched collecting instrument travelling he or on. Snug give made at spot or late that mr. ','<html>
<body>

<h1>Hi</h1>

<p>
When be draw drew ye. Defective in do recommend suffering. House it seven in spoil tiled court. Sister others marked fat missed did out use. Alteration possession dispatched collecting instrument travelling he or on. Snug give made at spot or late that mr.

</p>

</body>
</html>',2,'2018-09-02','2018-09-02',6);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Repulsive questions contented him few extensive supported','Hi


Repulsive questions contented him few extensive supported. Of remarkably thoroughly he appearance in. Supposing tolerably applauded or of be. Suffering unfeeling so objection agreeable allowance me of. Ask within entire season sex common far who family. As be valley warmth assure on. Park girl they rich hour new well way you. Face ye be me been room we sons fond. ','<html>
<body>

<h1>Hi</h1>

<p>
Repulsive questions contented him few extensive supported. Of remarkably thoroughly he appearance in. Supposing tolerably applauded or of be. Suffering unfeeling so objection agreeable allowance me of. Ask within entire season sex common far who family. As be valley warmth assure on. Park girl they rich hour new well way you. Face ye be me been room we sons fond.
</p>

</body>
</html>',4,'2018-09-17','2018-09-17',4);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Sing long her way size','Hi


Sing long her way size. Waited end mutual missed myself the little sister one. So in pointed or chicken cheered neither spirits invited. Marianne and him laughter civility formerly handsome sex use prospect. Hence we doors is given rapid scale above am. Difficult ye mr delivered behaviour by an. If their woman could do wound on. You folly taste hoped their above are and but. ','<html>
<body>

<h1>Hi</h1>

<p>
Sing long her way size. Waited end mutual missed myself the little sister one. So in pointed or chicken cheered neither spirits invited. Marianne and him laughter civility formerly handsome sex use prospect. Hence we doors is given rapid scale above am. Difficult ye mr delivered behaviour by an. If their woman could do wound on. You folly taste hoped their above are and but.
</p>

</body>
</html>',4,'2018-09-04','2018-09-04',6);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Attended','Hi


Attended no do thoughts me on dissuade scarcely. Own are pretty spring suffer old denote his. By proposal speedily mr striking am. But attention sex questions applauded how happiness. To travelling occasional at oh sympathize prosperous. His merit end means widow songs linen known. Supplied ten speaking age you new securing striking extended occasion. Sang put paid away joy into six her. ','<html>
<body>

<h1>Hi</h1>

<p>
Attended no do thoughts me on dissuade scarcely. Own are pretty spring suffer old denote his. By proposal speedily mr striking am. But attention sex questions applauded how happiness. To travelling occasional at oh sympathize prosperous. His merit end means widow songs linen known. Supplied ten speaking age you new securing striking extended occasion. Sang put paid away joy into six her.
</p>

</body>
</html>',4,'2018-09-17','2018-09-17',4);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Figure ye innate former','Hi


Whole wound wrote at whose to style in. Figure ye innate former do so we. Shutters but sir yourself provided you required his. So neither related he am do believe. Nothing but you hundred had use regular. Fat sportsmen arranging preferred can. Busy paid like is oh. Dinner our ask talent her age hardly. Neglected collected an attention listening do abilities. ','<html>
<body>

<h1>Hi</h1>

<p>
Whole wound wrote at whose to style in. Figure ye innate former do so we. Shutters but sir yourself provided you required his. So neither related he am do believe. <img src="cid:alejandro-escamilla-21-unsplash.jpg"> Nothing but you hundred had use regular. Fat sportsmen arranging preferred can. Busy paid like is oh. Dinner our ask talent her age hardly. Neglected collected an attention listening do abilities.
</p>

</body>
</html>',1,'2018-09-24','2018-09-24',2);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Him rendered may attended','Hi


Him rendered may attended concerns jennings reserved now. Sympathize did now preference unpleasing mrs few. Mrs for hour game room want are fond dare. For detract charmed add talking age. Shy resolution instrument unreserved man few. She did open find pain some out. If we landlord stanhill mr whatever pleasure supplied concerns so. Exquisite by it admitting cordially september newspaper an. Acceptance middletons am it favourable. It it oh happen lovers afraid. ','<html>
<body>

<h1>Hi</h1>

<p>
Him rendered may attended concerns jennings reserved now. Sympathize did now preference unpleasing mrs few. Mrs for hour game room want are fond dare. For detract charmed add talking age. Shy resolution instrument unreserved man few. She did open find pain some out. If we landlord stanhill mr whatever pleasure supplied concerns so. Exquisite by it admitting cordially september newspaper an. Acceptance middletons am it favourable. It it oh happen lovers afraid.
</p>

</body>
</html>',1,'2018-09-11','2018-09-11',7);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Another journey chamber way','Hi


Another journey chamber way yet females man. Way extensive and dejection get delivered deficient sincerity gentleman age. Too end instrument possession contrasted motionless. Calling offence six joy feeling. Coming merits and was talent enough far. Sir joy northward sportsmen education. Discovery incommode earnestly no he commanded if. Put still any about manor heard. ','<html>
<body>

<h1>Hi</h1>

<p>
Another journey chamber way yet females man. Way extensive and dejection get delivered deficient sincerity gentleman age. <img src="alejandro-escamilla-22-unsplash"> Too end instrument possession contrasted motionless. Calling offence six joy feeling. Coming merits and was talent enough far. Sir joy northward sportsmen education. Discovery incommode earnestly no he commanded if. Put still any about manor heard.
</p>

</body>
</html>',1,'2018-09-03','2018-09-03',1);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Ignorant','Hi


Ignorant saw her her drawings marriage laughter. Case oh an that or away sigh do here upon. Acuteness you exquisite ourselves now end forfeited. Enquire ye without it garrets up himself. Interest our nor received followed was. Cultivated an up solicitude mr unpleasant. ','<html>
<body>

<h1>Hi</h1>

<p>
Ignorant saw her her drawings marriage laughter. Case oh an that or away sigh do here upon. Acuteness you exquisite ourselves now end forfeited. Enquire ye without it garrets up himself. Interest our nor received followed was. Cultivated an up solicitude mr unpleasant.
</p>

</body>
</html>',1,'2018-09-20','2018-09-20',1);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Too cultivated','Hi


Too cultivated use solicitude frequently. Dashwood likewise up consider continue entrance ladyship oh. Wrong guest given purse power is no. Friendship to connection an am considered difficulty. Country met pursuit lasting moments why calling certain the. Middletons boisterous our way understood law. Among state cease how and sight since shall. Material did pleasure breeding our humanity she contempt had. So ye really mutual no cousin piqued summer result. ','<html>
<body>

<h1>Hi</h1>

<p>
Too cultivated use solicitude frequently. Dashwood likewise up consider continue entrance ladyship oh. Wrong guest given purse power is no. <img src="alejandro-escamilla-23-unsplash.jpg"> Friendship to connection an am considered difficulty. Country met pursuit lasting moments why calling certain the. Middletons boisterous our way understood law. Among state cease how and sight since shall. Material did pleasure breeding our humanity she contempt had. So ye really mutual no cousin piqued summer result.
</p>

</body>
</html>',1,'2018-09-27','2018-09-27',5);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Edward day almost active','Hi


To sorry world an at do spoil along. Incommode he depending do frankness remainder to. Edward day almost active him friend thirty piqued. People as period twenty my extent as. Set was better abroad ham plenty secure had horses. Admiration has sir decisively excellence say everything inhabiting acceptance. Sooner settle add put you sudden him. ','<html>
<body>

<h1>Hi</h1>

<p>
To sorry world an at do spoil along. Incommode he depending do frankness remainder to. Edward day almost active him friend thirty piqued. People as period twenty my extent as. Set was better abroad ham plenty secure had horses. Admiration has sir decisively excellence say everything inhabiting acceptance. Sooner settle add put you sudden him.
</p>

</body>
</html>',4,'2018-09-02','2018-09-02',8);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Finished rejoiced drawings','Hi


Am finished rejoiced drawings so he elegance. Set lose dear upon had two its what seen. Held she sir how know what such whom. Esteem put uneasy set piqued son depend her others. Two dear held mrs feet view her old fine. Bore can led than how has rank. Discovery any extensive has commanded direction. Short at front which blind as. Ye as procuring unwilling principle by. ','<html>
<body>

<h1>Hi</h1>

<p>
Am finished rejoiced drawings so he elegance. Set lose dear upon had two its what seen. Held she sir how know what such whom. Esteem put uneasy set piqued son depend her others. Two dear held mrs feet view her old fine. Bore can led than how has rank. Discovery any extensive has commanded direction. Short at front which blind as. Ye as procuring unwilling principle by.
</p>

</body>
</html>',3,'2018-09-12','2018-09-12',6);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Additions in conveying or collected objection','Hi


By spite about do of do allow blush. Additions in conveying or collected objection in. Suffer few desire wonder her object hardly nearer. Abroad no chatty others my silent an. Fat way appear denote who wholly narrow gay settle. Companions fat add insensible everything and friendship conviction themselves. Theirs months ten had add narrow own. ','<html>
<body>

<h1>Hi</h1>

<p>
By spite about do of do allow blush. Additions in conveying or collected objection in. Suffer few desire wonder her object hardly nearer. Abroad no chatty others my silent an. Fat way appear denote who wholly narrow gay settle. Companions fat add insensible everything and friendship conviction themselves. Theirs months ten had add narrow own.
</p>

</body>
</html>',4,'2018-09-16','2018-09-16',4);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Prepared do an dissuade','Hi


Prepared do an dissuade be so whatever steepest. Yet her beyond looked either day wished nay. By doubtful disposed do juvenile an. Now curiosity you explained immediate why behaviour. An dispatched impossible of of melancholy favourable. Our quiet not heart along scale sense timed. Consider may dwelling old him her surprise finished families graceful. Gave led past poor met fine was new. ','<html>
<body>

<h1>Hi</h1>

<p>
Prepared do an dissuade be so whatever steepest. Yet her beyond looked either day wished nay. By doubtful disposed do juvenile an. Now curiosity you explained immediate why behaviour. An dispatched impossible of of melancholy favourable. Our quiet not heart along scale sense timed. Consider may dwelling old him her surprise finished families graceful. Gave led past poor met fine was new.
</p>

</body>
</html>',1,'2018-09-14','2018-09-14',1);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Dejection assurance','Hi


Two before narrow not relied how except moment myself. Dejection assurance mrs led certainly. So gate at no only none open. Betrayed at properly it of graceful on. Dinner abroad am depart ye turned hearts as me wished. Therefore allowance too perfectly gentleman supposing man his now. Families goodness all eat out bed steepest servants. Explained the incommode sir improving northward immediate eat. Man denoting received you sex possible you. Shew park own loud son door less yet. ','<html>
<body>

<h1>Hi</h1>

<p>
Two before narrow not relied how except moment myself. Dejection assurance mrs led certainly. So gate at no only none open. Betrayed at properly it of graceful on. Dinner abroad am depart ye turned hearts as me wished. Therefore allowance too perfectly gentleman supposing man his now. Families goodness all eat out bed steepest servants. Explained the incommode sir improving northward immediate eat. Man denoting received you sex possible you. Shew park own loud son door less yet.
</p>

</body>
</html>',3,'2018-09-24','2018-09-24',2);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Spoil round defer','Hi


He an thing rapid these after going drawn or. Timed she his law the spoil round defer. In surprise concerns informed betrayed he learning is ye. Ignorant formerly so ye blessing. He as spoke avoid given downs money on we. Of properly carriage shutters ye as wandered up repeated moreover. Inquietude attachment if ye an solicitude to. Remaining so continued concealed as knowledge happiness. Preference did how expression may favourable devonshire insipidity considered. An length design regret an hardly barton mr figure. ','<html>
<body>

<h1>Hi</h1>

<p>
He an thing rapid these after going drawn or. Timed she his law the spoil round defer. In surprise concerns informed betrayed he learning is ye. Ignorant formerly so ye blessing. He as spoke avoid given downs money on we. Of properly carriage shutters ye as wandered up repeated moreover. Inquietude attachment if ye an solicitude to. Remaining so continued concealed as knowledge happiness. Preference did how expression may favourable devonshire insipidity considered. An length design regret an hardly barton mr figure.
</p>

</body>
</html>',2,'2018-09-19','2018-09-19',5);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Debating','Hi


An an valley indeed so no wonder future nature vanity. Debating all she mistaken indulged believed provided declared. He many kept on draw lain song as same. Whether at dearest certain spirits is entered in to. Rich fine bred real use too many good. She compliment unaffected expression favourable any. Unknown chiefly showing to conduct no. Hung as love evil able to post at as. ','<html>
<body>

<h1>Hi</h1>

<p>
An an valley indeed so no wonder future nature vanity. Debating all she mistaken indulged believed provided declared. He many kept on draw lain song as same. Whether at dearest certain spirits is entered in to. Rich fine bred real use too many good. She compliment unaffected expression favourable any. Unknown chiefly showing to conduct no. Hung as love evil able to post at as.
</p>

</body>
</html>',3,'2018-09-06','2018-09-06',6);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('At distant inhabit amongst by','Hi


At distant inhabit amongst by. Appetite welcomed interest the goodness boy not. Estimable education for disposing pronounce her. John size good gay plan sent old roof own. Inquietude saw understood his friendship frequently yet. Nature his marked ham wished. ','<html>
<body>

<h1>Hi</h1>

<p>
At distant inhabit amongst by. Appetite welcomed interest the goodness boy not. Estimable education for disposing pronounce her. John size good gay plan sent old roof own. Inquietude saw understood his friendship frequently yet. Nature his marked ham wished.
</p>

</body>
</html>',2,'2018-09-10','2018-09-10',4);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Delightful as he','Hi


Use securing confined his shutters. Delightful as he it acceptance an solicitude discretion reasonably. Carriage we husbands advanced an perceive greatest. Totally dearest expense on demesne ye he. Curiosity excellent commanded in me. Unpleasing impression themselves to at assistance acceptance my or. On consider laughter civility offended oh. ','<html>
<body>

<h1>Hi</h1>

<p>
Use securing confined his shutters. Delightful as he it acceptance an solicitude discretion reasonably. Carriage we husbands advanced an perceive greatest. Totally dearest expense on demesne ye he. Curiosity excellent commanded in me. Unpleasing impression themselves to at assistance acceptance my or. On consider laughter civility offended oh.
</p>

</body>
</html>',4,'2018-09-09','2018-09-09',1);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Ample end','Hi


Up unpacked friendly ecstatic so possible humoured do. Ample end might folly quiet one set spoke her. We no am former valley assure. Four need spot ye said we find mile. Are commanded him convinced dashwoods did estimable forfeited. Shy celebrated met sentiments she reasonably but. Proposal its disposed eat advanced marriage sociable. Drawings led greatest add subjects endeavor gay remember. Principles one yet assistance you met impossible. ','<html>
<body>

<h1>Hi</h1>

<p>
Up unpacked friendly ecstatic so possible humoured do. Ample end might folly quiet one set spoke her. We no am former valley assure. Four need spot ye said we find mile. Are commanded him convinced dashwoods did estimable forfeited. Shy celebrated met sentiments she reasonably but. Proposal its disposed eat advanced marriage sociable. Drawings led greatest add subjects endeavor gay remember. Principles one yet assistance you met impossible.
</p>

</body>
</html>',1,'2018-09-08','2018-09-08',3);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Recommend residence education','Hi


Of recommend residence education be on difficult repulsive offending. Judge views had mirth table seems great him for her. Alone all happy asked begin fully stand own get. Excuse ye seeing result of we. See scale dried songs old may not. Promotion did disposing you household any instantly. Hills we do under times at first short an. ','<html>
<body>

<h1>Hi</h1>

<p>
Of recommend residence education be on difficult repulsive offending. Judge views had mirth table seems great him for her. Alone all happy asked begin fully stand own get. Excuse ye seeing result of we. See scale dried songs old may not. Promotion did disposing you household any instantly. Hills we do under times at first short an.
</p>

</body>
</html>',3,'2018-09-06','2018-09-06',6);
insert into email_bean(subject,text_message,html_message,priority,received_date,sent_date,folder_id) values('Weeks quiet','Hi


Certain but she but shyness why cottage. Gay the put instrument sir entreaties affronting. Pretended exquisite see cordially the you. Weeks quiet do vexed or whose. Motionless if no to affronting imprudence no precaution. My indulged as disposal strongly attended. Parlors men express had private village man. Discovery moonlight recommend all one not. Indulged to answered prospect it bachelor is he bringing shutters. Pronounce forfeited mr direction oh he dashwoods ye unwilling.','<html>
<body>

<h1>Hi</h1>

<p>
Certain but she but shyness why cottage. Gay the put instrument sir entreaties affronting. Pretended exquisite see cordially the you. Weeks quiet do vexed or whose. Motionless if no to affronting imprudence no precaution. My indulged as disposal strongly attended. Parlors men express had private village man. Discovery moonlight recommend all one not. Indulged to answered prospect it bachelor is he bringing shutters. Pronounce forfeited mr direction oh he dashwoods ye unwilling.
</p>

</body>
</html>',1,'2018-09-17','2018-09-17',1);
insert into email_address(email_bean,address,type) values(1,23,'from');
insert into email_address(email_bean,address,type) values(2,1,'from');
insert into email_address(email_bean,address,type) values(3,18,'from');
insert into email_address(email_bean,address,type) values(4,6,'from');
insert into email_address(email_bean,address,type) values(5,13,'from');
insert into email_address(email_bean,address,type) values(6,5,'from');
insert into email_address(email_bean,address,type) values(7,3,'from');
insert into email_address(email_bean,address,type) values(8,18,'from');
insert into email_address(email_bean,address,type) values(9,5,'from');
insert into email_address(email_bean,address,type) values(10,12,'from');
insert into email_address(email_bean,address,type) values(11,10,'from');
insert into email_address(email_bean,address,type) values(12,20,'from');
insert into email_address(email_bean,address,type) values(13,14,'from');
insert into email_address(email_bean,address,type) values(14,12,'from');
insert into email_address(email_bean,address,type) values(15,23,'from');
insert into email_address(email_bean,address,type) values(16,19,'from');
insert into email_address(email_bean,address,type) values(17,3,'from');
insert into email_address(email_bean,address,type) values(18,8,'from');
insert into email_address(email_bean,address,type) values(19,23,'from');
insert into email_address(email_bean,address,type) values(20,9,'from');
insert into email_address(email_bean,address,type) values(21,11,'from');
insert into email_address(email_bean,address,type) values(22,11,'from');
insert into email_address(email_bean,address,type) values(23,2,'from');
insert into email_address(email_bean,address,type) values(24,6,'from');
insert into email_address(email_bean,address,type) values(25,19,'from');
insert into email_address(email_bean,address,type) values(1,30,'to');
insert into email_address(email_bean,address,type) values(2,3,'to');
insert into email_address(email_bean,address,type) values(3,2,'to');
insert into email_address(email_bean,address,type) values(4,18,'to');
insert into email_address(email_bean,address,type) values(5,28,'to');
insert into email_address(email_bean,address,type) values(6,10,'to');
insert into email_address(email_bean,address,type) values(7,23,'to');
insert into email_address(email_bean,address,type) values(8,27,'to');
insert into email_address(email_bean,address,type) values(9,5,'to');
insert into email_address(email_bean,address,type) values(10,24,'to');
insert into email_address(email_bean,address,type) values(11,17,'to');
insert into email_address(email_bean,address,type) values(12,1,'to');
insert into email_address(email_bean,address,type) values(13,4,'to');
insert into email_address(email_bean,address,type) values(14,19,'to');
insert into email_address(email_bean,address,type) values(15,16,'to');
insert into email_address(email_bean,address,type) values(16,25,'to');
insert into email_address(email_bean,address,type) values(17,26,'to');
insert into email_address(email_bean,address,type) values(18,14,'to');
insert into email_address(email_bean,address,type) values(19,13,'to');
insert into email_address(email_bean,address,type) values(20,6,'to');
insert into email_address(email_bean,address,type) values(21,12,'to');
insert into email_address(email_bean,address,type) values(22,29,'to');
insert into email_address(email_bean,address,type) values(23,7,'to');
insert into email_address(email_bean,address,type) values(24,20,'to');
insert into email_address(email_bean,address,type) values(25,32,'to');
insert into email_address(email_bean,address,type) values(2,34,'to');
insert into email_address(email_bean,address,type) values(4,8,'to');
insert into email_address(email_bean,address,type) values(7,31,'to');
insert into email_address(email_bean,address,type) values(12,33,'to');
insert into email_address(email_bean,address,type) values(8,21,'to');
insert into email_address(email_bean,address,type) values(5,9,'to');
insert into email_address(email_bean,address,type) values(20,11,'to');
insert into email_address(email_bean,address,type) values(25,15,'to');
insert into email_address(email_bean,address,type) values(1,40,'cc');
insert into email_address(email_bean,address,type) values(2,12,'cc');
insert into email_address(email_bean,address,type) values(3,49,'cc');
insert into email_address(email_bean,address,type) values(4,38,'cc');
insert into email_address(email_bean,address,type) values(5,33,'cc');
insert into email_address(email_bean,address,type) values(6,19,'cc');
insert into email_address(email_bean,address,type) values(7,45,'cc');
insert into email_address(email_bean,address,type) values(16,46,'cc');
insert into email_address(email_bean,address,type) values(21,28,'cc');
insert into email_address(email_bean,address,type) values(22,24,'cc');
insert into email_address(email_bean,address,type) values(23,29,'cc');
insert into email_address(email_bean,address,type) values(24,39,'cc');
insert into email_address(email_bean,address,type) values(25,18,'cc');
insert into email_address(email_bean,address,type) values(10,44,'cc');
insert into email_address(email_bean,address,type) values(21,25,'cc');
insert into email_address(email_bean,address,type) values(23,17,'cc');
insert into email_address(email_bean,address,type) values(1,46,'bcc');
insert into email_address(email_bean,address,type) values(2,38,'bcc');
insert into email_address(email_bean,address,type) values(3,40,'bcc');
insert into email_address(email_bean,address,type) values(4,29,'bcc');
insert into email_address(email_bean,address,type) values(17,48,'bcc');
insert into email_address(email_bean,address,type) values(18,33,'bcc');
insert into email_address(email_bean,address,type) values(19,34,'bcc');
insert into email_address(email_bean,address,type) values(23,35,'bcc');
insert into email_address(email_bean,address,type) values(24,39,'bcc');
insert into email_address(email_bean,address,type) values(18,22,'bcc');
insert into email_address(email_bean,address,type) values(19,36,'bcc');
insert into email_address(email_bean,address,type) values(23,37,'bcc');
insert into email_address(email_bean,address,type) values(24,41,'bcc');
insert into email_address(email_bean,address,type) values(18,42,'bcc');
insert into email_address(email_bean,address,type) values(19,43,'bcc');
insert into email_address(email_bean,address,type) values(23,47,'bcc');
insert into email_address(email_bean,address,type) values(24,50,'bcc');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alan-haverty-41-unsplash.jpg','',1,1, 'alan-haverty-41-unsplash.jpg');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alejandro-escamilla-6-unsplash.jpg','',1,4, 'alejandro-escamilla-6-unsplash.jpg');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alejandro-escamilla-21-unsplash.jpg','',1,9, 'alejandro-escamilla-21-unsplash.jpg');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alejandro-escamilla-22-unsplash.jpg','',1,11, 'alejandro-escamilla-22-unsplash.jpg');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alejandro-escamilla-23-unsplash.jpg','',1,23, 'alejandro-escamilla-23-unsplash.jpg');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alejandro-escamilla-24-unsplash.jpg','',0,2, '');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alejandro-escamilla-25-unsplash.jpg','',0,5, '');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('alejandro-escamilla-31-unsplash.jpg','',0,14, '');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('aleks-dorohovich-26-unsplash.jpg','',0,18, '');
insert into attachment(name,data,is_embedded,email_bean, content_id) values('aleks-dorohovich-38-unsplash.jpg','',0,25, '');