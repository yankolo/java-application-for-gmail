-- This script needs to be run only once

DROP DATABASE IF EXISTS TEST_DB;
CREATE DATABASE TEST_DB;

USE TEST_DB;

DROP USER IF EXISTS jag@localhost;
CREATE USER jag@'localhost' IDENTIFIED BY 'jag-password';
GRANT ALL ON TEST_DB.* TO jag@'localhost';

FLUSH PRIVILEGES;

-- Sets the max packet size to 1GB to support transmission of MEDIUMBLOBS
-- Might want to consider to change my.ini -> max_allowed_packet=1G
-- Because it resets when the server restarts
SET GLOBAL max_allowed_packet=1073741824;