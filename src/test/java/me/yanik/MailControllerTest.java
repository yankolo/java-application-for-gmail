package me.yanik;

import me.yanik.business.*;
import me.yanik.data.*;
import org.junit.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Unit tests MailController
 */
public class MailControllerTest
{
    private String smtpServerName = "smtp.gmail.com";
    private String imapServerName = "imap.gmail.com";

    private int smtpServerPort = 465;
    private int imapServerPort = 993;

    // Change the following values to use other emails for unit testing
    private String senderEmail = "send.1640919@gmail.com";
    private String senderPassword = "java.dev";

    private String recipientEmail = "receive.1640919@gmail.com";
    private String recipientPassword = "java.dev";

    private String otherEmail1 = "other.1640919@gmail.com";
    private String otherPassword1 = "java.dev";

    private String otherEmail2 = "other2.1640919@gmail.com";
    private String otherPassword2 = "java.dev";

    @Test
    public void sendAndReceiveEmail_MinimallyValidEmail_EmailSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        // Trim plain text message from received email to remove line breaks as
        // Google adds line breaks for every 76 characters (even if it less than that) probably because of
        // Section 2.1.1 of RFC 2822 "Each line of characters ... SHOULD be no more than 78 characters"
        receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim());

        Assert.assertEquals(email, receivedEmail);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithSubject_EmailSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setSubject("Unit Test Subject");

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim());

        Assert.assertEquals(email, receivedEmail);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithPlainText_EmailSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim());

        Assert.assertEquals(email, receivedEmail);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithHtmlMessage_EmailSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setHtmlMessage("<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>This is a test</h1></body></html>");

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        receivedEmail.setHtmlMessage(receivedEmail.getHtmlMessage().trim());

        Assert.assertEquals(email, receivedEmail);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithMultipleRecipients_AllEmailsSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController recipient2MailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient 1 Address", recipientEmail),
                                    new Address("Recipient 2 Address", otherEmail1)});
        email.setTextMessage("Test");

        EmailBean[] expectedReceivedEmails = {email, email};

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] receivedEmailsRecipient1 = recipientMailController.receiveEmails();
        EmailBean[] receivedEmailsRecipient2 = recipient2MailController.receiveEmails();
        EmailBean[] allReceivedEmails = Stream.of(receivedEmailsRecipient1, receivedEmailsRecipient2)
                .flatMap(emailArray -> Arrays.stream(emailArray))
                .toArray(EmailBean[]::new);

        Arrays.stream(allReceivedEmails).forEach(receivedEmail -> receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim()));

        Assert.assertArrayEquals(expectedReceivedEmails, allReceivedEmails);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithCc_EmailSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController ccMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setCc(new Address[] {new Address("CC Address", otherEmail1)});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        recipientMailController.receiveEmails();

        EmailBean[] allReceivedEmails = ccMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim());

        Assert.assertEquals(email, receivedEmail);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithMultipleCc_AllEmailsSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController ccMailController1 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);
        MailController ccMailController2 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail2, otherPassword2);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setCc(new Address[] {new Address("CC 1 Address", otherEmail1),
                                    new Address("CC 2 Address", otherEmail2)});
        email.setTextMessage("Test");

        EmailBean[] expectedReceivedEmails = {email, email};

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        recipientMailController.receiveEmails();
        EmailBean[] receivedEmailsCc1 = ccMailController1.receiveEmails();
        EmailBean[] receivedEmailsCc2 = ccMailController2.receiveEmails();
        EmailBean[] allReceivedEmails = Stream.of(receivedEmailsCc1, receivedEmailsCc2)
                .flatMap(emailArray -> Arrays.stream(emailArray))
                .toArray(EmailBean[]::new);

        Arrays.stream(allReceivedEmails).forEach(receivedEmail -> receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim()));

        Assert.assertArrayEquals(expectedReceivedEmails, allReceivedEmails);
    }

    @Test
    public void sendAndReceiveEmail_MultipleValidEmailsWithMultipleCc_AllEmailsSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController ccMailController1 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);
        MailController ccMailController2 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail2, otherPassword2);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setCc(new Address[] {new Address("CC 1 Address", otherEmail1),
                new Address("CC 2 Address", otherEmail2)});
        email.setTextMessage("Test");

        EmailBean[] expectedReceivedEmails = {email, email, email, email};

        senderMailController.sendEmail(email);
        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        recipientMailController.receiveEmails();
        EmailBean[] receivedEmailsCc1 = ccMailController1.receiveEmails();
        EmailBean[] receivedEmailsCc2 = ccMailController2.receiveEmails();
        EmailBean[] allReceivedEmails = Stream.of(receivedEmailsCc1, receivedEmailsCc2)
                .flatMap(emailArray -> Arrays.stream(emailArray))
                .toArray(EmailBean[]::new);

        Arrays.stream(allReceivedEmails).forEach(receivedEmail -> receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim()));

        Assert.assertArrayEquals(expectedReceivedEmails, allReceivedEmails);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithBcc_EmailSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController bccMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setBcc(new Address[] {new Address("BCC Address", otherEmail1)});
        email.setTextMessage("Test");

        EmailBean expectedEmail = new EmailBean();
        expectedEmail.setFrom(email.getFrom());
        expectedEmail.setTo(email.getTo());
        expectedEmail.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        recipientMailController.receiveEmails();

        EmailBean[] allReceivedEmails = bccMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim());

        Assert.assertEquals(expectedEmail, receivedEmail);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithMultipleBcc_AllEmailsSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController bccMailController1 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);
        MailController bccMailController2 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail2, otherPassword2);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setBcc(new Address[] {new Address("BCC 1 Address", otherEmail1),
                new Address("BCC 2 Address", otherEmail2)});
        email.setTextMessage("Test");

        EmailBean expectedEmail = new EmailBean();
        expectedEmail.setFrom(email.getFrom());
        expectedEmail.setTo(email.getTo());
        expectedEmail.setTextMessage("Test");

        EmailBean[] expectedReceivedEmails = {expectedEmail, expectedEmail};

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        recipientMailController.receiveEmails();
        EmailBean[] receivedEmailsBcc1 = bccMailController1.receiveEmails();
        EmailBean[] receivedEmailsBcc2 = bccMailController2.receiveEmails();
        EmailBean[] allReceivedEmails = Stream.of(receivedEmailsBcc1, receivedEmailsBcc2)
                .flatMap(emailArray -> Arrays.stream(emailArray))
                .toArray(EmailBean[]::new);

        Arrays.stream(allReceivedEmails).forEach(receivedEmail -> receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim()));

        Assert.assertArrayEquals(expectedReceivedEmails, allReceivedEmails);
    }

    @Test
    public void sendAndReceiveEmail_MultipleValidEmailsWithMultipleBcc_AllEmailsSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController bccMailController1 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);
        MailController bccMailController2 = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail2, otherPassword2);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setBcc(new Address[] {new Address("BCC 1 Address", otherEmail1),
                new Address("BCC 2 Address", otherEmail2)});
        email.setTextMessage("Test");

        EmailBean expectedEmail = new EmailBean();
        expectedEmail.setFrom(email.getFrom());
        expectedEmail.setTo(email.getTo());
        expectedEmail.setTextMessage("Test");

        EmailBean[] expectedReceivedEmails = {expectedEmail, expectedEmail, expectedEmail, expectedEmail};

        senderMailController.sendEmail(email);
        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        recipientMailController.receiveEmails();
        EmailBean[] receivedEmailsBcc1 = bccMailController1.receiveEmails();
        EmailBean[] receivedEmailsBcc2 = bccMailController2.receiveEmails();
        EmailBean[] allReceivedEmails = Stream.of(receivedEmailsBcc1, receivedEmailsBcc2)
                .flatMap(emailArray -> Arrays.stream(emailArray))
                .toArray(EmailBean[]::new);

        Arrays.stream(allReceivedEmails).forEach(receivedEmail -> receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim()));

        Assert.assertArrayEquals(expectedReceivedEmails, allReceivedEmails);
    }

    @Test
    public void sendAndReceiveEmail_MultipleValidEmailsWithOneRecipient_AllEmailsSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");

        EmailBean[] expectedReceivedEmails = {email, email};

        senderMailController.sendEmail(email);
        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();

        Arrays.stream(allReceivedEmails).forEach(receivedEmail -> receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim()));

        Assert.assertArrayEquals(expectedReceivedEmails, allReceivedEmails);
    }

    @Ignore("Inconsistent results - Need to show to Ken")
    @Test
    public void sendAndReceiveEmail_MultipleValidEmailsWithMultipleRecipients_AllEmailsSentAndReceived() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);
        MailController recipient2MailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, otherEmail1, otherPassword1);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient 1 Address", recipientEmail),
                new Address("Recipient 2 Address", otherEmail1)});
        email.setTextMessage("Test");

        EmailBean[] expectedReceivedEmails = {email, email, email, email};

        senderMailController.sendEmail(email);
        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] receivedEmailsRecipient1 = recipientMailController.receiveEmails();
        EmailBean[] receivedEmailsRecipient2 = recipient2MailController.receiveEmails();
        EmailBean[] allReceivedEmails = Stream.of(receivedEmailsRecipient1, receivedEmailsRecipient2)
                .flatMap(emailArray -> Arrays.stream(emailArray))
                .toArray(EmailBean[]::new);

        Arrays.stream(allReceivedEmails).forEach(receivedEmail -> receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim()));

        Assert.assertArrayEquals(expectedReceivedEmails, allReceivedEmails);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithNormalAttachmentAndPlainText_EmailSentAndReceived() throws InvalidMailException, InterruptedException, URISyntaxException, IOException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        String pictureName = "WindsorKen180.jpg";
        URI pictureURI = getClass().getClassLoader().getResource("WindsorKen180.jpg").toURI();
        Path picture2Path = Paths.get(pictureURI);
        byte[] picture = Files.readAllBytes(picture2Path);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");
        email.setAttachments(new Attachment[] {new Attachment(pictureName, picture, false)});

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim());

        Assert.assertEquals(email, receivedEmail);
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithEmbeddedAttachmentAndHtmlText_EmailSentAndReceived() throws InvalidMailException, InterruptedException, URISyntaxException, IOException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        String pictureName = "FreeFall.jpg";
        URI pictureURI = getClass().getClassLoader().getResource("FreeFall.jpg").toURI();
        Path picture2Path = Paths.get(pictureURI);
        byte[] picture = Files.readAllBytes(picture2Path);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setHtmlMessage("<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in "
                + "this email.</h1><img src='cid:FreeFall.jpg'>"
                + "<h2>I'm flying!</h2></body></html>");
        email.setAttachments(new Attachment[] {new Attachment(pictureName, picture, true)});

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        boolean areEqual = email.equals(receivedEmail);
        Assert.assertEquals(email, receivedEmail);
    }

    @Test(expected=InvalidMailException.class)
    public void sendAndReceiveEmail_InvalidEmailWithoutRecipient_InvalidMailException() throws InvalidMailException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Assert.fail();
    }

    @Test(expected=InvalidMailException.class)
    public void sendAndReceiveEmail_InvalidEmailWithoutSender_InvalidMailException() throws InvalidMailException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Assert.fail();
    }

    @Test(expected=InvalidMailException.class)
    public void sendAndReceiveEmail_InvalidEmailSenderDoesNotMatchController_InvalidMailException() throws InvalidMailException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", otherEmail1));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Assert.fail();
    }

    @Test(expected=InvalidMailException.class)
    public void sendAndReceiveEmail_EmailWithInvalidRecipient_InvalidMailException() throws InvalidMailException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", "#($)&#$#$&@gmail.com")});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Assert.fail();
    }

    @Test(expected=InvalidMailException.class)
    public void sendAndReceiveEmail_EmailWithInvalidCc_InvalidMailException() throws InvalidMailException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setCc(new Address[] {new Address("CC Address", "#($)&#$#$&@gmail.com")});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Assert.fail();
    }

    @Test(expected=InvalidMailException.class)
    public void sendAndReceiveEmail_EmailWithInvalidBcc_InvalidMailException() throws InvalidMailException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setBcc(new Address[] {new Address("BCC Address", "#($)&#$#$&@gmail.com")});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Assert.fail();
    }

    @Test
    public void sendAndReceiveEmail_ValidEmail_ReceivedDateSet() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        Assert.assertNotNull(receivedEmail.getDateReceived());
    }

    @Test
    public void sendAndReceiveEmail_ValidEmailWithPriority_EmailSentAndReceivedIsSame() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);
        MailController recipientMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, recipientEmail, recipientPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");
        email.setPriority(5);

        senderMailController.sendEmail(email);

        Thread.sleep(2000);

        EmailBean[] allReceivedEmails = recipientMailController.receiveEmails();
        EmailBean receivedEmail = allReceivedEmails[0];

        receivedEmail.setTextMessage(receivedEmail.getTextMessage().trim());

        Assert.assertEquals(email, receivedEmail);
    }

    @Test(expected=InvalidMailException.class)
    public void sendAndReceiveEmail_EmailWithInvalidPriority_InvalidMailException() throws InvalidMailException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");
        email.setPriority(6);

        senderMailController.sendEmail(email);

        Assert.fail();
    }

    @Test
    public void sendEmail_ValidEmail_SendDateSet() throws InvalidMailException, InterruptedException {
        MailController senderMailController = new MailController(smtpServerName, smtpServerPort, imapServerName, imapServerPort, senderEmail, senderPassword);

        EmailBean email = new EmailBean();
        email.setFrom(new Address("Sending Address", senderEmail));
        email.setTo(new Address[] {new Address("Recipient Address", recipientEmail)});
        email.setTextMessage("Test");

        senderMailController.sendEmail(email);

        Assert.assertNotNull(email.getDateSent());
    }
}
