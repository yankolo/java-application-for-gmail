package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.*;

@RunWith(Enclosed.class)
public class EmailBeanDAOTest {
    private final static Logger LOG = LoggerFactory.getLogger(EmailBeanDAOTest.class);

    private static DAOTestHelper helper;
    private static EmailBeanDAO dao;
    private static FolderDAO folderDAO;

    @BeforeClass
    public static void beforeClass() {
        LOG.info("@BeforeClass");
        helper = new DAOTestHelper();
        AddressDAO addressDAO = new AddressDAOImpl(helper.getConnectionManager());
        AttachmentDAO attachmentDAO = new AttachmentDAOImpl(helper.getConnectionManager());
        dao = new EmailBeanDAOImpl(helper.getConnectionManager(), addressDAO, attachmentDAO);
        folderDAO = new FolderDAOImpl(helper.getConnectionManager(), dao);
    }

    @AfterClass
    public static void afterClass() {
        LOG.info("@AfterClass");
        helper.seedDatabase();
    }

    /* ----------- Tests Cases that run only once ------------ */

    public static class OneTimeTests {
        @Before
        public void before() {
            LOG.info("@before");
            helper.seedDatabase();
        }

        /* -- deleteEmailBean() tests -- */
        @Test
        public void deleteEmail_EmailExistsInDB_EmailNoLongerInDB() throws DaoException {
            EmailBean emailBean = dao.findEmailBeanById(1);
            dao.deleteEmailBean(emailBean);

            Assert.assertNull(dao.findEmailBeanById(1));
        }

        @Test
        public void deleteEmail_EmailExistsInDB_IdUnset() throws DaoException {
            EmailBean emailBean = dao.findEmailBeanById(1);
            dao.deleteEmailBean(emailBean);

            Assert.assertEquals(-1, emailBean.getId());
        }

        @Test(expected=DaoException.class)
        public void deleteEmail_EmailNotInDB_DaoException() throws DaoException {
            EmailBean emailBean = helper.getMockEmailBean();
            dao.deleteEmailBean(emailBean);

            Assert.fail();
        }


        /* -- findEmailBeanById() tests -- */

        @Test
        public void findEmailBeanById_EmailBeanInDB_RightEmailBeanRetrieved() throws DaoException {
            EmailBean emailBean = dao.findEmailBeanById(1);

            EmailBean expectedEmailBean = new EmailBean();
            expectedEmailBean.setSubject("Sincerity");
            expectedEmailBean.setTextMessage("HiAn sincerity so extremity he additions. " +
                    "Her yet there truth merit. Mrs all projecting favourable now unpleasing." +
                    " Son law garden chatty temper. Oh children provided to mr elegance marriage" +
                    " strongly. Off can admiration prosperous now devonshire diminution law.");
            expectedEmailBean.setHtmlMessage("<html><body><h1>Hi</h1><p>An sincerity so extremity he" +
                    " additions. Her yet there truth merit. Mrs all projecting favourable now unpleasing." +
                    " <img src=\"cid:alan-haverty-41-unsplash.jpg\"> Son law garden chatty temper." +
                    " Oh children provided to mr elegance marriage strongly. Off can admiration " +
                    "prosperous now devonshire diminution law.</p></body></html>");
            expectedEmailBean.setPriority(3);
            expectedEmailBean.setDateReceived(LocalDateTime.of(2018, 9, 11,00, 00,00));
            expectedEmailBean.setDateSent(LocalDateTime.of(2018, 9, 11,00, 00,00));
            expectedEmailBean.setFrom(new Address("Tia Standard", "lhzyzylzm@oapw.com"));
            expectedEmailBean.setTo(new Address[] {new Address("Alana Brenes", "emylc@eiagpn.com")});
            expectedEmailBean.setCc(new Address[] {new Address("Man Diachenko", "xoid@dtg.com")});
            expectedEmailBean.setBcc(new Address[] {new Address("Selena Krebbs", "dgeifzv@pbr.com")});
            expectedEmailBean.setAttachments(new Attachment[] {
                    helper.loadAttachment("sql/attachment_blobs/alan-haverty-41-unsplash.jpg",
                            "alan-haverty-41-unsplash.jpg", true)});

            Assert.assertEquals(expectedEmailBean, emailBean);
        }

        @Test
        public void findEmailBeanById_EmailBeanInDB_IdSet() throws DaoException {
            EmailBean emailBean = dao.findEmailBeanById(1);
            Assert.assertEquals(1, emailBean.getId());
        }

        @Test
        public void findEmailBeanById_EmailBeanNotInDB_Null() throws DaoException {
            EmailBean emailBean = dao.findEmailBeanById(1000);
            Assert.assertNull(emailBean);
        }

        /* -- isEmailBeanExists() tests -- */

        @Test
        public void isEmailBeanExists_EmailBeanInDB_True() throws DaoException {
            Assert.assertTrue(dao.isEmailBeanExists(1));
        }
        @Test
        public void isEmailBeanExists_EmailBeanNotInDB_False() throws DaoException {
            Assert.assertFalse(dao.isEmailBeanExists(1000));
        }

        /* -- changeEmailBeanFolder() tests -- */

        @Test
        public void changeEmailBeanFolder_EmailBeanExists_FolderIdChnaged() throws DaoException  {
            EmailBean emailBean = dao.findEmailBeanById(1);
            dao.changeEmailBeanFolder(emailBean.getId(), 2);

            Folder folder = folderDAO.findFolderById(2);

            Assert.assertTrue(Arrays.asList(folder.getEmailBeans()).contains(emailBean));
        }

        @Test(expected=DaoException.class)
        public void changeEmailBeanFolder_EmailBeanNotInDb_DaoException() throws DaoException  {
            dao.changeEmailBeanFolder(helper.getMockEmailBean().getId(), 2);

            Assert.fail();
        }
    }

    /* ----------- Parameterized Tests Cases ------------ */

    @RunWith(Parameterized.class)
    public static class ParameterizedTest {
        private EmailBean emailBean;
        private int folderId;

        public ParameterizedTest(EmailBean emailBean) {
            this.emailBean = emailBean;
            this.folderId = 1;
        }

        @Parameterized.Parameters(name = "{index}: email={0}")
        public static Collection<Object> data() {
            return new ArrayList<Object>(helper.getManyMockEmailBeans());
        }

        @Before
        public void before() {
            LOG.info("@before");
            helper.seedDatabase();
        }


        /* -- createEmailBean() tests -- */
        @Test
        public void createEmail_ValidEmail_EmailAddedToDB() throws DaoException {
            dao.createEmailBean(emailBean, folderId);
            EmailBean retrievedEmail = dao.findEmailBeanById(emailBean.getId());
            Assert.assertEquals(emailBean, retrievedEmail);
        }
    }
}
