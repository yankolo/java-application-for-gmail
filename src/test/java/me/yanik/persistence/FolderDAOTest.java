package me.yanik.persistence;

import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FolderDAOTest {
    private final static Logger LOG = LoggerFactory.getLogger(FolderDAOTest.class);

    private static DAOTestHelper helper;
    private static FolderDAO dao;
    private static EmailBeanDAO emailBeanDAO;

    @BeforeClass
    public static void beforeClass() {
        LOG.info("@BeforeClass");
        helper = new DAOTestHelper();
        AddressDAO addressDAO = new AddressDAOImpl(helper.getConnectionManager());
        AttachmentDAO attachmentDAO = new AttachmentDAOImpl(helper.getConnectionManager());
        emailBeanDAO = new EmailBeanDAOImpl(helper.getConnectionManager(), addressDAO, attachmentDAO);
        dao = new FolderDAOImpl(helper.getConnectionManager(), emailBeanDAO);
    }

    @AfterClass
    public static void afterClass() {
        LOG.info("@AfterClass");
        helper.seedDatabase();
    }

    @Before
    public void before() {
        LOG.info("@before");
        helper.seedDatabase();
    }

    /* -- createFolder() tests -- */

    @Test
    public void createFolder_ValidFolder_FolderAddedToDB() throws DaoException {
        Folder folder = helper.getMockFolder();

        dao.createFolder(folder);
        Folder retrievedFolder = dao.findFolderById(folder.getId());

        Assert.assertEquals(folder, retrievedFolder);
    }


    /* -- changeFolderName() tests -- */

    @Test
    public void changeFolderName_ValidName_FolderNameChanged() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.changeFolderName(folder, "FolderDAOTest");

        Folder retrievedFolder = dao.findFolderById(3);

        Assert.assertEquals("FolderDAOTest", retrievedFolder.getName());
    }

    @Test
    public void changeFolderName_ValidName_ObjectFolderNameChanged() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.changeFolderName(folder, "FolderDAOTest");

        Assert.assertEquals("FolderDAOTest", folder.getName());
    }

    @Test(expected = DaoException.class)
    public void changeFolderName_FolderNameAlreadyExists_DaoException() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.changeFolderName(folder, "inbox");

        Assert.fail();
    }

    @Test(expected = DaoException.class)
    public void changeFolderName_FolderDoesntExist_DaoException() throws DaoException {
        dao.changeFolderName(helper.getMockFolder(), "FolderDAOTest");

        Assert.fail();
    }

    @Test(expected = DaoException.class)
    public void changeFolderName_FolderNotRenamable_DaoException() throws DaoException {
        Folder folder = dao.findFolderById(1);
        dao.changeFolderName(folder, "not_inbox");

        Assert.fail();
    }

    /* -- reloadEmailsInFolder() tests -- */

    @Test
    public void reloadEmailsInFolder_FolderExistsInDB_DaoException() throws DaoException {
        Folder folder = helper.getMockFolder();
        dao.createFolder(folder);

        Folder newFolder = helper.getMockFolder();
        newFolder.setEmailBeans(null);
        newFolder.setId(folder.getId());

        dao.reloadEmailsInFolder(newFolder);

        Assert.assertEquals(folder, newFolder);
    }

    /* -- deleteFolder() tests -- */

    @Test
    public void deleteFolder_FolderInDB_FolderNoLongerInDB() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.deleteFolder(folder);

        Assert.assertNull(dao.findFolderById(3));
    }

    @Test
    public void deleteFolder_FolderInDB_IdUnset() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.deleteFolder(folder);

        Assert.assertEquals(-1, folder.getId());
    }

    @Test
    public void deleteFolder_FolderInDB_EmailBeanDeleted() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.deleteFolder(folder);

        Assert.assertNull(emailBeanDAO.findEmailBeanById(23));
    }

    @Test(expected = DaoException.class)
    public void deleteFolder_FolderDoesntExist_DaoException() throws DaoException {
        dao.deleteFolder(helper.getMockFolder());

        Assert.fail();
    }

    @Test(expected = DaoException.class)
    public void deleteFolder_FolderNotDeletable_DaoException() throws DaoException {
        Folder folder = dao.findFolderById(1);
        dao.deleteFolder(folder);

        Assert.fail();
    }

    /* -- findFolderById() tests -- */

    @Test
    public void findFolderById_FolderInDb_CorrectFolderRetrieved() throws DaoException {
        Folder folder = helper.getMockFolder();
        dao.createFolder(folder);

        Folder retrievedFolder = dao.findFolderById(folder.getId());

        Assert.assertEquals(folder, retrievedFolder);
    }

    @Test
    public void findFolderById_FolderNotInDb_NullReturned() throws DaoException {
        Folder retrievedFolder = dao.findFolderById(100);

        Assert.assertNull(retrievedFolder);
    }

    /* -- findAllFolders() tests -- */

    @Test
    public void findAllFolders_8FoldersInDb_8FolderReturned() throws DaoException {
        List<Folder> folders = dao.findAllFolders();

        Assert.assertEquals(8, folders.size());
    }

    /* -- findFolderByName() tests -- */

    @Test
    public void findFolderByName_FolderInDb_CorrectFolderRetrieved() throws DaoException {
        Folder folder = helper.getMockFolder();
        dao.createFolder(folder);

        Folder retrievedFolder = dao.findFolderByName("DAOTest");

        Assert.assertEquals(folder, retrievedFolder);
    }

    @Test
    public void findFolderByName_FolderNotInDb_NullReturned() throws DaoException {
        Folder retrievedFolder = dao.findFolderByName("not_in_db");

        Assert.assertNull(retrievedFolder);
    }

    /* -- isFolderExists() tests -- */

    @Test
    public void isFolderExists_FolderInDb_True() throws DaoException {
        Assert.assertTrue(dao.isFolderExists(1));
    }

    @Test
    public void isFolderExists_FolderNotInDb_False() throws DaoException {
        Assert.assertFalse(dao.isFolderExists(1000));
    }

    /* -- isFolderNameExists() tests -- */

    @Test
    public void isFolderNameExists_FolderInDb_True() throws DaoException {
        Assert.assertTrue(dao.isFolderNameExists("inbox"));
    }

    @Test
    public void isFolderNameExistss_FolderNotInDb_False() throws DaoException {
        Assert.assertFalse(dao.isFolderNameExists("not_in_db"));
    }
}
