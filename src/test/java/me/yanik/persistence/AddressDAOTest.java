package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AddressDAOTest {
    private final static Logger LOG = LoggerFactory.getLogger(AddressDAOTest.class);

    private static DAOTestHelper helper;
    private static AddressDAO dao;

    @BeforeClass
    public static void beforeClass() {
        LOG.info("@BeforeClass");
        helper = new DAOTestHelper();
        dao = new AddressDAOImpl(helper.getConnectionManager());
    }

    @AfterClass
    public static void afterClass() {
        LOG.info("@AfterClass");
        helper.seedDatabase();
    }

    @Before
    public void before() {
        LOG.info("@before");
        helper.seedDatabase();
    }

    /* -- createAddress() tests -- */
    @Test
    public void createAddress_ValidAddress_AddressAddedToDB_1() throws DaoException {
        Address address = new Address("Yanik Kolomatski", "example@gmail.com");

        dao.createAddress(address, 1, "to");
        List<Address> retrievedAddresses = dao.findEmailAddresses(1, "to");

        Assert.assertTrue(retrievedAddresses.contains(address));
    }

    @Test
    public void createAddress_ValidAddress_AddressAddedToDB_2() throws DaoException {
        Address address = new Address("Yanik Kolomatski", "example@gmail.com");

        dao.createAddress(address, 1, "to");
        Address retrievedAddress = dao.findAddressById(address.getId());

        Assert.assertEquals(address, retrievedAddress);
    }

    /* -- deleteAddress() tests -- */

    @Test
    public void deleteAddress_SafeToDeleteAddress_AddressNoLongerInDB() throws DaoException {
        // Note: Safe to delete addresses are only referenced once

        Address address = dao.findAddressById(4);
        dao.deleteAddress(address, 13, "to");

        Assert.assertNull(dao.findAddressById(4));
    }

    @Test
    public void deleteAddress_SafeToDeleteAddress_IdUnset() throws DaoException {
        // Note: Safe to delete addresses are only referenced once

        Address address = dao.findAddressById(4);
        dao.deleteAddress(address, 13, "to");

        Assert.assertEquals(-1, address.getId());
    }

    @Test
    public void deleteAddress_AddressNotSafeToDelete_AddressStillInDB() throws DaoException {
        // Note: Safe to delete addresses are only referenced once

        Address address = dao.findAddressById(1);
        dao.deleteAddress(address, 2, "from");

        Address retrievedAddress = dao.findAddressById(1);

        Assert.assertEquals(address, retrievedAddress);
    }

    @Test
    public void deleteAddress_AddressNotSafe_EmailAddressLinkRemoved() throws DaoException {
        // Note: Safe to delete addresses are only referenced once
        // Note: EmailAddress is the entity that bridges the email bean and address

        Address address = dao.findAddressById(1);
        dao.deleteAddress(address, 2, "from");

        List<Address> emailAddresses = dao.findEmailAddresses(2, "from");

        Assert.assertNull(emailAddresses);
    }

    @Test(expected=DaoException.class)
    public void deleteAddress_AddressNotInDB_DaoException() throws DaoException {
        Address address = new Address("Yanik Kolomatski", "example@gmail.com");
        dao.deleteAddress(address, 1, "from");

        Assert.fail();
    }

    /* -- findEmailAddresses() tests -- */

    @Test
    public void findEmailAddresses_2AddressesMatching_2AddressesRetrieved() throws DaoException {
        List<Address> expectedAddresses = new ArrayList<>();

        Address address1 = new Address("Lenore Cabe", "vk@islvbuth.com");
        Address address2 = new Address("Maren Fazzinga", "ie@ezcuw.com");

        expectedAddresses.add(address1);
        expectedAddresses.add(address2);

        List<Address> retrievedAddresses = dao.findEmailAddresses(2, "to");

        // Lists might not be in order
        Assert.assertTrue(expectedAddresses.containsAll(retrievedAddresses) &&
                retrievedAddresses.containsAll(expectedAddresses));
    }


    /* -- findAddressById() tests -- */

    @Test
    public void findAddressById_AddressInDB_RightAddressRetrieved_1() throws DaoException {
        Address address = dao.findAddressById(1);

        Address expectedAddress = new Address();
        expectedAddress.setEmail("jw@rkekdhp.com");
        expectedAddress.setPersonalName("Kasey Bourns");

        Assert.assertEquals(expectedAddress, address);
    }

    @Test
    public void findAddressById_AddressInDB_RightAddressRetrieved_2() throws DaoException {
        Address address = dao.findAddressById(50);

        Address expectedAddress = new Address();
        expectedAddress.setEmail("drbr@oiqrthr.com");
        expectedAddress.setPersonalName("Mohinani");

        Assert.assertEquals(expectedAddress, address);
    }

    @Test
    public void findAddressById_AddressNotInDB_Null() throws DaoException {
        Address address = dao.findAddressById(1000);
        Assert.assertNull(address);
    }
}
