package me.yanik.persistence;


import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(Enclosed.class)
public class AttachmentDAOTest {
    private final static Logger LOG = LoggerFactory.getLogger(AttachmentDAOTest.class);

    private static DAOTestHelper helper;
    private static AttachmentDAO dao;

    @BeforeClass
    public static void beforeClass() {
        LOG.info("@BeforeClass");
        helper = new DAOTestHelper();
        dao = new AttachmentDAOImpl(helper.getConnectionManager());
    }

    @AfterClass
    public static void afterClass() {
        LOG.info("@AfterClass");
        helper.seedDatabase();
    }

    /* ----------- Tests Cases that run only once ------------ */
    public static class OneTimeTests {
        @Before
        public void before() {
            LOG.info("@before");
            helper.seedDatabase();
        }

        /* -- deleteAttachment() tests -- */
        @Test
        public void deleteAttachment_AttachmentExistsInDB_AttachmentNoLongerInDB() throws DaoException {
            Attachment attachment = dao.findAttachmentById(1);
            dao.deleteAttachment(attachment);

            Assert.assertNull(dao.findAttachmentById(1));
        }

        @Test
        public void deleteAttachment_AttachmentExistsInDB_IdUnset() throws DaoException {
            Attachment attachment = dao.findAttachmentById(1);
            dao.deleteAttachment(attachment);

            Assert.assertEquals(-1, attachment.getId());
        }

        @Test(expected=DaoException.class)
        public void deleteAttachment_AttachmentNotInDB_DaoException() throws DaoException {
            Attachment attachment = helper.getMockAttachment();
            dao.deleteAttachment(attachment);

            Assert.fail();
        }

        /* -- findAttachmentById() tests -- */

        @Test
        public void findAttachmentById_AttachmentInDB_RightAttachmentRetrieved_1() throws DaoException {
            Attachment attachment = dao.findAttachmentById(1);
            Attachment expectedAttachment = helper.loadAttachment("sql/attachment_blobs/alan-haverty-41-unsplash.jpg",
                    "alan-haverty-41-unsplash.jpg", true);
            Assert.assertEquals(expectedAttachment, attachment);
        }

        @Test
        public void findAttachmentById_AttachmentInDB_RightAttachmentRetrieved_2() throws DaoException {
            Attachment attachment = dao.findAttachmentById(10);
            Attachment expectedAttachment = helper.loadAttachment("sql/attachment_blobs/aleks-dorohovich-38-unsplash.jpg",
                    "aleks-dorohovich-38-unsplash.jpg", false);
            Assert.assertEquals(expectedAttachment, attachment);
        }

        @Test
        public void findAttachmentById_AttachmentNotInDB_Null() throws DaoException {
            Attachment attachment = dao.findAttachmentById(1000);
            Assert.assertNull(attachment);
        }
    }

    /* ----------- Parameterized Tests Cases ------------ */
    @RunWith(Parameterized.class)
    public static class ParameterizedTest {
        private Attachment attachment;

        public ParameterizedTest(Attachment attachment) {
            this.attachment = attachment;
        }

        @Parameterized.Parameters(name = "{index}: attachment={0}")
        public static Collection<Object> data() {
            return new ArrayList<Object>(helper.getManyMockAttachments());
        }

        @Before
        public void before() {
            LOG.info("@before");
            helper.seedDatabase();
        }


        /* -- createAttachment() tests -- */
        @Test
        public void createAttachment_ValidAttachment_AttachmentAddedToDB() throws DaoException {
            dao.createAttachment(attachment, 1);
            Attachment retrievedAttachment = dao.findAttachmentById(attachment.getId());
            Assert.assertEquals(attachment, retrievedAttachment);
        }
    }
}

