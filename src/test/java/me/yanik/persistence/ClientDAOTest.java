package me.yanik.persistence;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import org.junit.*;

import org.junit.experimental.runners.Enclosed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * >>>----- NOTE ---------------------------------<<<< !!!
 *
 * CientDAOTest duplicates almost all the tests of all the other DAOs.
 * This is intended. This is to assure that the DAO that will be presented to the application
 * functions as expected (even if most of its implemented methods are just calls to other DAOs)
 *
 */
public class ClientDAOTest {
    private final static Logger LOG = LoggerFactory.getLogger(ClientDAOTest.class);

    private static DAOTestHelper helper;
    private static ClientDAO dao;
    private static EmailBeanDAO emailBeanDAO;
    private static FolderDAO folderDAO;

    @BeforeClass
    public static void beforeClass() {
        LOG.info("@BeforeClass");
        helper = new DAOTestHelper();
        AddressDAO addressDAO = new AddressDAOImpl(helper.getConnectionManager());
        AttachmentDAO attachmentDAO = new AttachmentDAOImpl(helper.getConnectionManager());
        emailBeanDAO = new EmailBeanDAOImpl(helper.getConnectionManager(), addressDAO, attachmentDAO);
        folderDAO = new FolderDAOImpl(helper.getConnectionManager(), emailBeanDAO);
        dao = new ClientDAOImpl(helper.getConnectionManager());
    }

    @AfterClass
    public static void afterClass() {
        LOG.info("@AfterClass");
        helper.seedDatabase();
    }

    @Before
    public void before() {
        LOG.info("@before");
        helper.seedDatabase();
    }

    /* -- createEmailBean() tests -- */

    @Test
    public void createEmail_ValidEmail_EmailAddedToDB() throws DaoException {
        EmailBean emailBean = helper.getMockEmailBean();

        dao.createEmailBean(emailBean, "inbox");
        EmailBean retrievedEmail = emailBeanDAO.findEmailBeanById(emailBean.getId());
        Assert.assertEquals(emailBean, retrievedEmail);
    }

    @Test
    public void createEmail_ValidEmail_EmailAddedToFolder() throws DaoException {
        EmailBean emailBean = helper.getMockEmailBean();

        dao.createEmailBean(emailBean, "inbox");
        Folder folder = dao.findFolderByName("inbox");

        Assert.assertTrue(Arrays.asList(folder.getEmailBeans()).contains(emailBean));
    }

    @Test(expected=DaoException.class)
    public void createEmail_FolderDoesntExist_DaoException() throws DaoException {
        EmailBean emailBean = helper.getMockEmailBean();

        dao.createEmailBean(emailBean, "not_in_db");

        Assert.fail();
    }

    /* -- createFolder() tests -- */

    @Test
    public void createFolder_ValidFolder_FolderAddedToDB() throws DaoException {
        Folder folder = helper.getMockFolder();

        dao.createFolder(folder);
        Folder retrievedFolder = dao.findFolderById(folder.getId());

        Assert.assertEquals(folder, retrievedFolder);
    }

    /* -- findAllFolders() tests -- */

    @Test
    public void findAllFolders_8FoldersInDb_8FolderReturned() throws DaoException {
        List<Folder> folders = dao.findAllFolders();

        Assert.assertEquals(8, folders.size());
    }

    /* -- findFolderByName() tests -- */

    @Test
    public void findFolderByName_FolderInDb_CorrectFolderRetrieved() throws DaoException {
        Folder folder = helper.getMockFolder();
        dao.createFolder(folder);

        Folder retrievedFolder = dao.findFolderByName("DAOTest");

        Assert.assertEquals(folder, retrievedFolder);
    }

    @Test
    public void findFolderByName_FolderNotInDb_NullReturned() throws DaoException {
        Folder retrievedFolder = dao.findFolderByName("not_in_db");

        Assert.assertNull(retrievedFolder);
    }

    /* -- reloadEmailsInFolder() tests -- */

    @Test
    public void reloadEmailsInFolder_FolderExistsInDB_DaoException() throws DaoException {
        Folder folder = helper.getMockFolder();
        dao.createFolder(folder);

        Folder newFolder = helper.getMockFolder();
        newFolder.setEmailBeans(null);
        newFolder.setId(folder.getId());

        dao.reloadEmailsInFolder(newFolder);

        Assert.assertEquals(folder, newFolder);
    }

    /* -- changeEmailBeanFolder() tests -- */

    @Test
    public void changeEmailBeanFolder_ValidChange_FolderChnaged() throws DaoException  {
        EmailBean emailBean = dao.findEmailBeanById(1);
        dao.changeEmailBeanFolder(emailBean.getId(), "sent");

        Folder folder = folderDAO.findFolderByName("sent");

        Assert.assertTrue(Arrays.asList(folder.getEmailBeans()).contains(emailBean));
    }

    @Test(expected=DaoException.class)
    public void changeEmailBeanFolder_FolderDoesntExist_DaoException() throws DaoException  {
        EmailBean emailBean = dao.findEmailBeanById(1);
        dao.changeEmailBeanFolder(emailBean.getId(), "not_in_db");

        Assert.fail();
    }

    @Test(expected=DaoException.class)
    public void changeEmailBeanFolder_EmailBeanNotInDb_DaoException() throws DaoException  {
        dao.changeEmailBeanFolder(helper.getMockEmailBean().getId(), "sent");

        Assert.fail();
    }

    /* -- changeFolderName() tests -- */

    @Test
    public void changeFolderName_ValidName_FolderNameChanged() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.changeFolderName(folder, "FolderDAOTest");

        Folder retrievedFolder = dao.findFolderById(3);

        Assert.assertEquals("FolderDAOTest", retrievedFolder.getName());
    }

    @Test
    public void changeFolderName_ValidName_ObjectFolderNameChanged() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.changeFolderName(folder, "FolderDAOTest");

        Assert.assertEquals("FolderDAOTest", folder.getName());
    }

    @Test(expected = DaoException.class)
    public void changeFolderName_FolderNameAlreadyExists_DaoException() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.changeFolderName(folder, "inbox");

        Assert.fail();
    }

    @Test(expected = DaoException.class)
    public void changeFolderName_FolderDoesntExist_DaoException() throws DaoException {
        dao.changeFolderName(helper.getMockFolder(), "FolderDAOTest");

        Assert.fail();
    }

    @Test(expected = DaoException.class)
    public void changeFolderName_FolderNotRenamable_DaoException() throws DaoException {
        Folder folder = dao.findFolderById(1);
        dao.changeFolderName(folder, "not_inbox");

        Assert.fail();
    }

    /* -- deleteEmailBean() tests -- */

    @Test
    public void deleteEmail_EmailExistsInDB_EmailNoLongerInDB() throws DaoException {
        EmailBean emailBean = dao.findEmailBeanById(1);
        dao.deleteEmailBean(emailBean);

        Assert.assertNull(dao.findEmailBeanById(1));
    }

    @Test
    public void deleteEmail_EmailExistsInDB_IdUnset() throws DaoException {
        EmailBean emailBean = dao.findEmailBeanById(1);
        dao.deleteEmailBean(emailBean);

        Assert.assertEquals(-1, emailBean.getId());
    }

    @Test(expected=DaoException.class)
    public void deleteEmail_EmailNotInDB_DaoException() throws DaoException {
        EmailBean emailBean = helper.getMockEmailBean();
        dao.deleteEmailBean(emailBean);

        Assert.fail();
    }

    /* -- deleteFolder() tests -- */

    @Test
    public void deleteFolder_FolderInDB_FolderNoLongerInDB() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.deleteFolder(folder);

        Assert.assertNull(dao.findFolderById(3));
    }

    @Test
    public void deleteFolder_FolderInDB_IdUnset() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.deleteFolder(folder);

        Assert.assertEquals(-1, folder.getId());
    }

    @Test
    public void deleteFolder_FolderInDB_EmailBeanDeleted() throws DaoException {
        Folder folder = dao.findFolderById(3);
        dao.deleteFolder(folder);

        Assert.assertNull(emailBeanDAO.findEmailBeanById(23));
    }

    @Test(expected = DaoException.class)
    public void deleteFolder_FolderDoesntExist_DaoException() throws DaoException {
        dao.deleteFolder(helper.getMockFolder());

        Assert.fail();
    }

    @Test(expected = DaoException.class)
    public void deleteFolder_FolderNotDeletable_DaoException() throws DaoException {
        Folder folder = dao.findFolderById(1);
        dao.deleteFolder(folder);

        Assert.fail();
    }

    /* -- findEmailById() tests -- */

    @Test
    public void findEmailBeanById_EmailBeanInDB_RightEmailBeanRetrieved() throws DaoException {
        EmailBean emailBean = dao.findEmailBeanById(1);

        EmailBean expectedEmailBean = new EmailBean();
        expectedEmailBean.setSubject("Sincerity");
        expectedEmailBean.setTextMessage("HiAn sincerity so extremity he additions. " +
                "Her yet there truth merit. Mrs all projecting favourable now unpleasing." +
                " Son law garden chatty temper. Oh children provided to mr elegance marriage" +
                " strongly. Off can admiration prosperous now devonshire diminution law.");
        expectedEmailBean.setHtmlMessage("<html><body><h1>Hi</h1><p>An sincerity so extremity he" +
                " additions. Her yet there truth merit. Mrs all projecting favourable now unpleasing." +
                " <img src=\"cid:alan-haverty-41-unsplash.jpg\"> Son law garden chatty temper." +
                " Oh children provided to mr elegance marriage strongly. Off can admiration " +
                "prosperous now devonshire diminution law.</p></body></html>");
        expectedEmailBean.setPriority(3);
        expectedEmailBean.setDateReceived(LocalDateTime.of(2018, 9, 11,00, 00,00));
        expectedEmailBean.setDateSent(LocalDateTime.of(2018, 9, 11,00, 00,00));
        expectedEmailBean.setFrom(new Address("Tia Standard", "lhzyzylzm@oapw.com"));
        expectedEmailBean.setTo(new Address[] {new Address("Alana Brenes", "emylc@eiagpn.com")});
        expectedEmailBean.setCc(new Address[] {new Address("Man Diachenko", "xoid@dtg.com")});
        expectedEmailBean.setBcc(new Address[] {new Address("Selena Krebbs", "dgeifzv@pbr.com")});
        expectedEmailBean.setAttachments(new Attachment[] {
                helper.loadAttachment("sql/attachment_blobs/alan-haverty-41-unsplash.jpg",
                        "alan-haverty-41-unsplash.jpg", true)});

        Assert.assertEquals(expectedEmailBean, emailBean);
    }

    @Test
    public void findEmailBeanById_EmailBeanInDB_IdSet() throws DaoException {
        EmailBean emailBean = dao.findEmailBeanById(1);
        Assert.assertEquals(1, emailBean.getId());
    }

    @Test
    public void findEmailBeanById_EmailBeanNotInDB_Null() throws DaoException {
        EmailBean emailBean = dao.findEmailBeanById(1000);
        Assert.assertNull(emailBean);
    }

    /* -- findFolderById() tests -- */

    @Test
    public void findFolderById_FolderInDb_CorrectFolderRetrieved() throws DaoException {
        Folder folder = helper.getMockFolder();
        dao.createFolder(folder);

        Folder retrievedFolder = dao.findFolderById(folder.getId());

        Assert.assertEquals(folder, retrievedFolder);
    }

    @Test
    public void findFolderById_FolderNotInDb_NullReturned() throws DaoException {
        Folder retrievedFolder = dao.findFolderById(100);

        Assert.assertNull(retrievedFolder);
    }

    /* -- findAttachmentById() tests -- */

    @Test
    public void findAttachmentById_AttachmentInDB_RightAttachmentRetrieved_1() throws DaoException {
        Attachment attachment = dao.findAttachmentById(1);
        Attachment expectedAttachment = helper.loadAttachment("sql/attachment_blobs/alan-haverty-41-unsplash.jpg",
                "alan-haverty-41-unsplash.jpg", true);
        Assert.assertEquals(expectedAttachment, attachment);
    }

    @Test
    public void findAttachmentById_AttachmentInDB_RightAttachmentRetrieved_2() throws DaoException {
        Attachment attachment = dao.findAttachmentById(10);
        Attachment expectedAttachment = helper.loadAttachment("sql/attachment_blobs/aleks-dorohovich-38-unsplash.jpg",
                "aleks-dorohovich-38-unsplash.jpg", false);
        Assert.assertEquals(expectedAttachment, attachment);
    }

    @Test
    public void findAttachmentById_AttachmentNotInDB_Null() throws DaoException {
        Attachment attachment = dao.findAttachmentById(1000);
        Assert.assertNull(attachment);
    }

    /* -- findAddressById() tests -- */

    @Test
    public void findAddressById_AddressInDB_RightAddressRetrieved_1() throws DaoException {
        Address address = dao.findAddressById(1);

        Address expectedAddress = new Address();
        expectedAddress.setEmail("jw@rkekdhp.com");
        expectedAddress.setPersonalName("Kasey Bourns");

        Assert.assertEquals(expectedAddress, address);
    }

    @Test
    public void findAddressById_AddressInDB_RightAddressRetrieved_2() throws DaoException {
        Address address = dao.findAddressById(50);

        Address expectedAddress = new Address();
        expectedAddress.setEmail("drbr@oiqrthr.com");
        expectedAddress.setPersonalName("Mohinani");

        Assert.assertEquals(expectedAddress, address);
    }

    @Test
    public void findAddressById_AddressNotInDB_Null() throws DaoException {
        Address address = dao.findAddressById(1000);
        Assert.assertNull(address);
    }

}
