package me.yanik.persistence;

import me.yanik.data.Address;
import me.yanik.data.Attachment;
import me.yanik.data.EmailBean;
import me.yanik.data.Folder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

public class DAOTestHelper {
    private final static Logger LOG = LoggerFactory.getLogger(DAOTestHelper.class);

    private final static String URL = "jdbc:mysql://localhost:3306/test_db?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final static String USER = "jag";
    private final static String PASSWORD = "jag-password";

    public static ConnectionManager getConnectionManager() {
        return new ConnectionManager(URL, USER, PASSWORD);
    }

    public static Folder getMockFolder() {
        Folder folder = new Folder();
        folder.setName("DAOTest");

        List<EmailBean> emailBeans = getManyMockEmailBeans();
        EmailBean[] emailBeansArray = emailBeans.toArray(new EmailBean[emailBeans.size()]);
        folder.setEmailBeans(emailBeansArray);

        return folder;
    }

    /**
     * Method to generate one mock attachment
     */
    public static Attachment getMockAttachment() {
        return loadAttachment("WindsorKen180.jpg", "WindsorKen180.jpg", false);
    }

    /**
     * Method to generate a list of mock attachments
     */
    public static List<Attachment> getManyMockAttachments() {
        List<Attachment> attachments = new ArrayList<>();
        attachments.add(loadAttachment("WindsorKen180.jpg", "WindsorKen180.jpg", false));
        attachments.add(loadAttachment("sql/attachment_blobs/alan-haverty-41-unsplash.jpg", "alan-haverty-41-unsplash.jpg", true));
        attachments.add(loadAttachment("sql/attachment_blobs/alejandro-escamilla-6-unsplash.jpg", "alejandro-escamilla-6-unsplash.jpg", false));
        attachments.add(loadAttachment("sql/attachment_blobs/alejandro-escamilla-21-unsplash.jpg", "alejandro-escamilla-21-unsplash.jpg", false));
        attachments.add(loadAttachment("sql/attachment_blobs/alejandro-escamilla-22-unsplash.jpg", "alejandro-escamilla-22-unsplash.jpg", true));

        return attachments;
    }

    /**
     * Helper method to generate one mock email
     * It is static because it can be called from static contexts
     */
    public static EmailBean getMockEmailBean() {
        EmailBean email = new EmailBean();
        email.setFrom(new Address("Address", "sender@gmailcom"));
        email.setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        email.setSubject("Unit Test Subject");
        email.setTextMessage("Test");

        return email;
    }

    /**
     * Helper method to generate a list of mock emails
     * It is static because it can be called from static contexts (such as setting parameterized parameters)
     */
    public static List<EmailBean> getManyMockEmailBeans() {
        EmailBean[] emails = new EmailBean[19];

        emails[0] = new EmailBean();
        emails[0].setFrom(new Address("Address", "sender@gmail.com"));
        emails[0].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});

        emails[1] = new EmailBean();
        emails[1].setFrom(new Address("Address", "sender@gmailcom"));
        emails[1].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[1].setSubject("Unit Test Subject");

        emails[2] = new EmailBean();
        emails[2].setFrom(new Address("Address", "sender@gmailcom"));
        emails[2].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[2].setTextMessage("Test");

        emails[3] = new EmailBean();
        emails[3].setFrom(new Address("Address", "sender@gmailcom"));
        emails[3].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[3].setHtmlMessage("<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>This is a test</h1></body></html>");

        emails[4] = new EmailBean();
        emails[4].setFrom(new Address("Address", "sender@gmailcom"));
        emails[4].setTo(new Address[]{new Address("Address", "recipient@gmail.com"),
                new Address("Address", "other1@gmail.com")});
        emails[4].setTextMessage("Test");

        emails[5] = new EmailBean();
        emails[5].setFrom(new Address("Address", "sender@gmailcom"));
        emails[5].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[5].setCc(new Address[]{new Address("Address", "other1@gmail.com")});
        emails[5].setTextMessage("Test");

        emails[6] = new EmailBean();
        emails[6].setFrom(new Address("Address", "sender@gmailcom"));
        emails[6].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[6].setCc(new Address[]{new Address("Address", "other1@gmail.com"),
                new Address("Address", "other2@gmail.com")});
        emails[6].setTextMessage("Test");

        emails[7] = new EmailBean();
        emails[7].setFrom(new Address("Address", "sender@gmailcom"));
        emails[7].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[7].setCc(new Address[]{new Address("Address", "other1@gmail.com"),
                new Address("Address", "other2@gmail.com")});
        emails[7].setTextMessage("Test");

        emails[8] = new EmailBean();
        emails[8].setFrom(new Address("Address", "sender@gmailcom"));
        emails[8].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[8].setBcc(new Address[]{new Address("Address", "other1@gmail.com")});
        emails[8].setTextMessage("Test");

        emails[9] = new EmailBean();
        emails[9].setFrom(new Address("Address", "sender@gmailcom"));
        emails[9].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[9].setBcc(new Address[]{new Address("Address", "other1@gmail.com"),
                new Address("Address", "other2@gmail.com")});
        emails[9].setTextMessage("Test");

        emails[10] = new EmailBean();
        emails[10].setFrom(new Address("Address", "sender@gmailcom"));
        emails[10].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[10].setBcc(new Address[]{new Address("Address", "other1@gmail.com"),
                new Address("Address", "other2@gmail.com")});
        emails[10].setTextMessage("Test");

        emails[11] = new EmailBean();
        emails[11].setFrom(new Address("Address", "sender@gmailcom"));
        emails[11].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[11].setTextMessage("Test");

        emails[12] = new EmailBean();
        emails[12].setFrom(new Address("Address", "sender@gmailcom"));
        emails[12].setTo(new Address[]{new Address("Address", "recipient@gmail.com"),
                new Address("Address", "other1@gmail.com")});
        emails[12].setTextMessage("Test");

        emails[13] = new EmailBean();
        emails[13].setFrom(new Address("Address", "sender@gmailcom"));
        emails[13].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[13].setTextMessage("Test");
        emails[13].setAttachments(new Attachment[]{loadAttachment("WindsorKen180.jpg", "WindsorKen180.jpg", false)});

        emails[14] = new EmailBean();
        emails[14].setFrom(new Address("Address", "sender@gmailcom"));
        emails[14].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[14].setHtmlMessage("<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in "
                + "this email.</h1><img src='cid:FreeFall.jpg'>"
                + "<h2>I'm flying!</h2></body></html>");
        emails[14].setAttachments(new Attachment[]{loadAttachment("FreeFall.jpg", "FreeFall.jpg", true)});

        emails[15] = new EmailBean();
        emails[15].setFrom(new Address("Address", "sender@gmailcom"));
        emails[15].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[15].setTextMessage("Test");
        emails[15].setDateReceived(LocalDateTime.now());

        emails[16] = new EmailBean();
        emails[16].setFrom(new Address("Address", "sender@gmailcom"));
        emails[16].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[16].setTextMessage("Test");
        emails[16].setDateSent(LocalDateTime.now());

        emails[17] = new EmailBean();
        emails[17].setFrom(new Address("Address", "sender@gmailcom"));
        emails[17].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[17].setTextMessage("Test");
        emails[17].setPriority(5);

        emails[18] = new EmailBean();
        emails[18].setFrom(new Address("Address", "sender@gmailcom"));
        emails[18].setTo(new Address[]{new Address("Address", "recipient@gmail.com")});
        emails[18].setTextMessage("Test");

        LOG.info("Mock emails generated");
        return Arrays.asList(emails);
    }

    /**
     * Helper method to generate a list of mock attachments
     */
    public static Attachment loadAttachment(String path, String name, boolean isEmbedded) {
        try {
            URI pictureURI = Thread.currentThread().getContextClassLoader().getResource(path).toURI();
            Path picturePath = Paths.get(pictureURI);
            byte[] picture = Files.readAllBytes(picturePath);

            Attachment attachment = new Attachment();
            attachment.setName(name);
            attachment.setContent(picture);
            attachment.setEmbedded(isEmbedded);

            return attachment;
        } catch (IOException | URISyntaxException e) {
            String errorMessage = "Failed to load attachments";
            LOG.error(errorMessage + " " + e);
            throw new RuntimeException(errorMessage, e);
        }
    }

    /**
     * -- Code copied from demo and modified to support procedures and load blobs from resources
     *
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss
     */
    public void seedDatabase() {
        LOG.info("Seeding Database");

        final String seedDataScript = loadAsString("sql/CreateJagTables.sql");

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);) {
            LOG.info("Executing sql/CreateJagTables.sql");
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }

            LOG.info("Loading images from resources");
            // Loading images from resources
            loadBlobDirectory("sql/attachment_blobs", connection);



            LOG.info("Seeding finished");
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        } catch (IOException ioe) {
            throw new RuntimeException("Failed seeding database", ioe);
        }
    }

    /**
     * -- Code copied from demo
     *
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
             Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    /**
     * -- Code copied from demo and modified to support procedures
     *
     */
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                //line = line.trim();
                line = line.replace(System.getProperty("line.separator"), " ");
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    // Remove the statement delimiter. This let's us to put a custom
                    // delimiter to support procedures (Statements do not need
                    // delimiter to be able to execute
                    line = line.replace(statementDelimiter, "");

                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    /**
     * -- Code copied from demo
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

    /**
     * It is not efficient to put all the bytes of the images in the
     * script. Therefore, loading all the blobs and updating attachments.
     */
    private void loadBlobDirectory(String resourceDirectory, Connection connection) throws IOException, SQLException {
        URI directoryURI = getResourceURI(resourceDirectory);
        File directory = Paths.get(directoryURI).toFile();

        for (File image : directory.listFiles()) {
            Path imagePath = image.toPath();
            String imageName = imagePath.getFileName().toString();
            byte[] imageBytes = Files.readAllBytes(imagePath);

            loadBlob(imageName, imageBytes, connection);
        }
    }

    /**
     * Loads byte[] from image path and updates the 'data' field of the associated attachment in the database
     */
    private void loadBlob(String imageName, byte[] imageBytes, Connection connection) throws SQLException {
        String query = "select attachment_id, name, data from attachment where name = ?";
        PreparedStatement getAttachment = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        getAttachment.setString(1, imageName);

        ResultSet results = getAttachment.executeQuery();

        while (results.next()) {
            results.updateBytes("data", imageBytes);
            results.updateRow();
        }

        results.close();
    }

    /**
     *  Gets the URI of the specified resource
     */
    private URI getResourceURI(String resource) {
        try {
            URI resourceURI = Thread.currentThread().getContextClassLoader().getResource(resource).toURI();

            return resourceURI;
        } catch (URISyntaxException e) {
            throw new RuntimeException("Unable to get URI of: " + resource);
        }
    }
}
